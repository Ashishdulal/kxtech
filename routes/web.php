<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/contact', function () {
    return view('contact');
});

Route::fallback(function() {
    return view('nopage');
});


Route::get('/','MainpageController@index');

Route::post('/sendemail/send', 'SendMailController@send');

Route::post('/subscribe/send', 'SendMailController@subscribe');

Route::get('/focus-detail/{id}', 'MainpageController@FocusDetailPage');

Route::get('/home/our-focus', 'FocusController@index');
Route::get('/home/our-focus/create', 'FocusController@create');
Route::post('/home/our-focus/create', 'FocusController@store');
Route::get('/home/our-focus/edit/{id}', 'FocusController@edit')->name('focus.edit');
Route::post('/home/our-focus/edit/{id}', 'FocusController@update')->name('focus.update');
Route::delete('/home/our-focus/destroy/{id}', 'FocusController@destroy')->name('focus.delete');


Route::get('/home/services', 'ServiceController@index');
Route::get('/home/services/create', 'ServiceController@create');
Route::post('/home/services/create', 'ServiceController@store');
Route::get('/home/services/edit/{id}', 'ServiceController@edit')->name('service.edit');
Route::post('/home/services/edit/{id}', 'ServiceController@update')->name('service.update');
Route::delete('/home/services/destroy/{id}', 'ServiceController@destroy')->name('service.delete');


Route::get('/jobs', 'MainpageController@jobsIndex');
Route::get('/jobs-detail/{id}', 'MainpageController@jobsDetail');


Route::get('/home/jobs', 'JobController@index');
Route::get('/home/jobs/create', 'JobController@create');
Route::post('/home/jobs/create', 'JobController@store');
Route::get('/home/jobs/edit/{id}', 'JobController@edit')->name('job.edit');
Route::post('/home/jobs/edit/{id}', 'JobController@update')->name('job.update');
Route::delete('/home/jobs/destroy/{id}', 'JobController@destroy')->name('job.delete');


Route::get('/show-gallery', 'MainpageController@galleryPage');

Route::get('/home/gallery', 'GalleryController@index');
Route::get('/home/gallery/create', 'GalleryController@create');
Route::post('/home/gallery/create', 'GalleryController@store');
Route::get('/home/gallery/createVideo', 'GalleryController@createVideo');
Route::post('/home/gallery/createVideo', 'GalleryController@storeVideo');
Route::get('/home/gallery/edit/{id}', 'GalleryController@edit')->name('gallery.edit');
Route::post('/home/gallery/edit/{id}', 'GalleryController@update')->name('gallery.update');
Route::delete('/home/gallery/destroy/{id}', 'GalleryController@destroy')->name('gallery.delete');


Route::get('/home/portfolio-category', 'PortfolioCategoryController@index');
Route::get('/home/portfolio-category/create', 'PortfolioCategoryController@create');
Route::post('/home/portfolio-category/create', 'PortfolioCategoryController@store')->name('make.portfolio');
Route::get('/home/portfolio-category/{id}', 'PortfolioCategoryController@show');
Route::get('/home/portfolio-category/edit/{id}', 'PortfolioCategoryController@edit');
Route::post('/home/portfolio-category/edit/{id}', 'PortfolioCategoryController@update')->name('update.portfolio');
Route::get('/home/portfolio-category/destroy/{id}', 'PortfolioCategoryController@destroy')->name('delete.portfolio');

Route::get('/home/blog-category', 'BlogcategoryController@index');
Route::get('/home/blog-category/create', 'BlogcategoryController@create');
Route::post('/home/blog-category/create', 'BlogcategoryController@store')->name('make.blog');
Route::get('/home/blog-category/{id}', 'BlogcategoryController@show');
Route::get('/home/blog-category/edit/{id}', 'BlogcategoryController@edit');
Route::post('/home/blog-category/edit/{id}', 'BlogcategoryController@update')->name('update.blog');
Route::get('/home/blog-category/destroy/{id}', 'BlogcategoryController@destroy')->name('delete.blog');

Route::get('/services', 'MainpageController@servicepage');
Route::get('/services-detail/{id}', 'MainpageController@detailpage');

Route::get('/portfolio', 'MainpageController@portfoliohomepage');
Route::get('/career', 'MainpageController@carrierhomepage');

Route::get('/blog', 'MainpageController@bloghomepage');
Route::get('/blog/blog-category-detail/{id}', 'MainpageController@blogCategoryPage');
Route::get('/blog-detail/{id}', 'MainpageController@blogdetail');

// Route::get('/portfolio/portfolio-category-detail/{id}', 'MainpageController@portfolioCategoryPage');
Route::get('/portfolio-detail/{id}', 'MainpageController@portfoliodetailpage');

Route::get('/home/portfolio', 'PortfolioController@index');
Route::get('/home/portfolio/create', 'PortfolioController@create');
Route::post('/home/portfolio/create', 'PortfolioController@store')->name('make-portfolio.post');
Route::get('/home/portfolio/{id}', 'PortfolioController@show');
Route::get('/home/portfolio/edit/{id}', 'PortfolioController@edit');
Route::post('/home/portfolio/edit/{id}', 'PortfolioController@update')->name('update-portfolio.post');
Route::get('/home/portfolio/destroy/{id}', 'PortfolioController@destroy')->name('delete-portfolio.post');


Route::get('/home/blogs', 'BlogController@index');
Route::get('/home/blog/create', 'BlogController@create');
Route::post('/home/blog/create', 'BlogController@store')->name('make-post.blog');
Route::get('/home/blog/{id}', 'BlogController@show');
Route::get('/home/blog/edit/{id}', 'BlogController@edit');
Route::post('/home/blog/edit/{id}', 'BlogController@update')->name('update-post.blog');
Route::get('/home/blog/destroy/{id}', 'BlogController@destroy')->name('delete-post.blog');


Route::get('/home/sliders', 'SliderController@index');
Route::get('/home/slider/create', 'SliderController@create');
Route::post('/home/slider/create', 'SliderController@store');
Route::get('/home/slider/edit/{id}', 'SliderController@edit')->name('slider.edit');
Route::post('/home/slider/edit/{id}', 'SliderController@update')->name('slider.update');
Route::delete('/home/slider/destroy/{id}', 'SliderController@destroy')->name('slider.delete');


Route::get('/expertises', 'MainpageController@aboutPage');



Route::get('/home/team-members', 'DoctorsController@index');
Route::get('/home/team-members/create', 'DoctorsController@create');
Route::post('/home/team-members/create', 'DoctorsController@store')->name('make.doctor');
Route::get('/home/team-members/{id}', 'DoctorsController@show');
Route::get('/home/team-members/edit/{id}', 'DoctorsController@edit');
Route::post('/home/team-members/edit/{id}', 'DoctorsController@update')->name('update.doctor');
Route::delete('/home/team-members/destroy/{id}', 'DoctorsController@destroy')->name('delete.doctor');


Route::get('/home/testimonials', 'TestimonialController@index');
Route::get('/home/testimonials/create', 'TestimonialController@create');
Route::post('/home/testimonials/create', 'TestimonialController@store');
Route::get('/home/testimonials/edit/{id}', 'TestimonialController@edit')->name('testimonial.edit');
Route::post('/home/testimonials/edit/{id}', 'TestimonialController@update')->name('testimonial.update');
Route::delete('/home/testimonials/destroy/{id}', 'TestimonialController@destroy')->name('testimonial.delete');