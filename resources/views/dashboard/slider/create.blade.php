@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
								<div class="col-lg-12">
									<div class="container card">
										<div class="card-header">
											<div class="au-breadcrumb-left">
												<span class="au-breadcrumb-span">You are here:</span>
												<ul class="list-unstyled list-inline au-breadcrumb__list">
													<li class="list-inline-item">
														<a href="/home">Home</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item">
														<a href="/home/sliders">Sliders</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item active">create</li>
												</ul>
											</div>
										</div>
										@if($errors->any())
										@foreach($errors->all() as $error)
										<ul>
											<li>{{$error}}</li>
										</ul>
										@endforeach
										@endif
										<div class="card-body card-block">
											<form class="form-horizontal" action="/home/slider/create" method="post" enctype="multipart/form-data">
							@csrf
							<fieldset>
								<!-- Name input-->
								<div class="form-group">
									<label class="col-md-3 control-label" for="name">Name:</label>
									<div class="col-md-9">
										<input id="name" name="name" type="text" required="" placeholder="Slider name" class="form-control">
									</div>
								</div>
							
								<!-- image input-->
								<div class="form-group">
									<label class="col-md-3 control-label" for="image">Select Image:</label>
									<div class="col-md-9">
										<input type="file" class="form-control" name="image"  accept="image/png, image/jpg, image/jpeg">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="title">Title:</label>
									<div class="col-md-9">
										<input id="title" name="title" type="text" required="" value="#1 Skin Care Center" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="title_1">Title Name:</label>
									<div class="col-md-9">
										<input id="title_1" name="title_1" type="text" required="" placeholder="Slider Title Name" class="form-control">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="image">Description:</label>
									<div class="col-md-9">
										<textarea id="description" name="description" type="text" required="" placeholder="Slider description" class="form-control"></textarea>
									</div>
								</div>
								
								<!-- Form actions -->
								<button type="submit" class="btn btn-primary btn-sm">
													<i class="fa fa-dot-circle-o"></i> Submit
												</button>
												<button type="reset" class="btn btn-danger btn-sm">
													<i class="fa fa-ban"></i> Reset
												</button>
							</fieldset>
						</form>
										</div>
										<div class="card-footer">
											
										</div>
									</div>
								</div>



							</div><!--/.col-->

							@endsection