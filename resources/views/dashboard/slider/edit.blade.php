@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->


								<div class="col-lg-12">
									<div class="container card">
										<div class="card-header">
											<div class="au-breadcrumb-left">
												<span class="au-breadcrumb-span">You are here:</span>
												<ul class="list-unstyled list-inline au-breadcrumb__list">
													<li class="list-inline-item">
														<a href="/home">Home</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item">
														<a href="/home/sliders">Sliders</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item active">Edit</li>
												</ul>
											</div>
										</div>
										@if($errors->any())
										@foreach($errors->all() as $error)
										<ul>
											<li>{{$error}}</li>
										</ul>
										@endforeach
										@endif
										<div class="card-body card-block">
											<form method="POST" action="/home/slider/edit/{{ $slider->id }}" enctype="multipart/form-data">
												@csrf
												<ul style="list-style: none;">

													<li>
														<div class="form-group">
															<label for="name">
																Edit the slider name:
															</label>
															<input type="text" class="form-control" name="name" value='{{$slider->name}}' >
														</div>
													</li>
													<li>
														<div class="form-group">
															<label for="image">
																Select the image:
															</label>
															<input type="file" class="form-control" name="image" accept="image/png, image/jpg, image/jpeg">
															<img src="/uploads/{{$slider->image}}" alt="{{$slider->name}}" >																
														</div>
													</li>
													<li>
														<div class="form-group">
															<label for="title">
																Edit the slider title:
															</label>
															<input type="text" class="form-control" name="title" value='{{$slider->title}}' >
														</div>
													</li>
													<li>
														<div class="form-group">
															<label for="title_1">
																Edit the slider Title Name:
															</label>
															<input type="text" class="form-control" name="title_1" value='{{$slider->title_1}}' >
														</div>
													</li>
													<li>
														<div class="form-group">
									<label class="col-md-3 control-label" for="image">Description:</label>
									<div class="col-md-9">
										<textarea id="description" name="description" type="text" required="" placeholder="Slider description" class="form-control">{{$slider->description}}</textarea>
									</div>
								</div>
													</li>
													<li>
														<div class="form-group">
															<button type="submit" class="btn btn-primary" value="update">Update project</button>
														</div>
													</li>
												</ul>
											</form>
										</div>
										<div class="card-footer">
											
										</div>
									</div>
								</div>



							</div><!--/.col-->

							@endsection