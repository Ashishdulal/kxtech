@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
									<div class="container card">
										<div class="card-header">
											<div class="au-breadcrumb-left">
												<span class="au-breadcrumb-span">You are here:</span>
												<ul class="list-unstyled list-inline au-breadcrumb__list">
													<li class="list-inline-item">
														<a href="/home">Home</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item">
														<a href="/home/team-members">Team Members</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item active">create</li>
												</ul>
											</div>
										</div>
										@if($errors->any())
										@foreach($errors->all() as $error)
										<ul>
											<li>{{$error}}</li>
										</ul>
										@endforeach
										@endif
										<div class="card-body card-block">
					<form class="form-horizontal" action="/home/team-members/create" method="post" enctype="multipart/form-data">
					@csrf
					<fieldset>
						<!-- Name input-->
						<div class="form-group">
							<label class="col-md-3 control-label" for="name">Member Name:</label>
							<div class="col-md-9">
								<input id="ch_name" name="name" type="text" placeholder="Team Member Name"  class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="designation">Member Designation:</label>
							<div class="col-md-9">
								<input id="ch_name" name="designation" type="text" placeholder="Member Designation"  class="form-control">
							</div>
						</div>						
						<div class="form-group">
							<label class="col-md-3 control-label" for="image">Description:</label>
							<div class="col-md-9">
								<textarea id="description" name="description" type="text" required="" placeholder="Your Description" class="form-control"></textarea>
							</div>
						</div>
						<!-- image input-->
						<div class="form-group">
							<label class="col-md-3 control-label" for="image">Select Image:</label>
							<div class="col-md-9">
									<app-image-upload  >
									<div  class="fileinput">
									<input  type="file" name="image"  accept="image/png, image/jpg, image/jpeg">
										</div>
									</app-image-upload>
								</div>
							</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="facebook_link">Facebook Link:</label>
							<div class="col-md-9">
								<input id="facebook_link" name="facebook_link" type="text" value=" https://www.facebook.com/username" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="twitter_link">Twitter Link:</label>
							<div class="col-md-9">
								<input id="twitter_link" name="twitter_link" type="text"  value=" https://www.twitter.com/username" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="instagram_link">Instagram Link:</label>
							<div class="col-md-9">
								<input id="instagram_link" name="instagram_link" type="text"  value=" https://www.facebook.com/username" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="mail">Mail Id:</label>
							<div class="col-md-9">
								<input id="mail" name="mail" type="email" required="" placeholder="Enter mail Id" class="form-control">
							</div>
						</div>
						
						<!-- Form actions -->
						<button type="submit" class="btn btn-primary btn-sm">
							<i class="fa fa-dot-circle-o"></i> Submit
						</button>
						<button type="reset" class="btn btn-danger btn-sm">
							<i class="fa fa-ban"></i> Reset
						</button>
					</fieldset>
				</form>
			</div>
			</div>
			</div>
            <div class="copyright">
            <p>Copyright © 2018 Atlassian. All rights reserved by <a target="_blank" href="https://www.nepgeeks.com">Nepgeeks Technology</a>.</p>
            </div>
			</div><!--/.col-->

							@endsection