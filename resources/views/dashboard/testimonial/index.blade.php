@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
									<div class="container card">
										<div class="card-header">
											<div class="au-breadcrumb-left">
												<span class="au-breadcrumb-span">You are here:</span>
												<ul class="list-unstyled list-inline au-breadcrumb__list">
													<li class="list-inline-item">
														<a href="/home">Home</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item active">
														<a href="/home/testimonials">testimonials</a>
													</li>
												</ul>
											</div>
										</div>
<div class="row m-t-30">
	<div class="col-md-12">
	<a style="margin-bottom: 10px;" href="/home/testimonials/create" class="btn btn-primary">Add New testimonial</a>
		<!-- DATA TABLE-->
		<div class="table-responsive m-b-40">
			<table class="table table-borderless table-data3">
				<thead>
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Description</th>
						<th>Image</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@if(count($testimonials))
					@foreach($testimonials as $testimonial)
					<tr>
						<td>{{$testimonial->id}}</td>
						<td>{{$testimonial->name}}</td>
						<td><?php echo ($testimonial->description ) ?></td>
						<td class="process"><img src="/uploads/{{$testimonial->image}}"></td>
						<td><a href="{{route('testimonial.edit', $testimonial->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>|
				<form method="post" action="{{route('testimonial.delete',$testimonial->id)}}">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
				</form>
				</td>
					</tr>
					@endforeach
					@else
			<tr>
				<td>
					<h5>There are no Testimonials added.</h5>
				</td>
			</tr>
			
			@endif
				</tbody>
			</table>
		</div>
		<!-- END DATA TABLE-->
	</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
			if(! result){
				evt.stopPropagation();
				evt.preventDefault();	
			}
	}
</script>

@endsection