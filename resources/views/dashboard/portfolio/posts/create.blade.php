@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
									<div class="container card">
										<div class="card-header">
											<div class="au-breadcrumb-left">
												<span class="au-breadcrumb-span">You are here:</span>
												<ul class="list-unstyled list-inline au-breadcrumb__list">
													<li class="list-inline-item">
														<a href="/home">Home</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item">
														<a href="/home/portfolio">Portfolio Posts</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item active">create</li>
												</ul>
											</div>
										</div>
										@if($errors->any())
										@foreach($errors->all() as $error)
										<ul>
											<li>{{$error}}</li>
										</ul>
										@endforeach
										@endif
										<div class="card-body card-block">
											<form class="form-horizontal" action="/home/portfolio/create" method="POST" enctype="multipart/form-data">
												@csrf
												<fieldset>
													<!-- Post Title input-->
													<div class="form-group">
														<label class="col-md-3 control-label" for="title">Post Title:</label>
														<div class="col-md-9">
															<input id="title" name="title" type="text" required="" placeholder="Enter the Post Title" class="form-control">
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-3 control-label" for="image">Choose Post Category:</label>
														<div class="col-md-9">
															<select name="cat_id" required class="form-control">

																<option value="">---</option>
																@foreach($portfolioCategories as $category)

																<option value="{{$category->id}}">{{$category->title}}</option>

																@endforeach
															</select>
														</div>
													</div>

													<!-- image input-->
													<div class="form-group">
														<label for="image">Select The Product Image:</label>
														<div class="row">
															<div  class="col-md-4">
																<app-image-upload  >
																	<div  class="fileinput text-center">
																		<input  type="file" name="f_image"  accept="image/png, image/jpg, image/jpeg">
																	</div>
																</app-image-upload>
															</div>
															<div  class="col-md-4">
																<app-image-upload  >
																	<div  class="fileinput text-center">
																		<input  type="file" name="i_image"  accept="image/png, image/jpg, image/jpeg">
																	</div>
																</app-image-upload>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-3 control-label" for="image">Description:</label>
														<div class="col-md-9">
															<textarea id="description" name="description" type="text" required="" placeholder="Post description" class="form-control"></textarea>
														</div>
													</div>

													<!-- Form actions -->
													<button type="submit" class="btn btn-primary btn-sm">
														<i class="fa fa-dot-circle-o"></i> Submit
													</button>
													<button type="reset" class="btn btn-danger btn-sm">
														<i class="fa fa-ban"></i> Reset
													</button>
												</fieldset>
											</form>
										</div>
										<div class="card-footer">
											<div class="copyright">
            <p>Copyright © 2018 Kx Technologies. All rights reserved by <a target="_blank" href="https://www.nepgeeks.com">Nepgeeks Technology</a>.</p>
            </div>
										</div>
									</div>
								</div>



							</div><!--/.col-->

							@endsection