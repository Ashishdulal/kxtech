@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
									<div class="container card">
										<div class="card-header">
											<div class="au-breadcrumb-left">
												<span class="au-breadcrumb-span">You are here:</span>
												<ul class="list-unstyled list-inline au-breadcrumb__list">
													<li class="list-inline-item">
														<a href="/home">Home</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item active">
														portfolio Posts
													</li>
												</ul>
											</div>
										</div>
<div class="row m-t-30">
	<div class="col-md-12">
	<a style="margin-bottom: 10px;" href="/home/portfolio/create" class="btn btn-primary">Add New portfolio</a>
		<!-- DATA TABLE-->
		<div class="table-responsive m-b-40">
			@foreach ($portfolioCategories as $post_cat)
				<h3>{{$post_cat->title}}</h3>
				<table class="table table-hover make-gap">

					<thead>
						<tr>
							<th>Id</th>
							<th>Title</th>
							<th>Description</th>
							<th>Images</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($portfolios))
						@foreach($portfolios as $portfolio)
						@if( $post_cat->id === $portfolio->cat_id)
						<tr>
							<td>{{$portfolio->id}}.</td>
							<td>{{$portfolio->title}}</td>
							<td><?php echo ($portfolio->description)?></td>
							<td class="work-img">
								<img src="/uploads/{{$portfolio->f_image}}" alt="{{$portfolio->title}}">
								<img src="/uploads/{{$portfolio->i_image}}" alt="{{$portfolio->title}}">
							</td>
							<td>
								<a href="/home/portfolio/edit/{{$portfolio->id}}" class="btn btn-primary make-btn">
								Edit
							</a>|
								<form action="{{route('delete-portfolio.post',$portfolio->id)}}">
									<button type="submit" class="btn btn-danger" onclick="makeWarning(event)">Delete</button>
								</form>
							</td>
						</tr>
						@endif
						@endforeach
						@else
			<tr>
				<td>
					<h5>There are no Posts in Portfolio.</h5>
				</td>
			</tr>
			
			@endif
					</tbody>
				</table>
				@endforeach
		</div>
		<!-- END DATA TABLE-->
	</div>
</div>
</div>
</div>
<div class="copyright">
            <p>Copyright © 2018 Kx Technologies. All rights reserved by <a target="_blank" href="https://www.nepgeeks.com">Nepgeeks Technology</a>.</p>
            </div>
</div>
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
			if(! result){
				evt.stopPropagation();
				evt.preventDefault();	
			}
	}
</script>

@endsection