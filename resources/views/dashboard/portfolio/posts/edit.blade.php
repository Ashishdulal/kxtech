@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
									<div class="container card">
										<div class="card-header">
											<div class="au-breadcrumb-left">
												<span class="au-breadcrumb-span">You are here:</span>
												<ul class="list-unstyled list-inline au-breadcrumb__list">
													<li class="list-inline-item">
														<a href="/home">Home</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item">
														<a href="/home/portfolios">Blog Post</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item active">Edit</li>
												</ul>
											</div>
										</div>
										@if($errors->any())
										@foreach($errors->all() as $error)
										<ul>
											<li>{{$error}}</li>
										</ul>
										@endforeach
										@endif
										<div class="card-body card-block">
											<form method="POST" action="/home/portfolio/edit/{{ $portfolios->id }}" class="form-group" enctype="multipart/form-data">

					{{csrf_field()}}

					<div class="form-group">
						<label for="title">
							Edit The Post Title:
						</label>
						<input type="text" name="title" required class="form-control" value="{{$portfolios->title}}">
					</div>
					<div class="form-group">
						<label for="cat_id">
							Choose The Post Category:
						</label>
						<select name="cat_id" class="form-control">


							@foreach($portfolioCategories as $Postcat)
							@if($Postcat->id == $portfolios->cat_id)
							<option value="{{$Postcat->id}}">{{$Postcat->title}}</option>
							@endif
							@endforeach


							@foreach($portfolioCategories as $Postcat)
							@if($Postcat->id != $portfolios->cat_id)
							<option value="{{$Postcat->id}}">{{$Postcat->title}}</option>
							@endif
							@endforeach
						</select>
					</div>
					<!-- image input-->
					<div class="form-group">
						<label for="image">Select The Post Image:</label>
						<div class="row">
							<div  class="col-md-6">
								<app-image-upload  >
									<div  class="fileinput text-center">
										<div  class="thumbnail img-raised">
											<img  alt="..." src="/uploads/{{$portfolios->f_image}}">
										</div>
										<div >
											<!----><button  class="btn btn-raised btn-round btn-default btn-file ng-star-inserted"> Select image </button><!---->
											<!----></div><input  type="file" name="f_image"  accept="image/png, image/jpg, image/jpeg">
										</div>
									</app-image-upload>
								</div>
								<div  class="col-md-6">
									<app-image-upload  >
										<div  class="fileinput text-center">
											<div  class="thumbnail img-raised">
												<img  alt="..." src="/uploads/{{$portfolios->i_image}}">
											</div>
											<div >
												<!----><button  class="btn btn-raised btn-round btn-default btn-file ng-star-inserted"> Select image </button><!---->
												<!----></div><input  type="file" name="i_image"  accept="image/png, image/jpg, image/jpeg">
											</div>
										</app-image-upload>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="image">Description:</label>
								<div class="col-md-9">
									<textarea id="description" name="description" type="text" required="" class="form-control"> {{$portfolios->description}}</textarea>
								</div>
							</div>


							<div class="form-group">
								<button type="submit" class="btn btn-primary">Update Post</button>
							</div>
						</form>
										</div>
										<div class="card-footer">
											<div class="copyright">
            <p>Copyright © 2018 Kx Technologies. All rights reserved by <a target="_blank" href="https://www.nepgeeks.com">Nepgeeks Technology</a>.</p>
            </div>
										</div>
									</div>
								</div>



							</div><!--/.col-->

							@endsection