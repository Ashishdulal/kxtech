@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
									<div class="container card">
										<div class="card-header">
											<div class="au-breadcrumb-left">
												<span class="au-breadcrumb-span">You are here:</span>
												<ul class="list-unstyled list-inline au-breadcrumb__list">
													<li class="list-inline-item">
														<a href="/home">Home</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item">
														<a href="/home/portfolio-category">Portfolio Categories</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item active">Edit</li>
												</ul>
											</div>
										</div>
										@if($errors->any())
										@foreach($errors->all() as $error)
										<ul>
											<li>{{$error}}</li>
										</ul>
										@endforeach
										@endif
										<div class="card-body card-block">
										<form method="post" action="/home/portfolio-category/edit/{{ $portfolioCategories->id }}">

					{{csrf_field()}}

					<div class="form-group">

						<label for="title">
							Edit The Product Title:
						</label>
						<input type="text" name="title" class="form-control" value="{{ $portfolioCategories->title }}">

					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Update Product</button>
					</div>
				</form>
										</div>
										<div class="card-footer">
											<div class="copyright">
            <p>Copyright © 2018 Kx Technologies. All rights reserved by <a target="_blank" href="https://www.nepgeeks.com">Nepgeeks Technology</a>.</p>
            </div>
										</div>
									</div>
								</div>



							</div><!--/.col-->

							@endsection