@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
									<div class="container card">
										<div class="card-header">
											<div class="au-breadcrumb-left">
												<span class="au-breadcrumb-span">You are here:</span>
												<ul class="list-unstyled list-inline au-breadcrumb__list">
													<li class="list-inline-item">
														<a href="/home">Home</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item">
														<a href="/home/portfolio">Portfolio</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item active">Portfolio Categories</li>
												</ul>
											</div>
										</div>
<div class="row m-t-30">
	<div class="col-md-12">
	<a style="margin-bottom: 10px;" href="/home/portfolio-category/create" class="btn btn-primary">Add New Portfolio Category</a>
		<!-- DATA TABLE-->
		<div class="table-responsive m-b-40">
			<table class="table table-borderless table-data3">
					<table class="table table-hover">
					<tr>
						<th>S. NO.</th>
						<th>Blog Category</th>
						<th>Action</th>
					</tr>

					@if(count($portfolioCategories))
					@foreach ($portfolioCategories as $portfolioCategory)
					<tr>
						<td>{{$portfolioCategory->id}}</td>
						<td>{{$portfolioCategory->title}}</td>
						<td><a href="/home/portfolio-category/edit/{{$portfolioCategory->id}}"> <button class="btn btn-info make-btn">Edit</button></a> |
							<form method="delete" action="{{route('delete.portfolio',$portfolioCategory->id)}}"> 

								{{csrf_field()}}

								{{method_field('DELETE')}}
								<div class="field"><button class="btn btn-danger" onclick="makeWarning(event)">Delete</button></div>
							</form>

						</td>
					</tr>
					@endforeach
					@else
			<tr>
				<td>
					<h5>There are no Portfolio Categories added.</h5>
				</td>
			</tr>
			
			@endif
				</table>
										</div>
										<div class="card-footer">
											<div class="copyright">
            <p>Copyright © 2018 Kx Technologies. All rights reserved by <a target="_blank" href="https://www.nepgeeks.com">Nepgeeks Technology</a>.</p>
            </div>
										</div>
									</div>
								</div>



							</div><!--/.col-->
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
			if(! result){
				evt.stopPropagation();
				evt.preventDefault();	
			}
	}
</script>
							@endsection