@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
									<div class="container card">
										<div class="card-header">
											<div class="au-breadcrumb-left">
												<span class="au-breadcrumb-span">You are here:</span>
												<ul class="list-unstyled list-inline au-breadcrumb__list">
													<li class="list-inline-item">
														<a href="/home">Home</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item active">
														Blog Posts
													</li>
												</ul>
											</div>
										</div>
<div class="row m-t-30">
	<div class="col-md-12">
	<a style="margin-bottom: 10px;" href="/home/blog/create" class="btn btn-primary">Add New Blog</a>
		<!-- DATA TABLE-->
		<div class="table-responsive m-b-40">
			@foreach ($blogcategories as $blog_cat)
				<h3>{{$blog_cat->title}}</h3>
				<table class="table table-hover make-gap">

					<thead>
						<tr>
							<th>Id</th>
							<th>Title</th>
							<th>Description</th>
							<th>Images</th>
							<th>Action</th>
							<th>Author</th>
						</tr>
					</thead>
					<tbody>
						@if(count($blogs))
						@foreach($blogs as $blog)
						@if( $blog_cat->id === $blog->cat_id)
						<tr>
							<td>{{$blog->id}}.</td>
							<td>{{$blog->title}}</td>
							<td><?php echo ($blog->description)?></td>
							<td class="work-img">
								<img src="/uploads/{{$blog->f_image}}" alt="{{$blog->title}}">
								<img src="/uploads/{{$blog->i_image}}" alt="{{$blog->title}}">
							</td>
							<td>
								<a href="/home/blog/edit/{{$blog->id}}" class="btn btn-primary make-btn">
								Edit
							</a>|
								<form action="{{route('delete-post.blog',$blog->id)}}">
									<button type="submit" class="btn btn-danger" onclick="makeWarning(event)">Delete</button>
								</form>
							</td>
							<td><p>Admin</p></td>
						</tr>
						@endif
						@endforeach
						@else
			<tr>
				<td>
					<h5>There are no posts in Blog.</h5>
				</td>
			</tr>
			
			@endif
					</tbody>
				</table>
				@endforeach
		</div>
		<!-- END DATA TABLE-->
	</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
			if(! result){
				evt.stopPropagation();
				evt.preventDefault();	
			}
	}
</script>

@endsection