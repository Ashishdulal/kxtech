@extends('layouts.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
									<div class="container card">
										<div class="card-header">
											<div class="au-breadcrumb-left">
												<span class="au-breadcrumb-span">You are here:</span>
												<ul class="list-unstyled list-inline au-breadcrumb__list">
													<li class="list-inline-item">
														<a href="/home">Home</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item">
														<a href="/home/blog-category">Blog Categories</a>
													</li>
													<li class="list-inline-item seprate">
														<span>/</span>
													</li>
													<li class="list-inline-item active">Edit</li>
												</ul>
											</div>
										</div>
										@if($errors->any())
										@foreach($errors->all() as $error)
										<ul>
											<li>{{$error}}</li>
										</ul>
										@endforeach
										@endif
										<div class="card-body card-block">
										<form method="post" action="/home/blog-category/edit/{{ $blogcategories->id }}">

					{{csrf_field()}}

					<div class="form-group">

						<label for="title">
							Edit The Product Title:
						</label>
						<input type="text" name="title" class="form-control" value="{{ $blogcategories->title }}">

					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Update Product</button>
					</div>
				</form>
										</div>
										<div class="card-footer">
											
										</div>
									</div>
								</div>



							</div><!--/.col-->

							@endsection