@extends('layouts.main.main')

@section('content')

<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-speed="0.8"></div>
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title banner">CAREER</div>
						<div class="home_text">Welcome to our company.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Intro -->

<div class="intro_carrier">
	<div class="container">
		<div class="row">

			<!-- Intro Content -->
			<div class="col-lg-12">
				<div class="intro_content">
					<div class="section_title_container make-center">
						<div class="section_title"><h2>kX Technologies is for everyone</h2></div>
					</div>
					<div class="cta_text">
						We are always looking for smart people like YOU, who have a passion for learning and implementing innovative ideas. At Kx Technologies we are a group of an innovative team who help each other improve in a collaborative environment. We strongly believe Our Teams are our Key to success. We consider having a helpful and friendly working environment while taking our work very seriously.
					</div>
					<div style="width: 20%;" class="button button_a ml-auto mr-auto make-center"><a href="/jobs">Browse All Jobs</a></div>

				</div>
			</div>
		</div>
	</div>

	<div class="other-carrier">
	<div class="home_overlay"><img src="images/home_overlay_next.png" alt=""></div>
		<div class="carrier_container">
		<div class="container">
			<div class="row">
			<div class="col-sm-3">
				<div class="section_title"><h4>Our teams</h4></div>
				<div class="cta_text">
						We love all teams. Explore ours and find the perfect fit for you.
					</div>
					<div style="width: 60%;" class="button_a carrier_setting"><a href="/jobs">Browse All Jobs</a>
					</div>
			</div>
			<div class="col-sm-3">
				<div class="component-textblock">
					<div class="component-right" style="padding-left:40px; list-style: none;">
						<p><li><a href="/jobs">General & Admin</a></li></p>
						<p><li><a href="#">Data & Analytics</a></li></p> 
						<p><li><a href="#">Design</a></li></p> 
						<p><li><a href="#">Engineering</a></li></p> 
					</div> 
				</div> 
			</div>
			<div class="col-sm-3">
				<div class="component-textblock">
					<div class="component-right" style="padding-left:40px; list-style: none;">
						<p><li><a href="/jobs">Marketing & Sales</a></li></p>
						<p><li><a href="#">Product Management</a></li></p> 
						<p><li><a href="#">Graduates</a></li></p> 
						<p><li><a href="#">Interns</a></li></p> 
						<p><li><a href="#">Support</a></li></p> 
					</div> 
				</div>
			</div>
			<div class="col-sm-3 component-right">
				<div class="section_title"><h4><i class="fa fa-trello" aria-hidden="true"></i>&nbsp;Trello</h4></div>
				<a href="/jobs">Browse Trello team jobs</a>
				
			</div>
		</div>
		</div>
	</div>
</div>

<div class="countries_carrier make-center component-right">
	<div class="section_title"><h4>Locations</h4></div>
	<div class="cta_text">Amazing cities? Check. Totally tricked out offices? Done. Explore the world of kX Technologies.</div>
	<a href="/jobs"><i class="fa fa-play-circle"style="font-size:24px;margin-top: 20px;" aria-hidden="true"></i>&nbsp;Learn more about ‘Life at kX Technologies’</a>
	</div>
	<div class="container all_country-width">
	<div class="row all_countries_carrier component-right">
		<div class="col-sm-3">
			<a href="">
			<div class="carrier_images">
				<img src="/images/San Francisco.png" alt="hello">
				<span>America</span>
			</div></a>
		</div>
		<div class="col-sm-3">
			<a href="">
			<div class="carrier_images">
				<img src="/images/Sydney.png" alt="hello">
				<span>Australia</span>
			</div></a>
		</div><div class="col-sm-3">
			<a href="">
			<div class="carrier_images">
				<img src="/images/Manila.png" alt="hello">
				<span>Canada</span>
			</div></a>
		</div><div class="col-sm-3">
			<a href="">
			<div class="carrier_images">
				<img src="/images/Austin.png" alt="hello">
				<span>Japan</span>
			</div>
		</a>
		</div><div class="col-sm-3">
			<a href="">
			<div class="carrier_images">
				<img src="/images/Sydney.png" alt="hello">
				<span>Malaysia</span>
			</div></a>
		</div><div class="col-sm-3">
			<a href="">
			<div class="carrier_images">
				<img src="/images/San Francisco.png" alt="hello">
				<span>Dubai</span>
			</div></a>
		</div><div class="col-sm-3">
			<a href="">
			<div class="carrier_images">
				<img src="/images/Amsterdam.png" alt="hello">
				<span>Nepal</span>
			</div></a>
		</div><div class="col-sm-3">
			<a href="">
			<div class="carrier_images">
				<img src="/images/Mountain View.png" alt="hello">
				<span>Indonesia</span>
			</div>
		</a>
		</div>
	</div>
</div>
<section>
	<div class="other-carrier">
	<div class="home_overlay"><img src="images/home_overlay_next.png" alt=""></div>
		<div class="carrier_container">
		<div class="container">
			<div class="row">
			<div class="col-sm-3">
				<div class="section_title"><h4>Looking for remote opportunities?</h4></div>
				<div class="cta_text">
						Find them here.
					</div>
					<div style="width: 60%;" class="button_a carrier_setting"><a href="/jobs">Browse All Jobs</a>
					</div>
			</div>
			<div class="col-sm-9">
				<div class="cta_text">
						More and more we're looking for ways to open opportunities to remote workers.
					</div><br>
					<div class="cta_text">
						While all kX Technologies jobs are tagged to a specific location, you'll notice some roles will have "Remote" in the title meaning these roles are available to those who'd prefer to work remotely.
					</div>
			</div>
			
		</div>
		</div>
	</div>
</div>
</section>
<section>
	<!-- Why Choose Us -->

	<div class="why_carrier my-border">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 carrier_image">
					<div class="homepage_border carrier_edit">
						<!-- Blog Post -->
						<div class="carrierPost_image"><img src="images/BWHAB@2x.png" alt=""></div>
						<div class="blog_post_homepage">
							<div class="portfolio_title"><h4>Our values</h4></div>
							<div class="blog_post_info">
								<ul class="d-flex flex-row">

								</ul>
							</div>
							<div class="cta_text">
						Learn about the values that guide our business, our product development, and our brand. As our company continues to evolve and grow, these five values remain constant.
					</div>
					<a href="#"><i class="fa fa-play-circle"style="font-size:24px;margin-top: 20px;" aria-hidden="true"></i>&nbsp;Watch Values video</a><br>
							<div class="component component--link-button">
								<a href="#" class="component__link link-arrow"> Learn More &nbsp;<svg class="link-arrow-image" width="11px" height="8px" viewBox="0 0 11 8" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<g class="group-path" transform="translate(-138.000000, -586.000000)" fill="#0052CC">
											<path d="M145.2803,586.507862 L144.2193,587.568863 L145.9393,589.287862 L138.7503,589.287862 C138.3363,589.287862 138.0003,589.623862 138.0003,590.037862 C138.0003,590.451862 138.3363,590.787862 138.7503,590.787862 L145.9393,590.787862 L144.2193,592.507862 L145.2803,593.568863 L148.8103,590.037862 L145.2803,586.507862 Z" id="Fill-1"></path></g></g></svg> </a> </div>
										</div>
									</div>
								</div>

								<div class="col-sm-4 carrier_image">
					<div class="homepage_border carrier_edit">
						<!-- Blog Post -->
						<div class="carrierPost_image"><img src="images/Clipboard List@2x.png" alt=""></div>
						<div class="blog_post_homepage">
							<div class="carrier_title"><h4>How well do your values align with ours?</h4></div>
							<div class="blog_post_info">
								<ul class="d-flex flex-row">

								</ul>
							</div>
							<div class="cta_text">
						When we're all on the same page, teamwork thrives. So, we take extra care to make sure every kX Technologies understands and utilizes our values. Take this 5 minute quiz to see how you’d fit in.
					</div><br>
							<div class="component component--link-button">
								<a href="#" class="component__link link-arrow"> Start quiz &nbsp;<svg class="link-arrow-image" width="11px" height="8px" viewBox="0 0 11 8" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<g class="group-path" transform="translate(-138.000000, -586.000000)" fill="#0052CC">
											<path d="M145.2803,586.507862 L144.2193,587.568863 L145.9393,589.287862 L138.7503,589.287862 C138.3363,589.287862 138.0003,589.623862 138.0003,590.037862 C138.0003,590.451862 138.3363,590.787862 138.7503,590.787862 L145.9393,590.787862 L144.2193,592.507862 L145.2803,593.568863 L148.8103,590.037862 L145.2803,586.507862 Z" id="Fill-1"></path></g></g></svg> </a> </div>
										</div>
									</div>
								</div>

		<div class="col-sm-4 carrier_image">
					<div class="homepage_border carrier_edit">
						<!-- Blog Post -->
						<div class="carrierPost_image"><img src="images/greatplace.png" alt=""></div>
						<div class="blog_post_homepage">
							<div class="portfolio_title"><h4>Our gold stars</h4></div><br><br>
							<div class="blog_post_info">
								<ul class="d-flex flex-row">
								</ul>
							</div>
							<div class="portfolio_title"><h6 style="font-weight: 700;">A Best Place to Work</h6></div>
							<div class="cta_text">
						Australia  |  2012-2018
					</div><br>
					<div class="portfolio_title"><h6 style="font-weight: 700;">A Best Place to Work</h6></div>
							<div class="cta_text">
						USA  |  2012-2019
					</div><br>
					<div class="portfolio_title"><h6 style="font-weight: 700;">A Best Place to Work</h6></div>
							<div class="cta_text">
						Asia  |  2014-2018
					</div><br>
					<div class="portfolio_title"><h6 style="font-weight: 700;">A Best Place to Work</h6></div>
							<div class="cta_text">
						Netherlands  |  2016, 2018
					</div>
					 </div>
				</div>
				</div>
			</div>

</section>
<section>
	<div class="cta-1 carrier_background my-border">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="cta_section">
							<div class="cta_content">
								<div class="cta_title carrier_text">Refer a mate, score some cash</div>
								<div class="cta_text carrier_text">Why wait? Let’s Connect and Start a Project Today</div>
								<div class="button button_a ml-auto mr-auto cta_button_carrier"><a href="/expertises#contact">Refer Now</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>
</div>
	@endsection