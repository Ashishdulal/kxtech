	@extends('layouts.main.main')

	@section('content')
	<div class="home">

		<!-- Home Slider -->
		<div class="home_slider_container">
			<div class="owl-carousel owl-theme home_slider">

				<!-- Slide -->
				@if(count($sliders))
				@foreach($sliders as $slider)
				<div class="owl-item">
					<div class="background_image" style="background-image:url(uploads/{{{$slider->image}}})"></div>
					<div class="home_container">
						<div class="container">
							<div class="row">
								<div class="col">
									<div class="home_content1">
										<div class="home_title">{{$slider->title}}</div>
										<div class="home_subtitle">{{$slider->title_1}}</div>
										<div class="home_text">
											<p><?php echo ($slider->description)?></p>
										</div>
										<div class="home_buttons align-items-center justify-content-start">
											<div class="button button_1 trans_200 home-button"><a href="/expertises">Read More</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				@else
				<div class="make-center">
					<h5 style="margin: 100px auto;">There are no Sliders.</h5>
				</div>
				@endif

			</div>

			<!-- Home Slider Dots -->

			<div class="home_slider_dots">
				<ul id="home_slider_custom_dots" class="home_slider_custom_dots d-flex flex-row align-items-center justify-content-start">
					@foreach($sliders as $slider)
					<li class="home_slider_custom_dot trans_200 active"></li>
					@endforeach
				</ul>
			</div>

		</div>

	</div>
<!-- 	<div style=" background-image: url('/images/worklife-image.png');" class="row flex-container component--image">
		<div class="col col-md-5 xs-none hide-md hide-lg hide-xl work-life-cropped text-center">
			<div class="component component--image">
				<img src="https://wac-cdn.atlassian.com/dam/jcr:599c8e1b-cebe-453f-b20e-0f45c4a31fd4/h2-worklife-image@2x.png?cdnVersion=354" alt="Jira Align logo" class="component__image" style="height:300px;">
			</div>
		</div>
		<div class="col col-md-8 col-lg-6 col-md-offset-4 vertical-middle">
			<div class="component component--heading"> 
				<h6 class="eyebrows heading">Introducing</h6> 
			</div>
			<div class="component component--image-heading-textblock"> 
				<div class="component__image">
					<img src="https://wac-cdn.atlassian.com/dam/jcr:712d7229-efc7-4668-8586-130a54b419b3/worklife-dark.svg?cdnVersion=354" alt="Work Life" class="component__image" style="height:25px;">
				</div>
				<div class="component__heading-textblock">
					<p>Whether you live to work or work to live, our new publication is your guide to getting the most from your 9-to-5. Get inspiring stories and useful resources from the team at Demo.</p>
				</div> 
			</div> 
			<div class="component component--link-button">
				<a href="#" class="component__link button button--one button--small button--secondary" data-data-track-event="homepage-blog-banner"> Check it out </a>
			</div>
		</div>
	</div> -->
	<!-- Services -->

	<div class="services">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title_container">
						<div class="section_title"><h2>Our Focus</h2></div>
						<div class="section_subtitle">Using Latest Technology Available, We Make Your Ideas Come Into Work.</div>
					</div>
				</div>
			</div>
			<div class="row services_row">
				@if(count($focus))

				<?php $counter=0; ?>
				<!-- Service -->
				@foreach($focus as $foci)
				@if($counter <= 5)
				<div class="col-xl-4 col-md-6 service_col">
					<div class="service my-style">
						<div class="service">
							<div class="service-image">
								<img style="max-width: 100%; height: 200px;" src="/uploads/{{$foci->image}}">
							</div>
							<div style="text-transform: uppercase;" class="service_title"><a href="/focus-detail/{{$foci->id}}">{{$foci->name}}</a></div>
							<div class="service_text my-overflow">
								<p><?php echo ($foci->description)?></p>
							</div>
							<div class="component component--link-button">
								<a href="/focus-detail/{{$foci->id}}" class="component__link link-arrow"> Learn more &nbsp;<svg class="link-arrow-image" width="11px" height="8px" viewBox="0 0 11 8" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<g class="group-path" transform="translate(-138.000000, -586.000000)" fill="#0052CC">
											<path d="M145.2803,586.507862 L144.2193,587.568863 L145.9393,589.287862 L138.7503,589.287862 C138.3363,589.287862 138.0003,589.623862 138.0003,590.037862 C138.0003,590.451862 138.3363,590.787862 138.7503,590.787862 L145.9393,590.787862 L144.2193,592.507862 L145.2803,593.568863 L148.8103,590.037862 L145.2803,586.507862 Z" id="Fill-1"></path></g></g></svg> </a> </div>
										</div>
									</div>
								</div>
								@endif
								<?php $counter++;?>
								@endforeach
								@else
								<h5>There are no Focus items added.</h5>
								@endif
							</div>
						</div>
					</div>
					<!--Call to action -->

					<div class="cta">
						<div class="container">
							<div class="row">
								<div class="col">
									<div class="cta_section">
										<div class="cta_content">
											<div class="cta_title ">Get started with our products, for free</div>
											<div class="cta_text ">Contact us to get a free quotation and a Demo Of our product .</div>
											<div class="button button_a ml-auto mr-auto cta_button"><a href="/contact">Get started for free</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Why Choose Us -->

					<div class="cta-1 why_us">
						<!-- <div class="background_image" style="background-image:url(images/why.jpg)"></div> -->
						<div class="container">
							<div class="row row-eq-height">

								<!-- Why Choose Us Image -->
								<div class="col-lg-6 order-lg-1 order-2">
									<div class="why_image_container">
										<div class="why_image"><img src="{{asset('/images/playsillustrationclean.png')}}" alt=""></div>
									</div>
								</div>

								<!-- Why Choose Us Content -->
								<div class="col-lg-6 order-lg-2 order-1">
									<div class="why_content">
										<div class="section_title_container">
											<div class="section_title"><h3>Why choose us?</h3></div>
										</div>
										<div class="why_text">
											<p>Kx Technology is a group of experts committed in fulfilling your digital requirements. Our Goal is to meet your expectations and provide you the best solution. We have innovative and passionate team members dedicated toward delivering quality service. We help you get your desired work efficiently. Connect with us today and move towards achieving Excellence.</p>
										</div>
										<div class="why_list">
											<ul>

												<!-- Why List Item -->
												<li class="d-flex flex-row align-items-center justify-content-start">
													<div class="icon_container d-flex flex-column align-items-center justify-content-center">
														<div class=""><i class="fa fa-bar-chart" aria-hidden="true"></i></div>
													</div>
													<div class="why_list_content">
														<div class="why_list_title">Only Top Products</div>
														<div class="why_list_text">We produce the best in market outcomes. Our teams are always dedicated towards product quality.</div>
													</div>
												</li>

												<!-- Why List Item -->
												<li class="d-flex flex-row align-items-center justify-content-start">
													<div class="icon_container d-flex flex-column align-items-center justify-content-center">
														<div class=""><i class="fa fa-apple" aria-hidden="true"></i></div>
													</div>
													<div class="why_list_content">
														<div class="why_list_title">The best Apps</div>
														<div class="why_list_text">We make sure that our Apps meets your requirement in every way and produce the best outcome.</div>
													</div>
												</li>

												<!-- Why List Item -->
												<li class="d-flex flex-row align-items-center justify-content-start">
													<div class="icon_container d-flex flex-column align-items-center justify-content-center">
														<div class=""><i class="fa fa-comments-o" aria-hidden="true"></i></div>
													</div>
													<div class="why_list_content">
														<div class="why_list_title">Great Feedback</div>
														<div class="why_list_text">We are all ears whenever you have something to say. Either Support or Feedback We are always here.</div>
													</div>
												</li>

											</ul>
										</div><br>
									</div>
								</div>
							</div>
						</div>
						<h2></h2>
					</div>


					<!-- Call to action -->

					<div class="cta-1">
						<div class="container">
							<div style="text-align: center;" class="cta_title">Our Areas Of Expertise</div>
							<div class="owl-carousel owl-theme home_slider">

								<div class="">
									<div class="row my-partners">
										<div class="col-md-2 col-xs-4 thumbnail"><img src="/expertise/laravellogo.png" alt="Image" ></div>
										<div class="col-md-2 col-xs-4 thumbnail"><img src="/expertise/css_logo4.png" alt="Image" ></div>
										<div class="col-md-2 col-xs-4 thumbnail"><img src="/expertise/htmlicon.png" alt="Image" ></div>
										<div class="col-md-2 col-xs-4 thumbnail"><img src="/expertise/illustrator.png" alt="Image" ></div>
										<div class="col-md-2 col-xs-4 thumbnail"><img src="/expertise/indesign.png" alt="Image" ></div>
										<div class="col-md-2 col-xs-4 thumbnail"><img src="/expertise/ionic-logo-portrait.png" alt="Image" ></div>
									</div>
								</div>

								<!-- Carousel items -->                    
								<div class="">
									<div class="row my-partners">
										<div class="col-md-2 col-xs-4 thumbnail"><img src="/expertise/javascripticon.png" alt="Image" ></div>
										<div class="col-md-2 col-xs-4 thumbnail"><img src="/expertise/photoshop-express.png" alt="Image" ></div>
										<div class="col-md-2 col-xs-4 thumbnail"><img src="/expertise/php-file.png" alt="Image" ></div>
										<div class="col-md-2 col-xs-4 thumbnail"><img src="/expertise/Python-Logo.png" alt="Image" ></div>
										<div class="col-md-2 col-xs-4 thumbnail"><img src="/expertise/seo-64.png" alt="Image" ></div>
										<div class="col-md-2 col-xs-4 thumbnail"><img src="/expertise/vue-js.png" alt="Image" ></div>

									</div><!--.row-->
								</div><!--.item-->                 
							</div>
						</div>
					</div>


					<!-- Team -->

<!-- <div class="team">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="section_title_container text-center">
					<div class="section_subtitle">This is kX Technologies</div>
					<div class="section_title"><h2>Our Team Members</h2></div>
				</div>
			</div>
		</div>
		<div class="row team_row">

			@foreach($doctor as $doctor)

			<a class="team_row" data-toggle="modal" data-target="#team-membermodal{{$doctor->id}}" href="#">
				<div class="col-lg-4 team_col">
					<div class="team_item text-center d-flex flex-column aling-items-center justify-content-end">
						<div class="team_image"><img src="uploads/{{$doctor->image}}" alt="{{$doctor->name}}"></div>
						<div class="team_content text-center">
							<div class="team_name"><a data-toggle="modal" data-target="#team-membermodal{{$doctor->id}}" href="#">{{$doctor->name}}</a></div>
							<div class="team_title">{{$doctor->designation}}</div>
							<div class="team_text my-overflow">
								<p><?php echo ($doctor->description)?></p>
							</div>
						</div>
					</div>
				</div>
			</a>
			<div class="modal fade" id="team-membermodal{{$doctor->id}}" tabindex="-1" role="dialog" aria-labelledby="team-membermodal5Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">Atlassian</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="uploads/{{$doctor->image}}" alt="{{$doctor->name}}">
										</div>
										<ul class="portfolio-social">
											<li><a href="{{$doctor->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="{{$doctor->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="{{$doctor->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:{{$doctor->mail}}" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>{{$doctor->name}}</h4>
											<h2>{{$doctor->designation}}</h2>
											<p align="justify"><?php echo ($doctor->description)?></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach

		</div>
	</div>
</div>
-->
<!-- Why Choose Us -->
 
<!-- 						<div class="services">
							<div class="container">
								<div class="row">
												<div class="col text-center">
													<div class="section_title_container">
														<div class="section_title"><h2>Our Portfolios</h2></div>
														<div class="section_subtitle">This is kX Technologies</div>
													</div>
												</div>
											</div>
								<div class="row services_row">
									<?php $count=1; ?>
									@foreach($portfolios as $portfolio)
									<div class="col-sm-4 homepage_blog">
										<div class="homepage_border">

											<div class="blog_post_image"><img style="width:100%;height:230px;" src="uploads/{{$portfolio-> f_image}}" alt=""></div>
											<div class="blog_post_homepage">
												<div class="blog_post_title"><h4>{{$portfolio->title}}</h4></div>
												<div class="blog_post_info">
													<ul class="d-flex flex-row">
														<li>In : <a >
															@foreach($portfolioCategories as $blogcategory)
															@if($portfolio->cat_id === $blogcategory->id)
															{{$blogcategory->title}}
															@endif
															@endforeach
														</a></li>
													</ul>
												</div>
												<div class="blog_post_text">
													<p><?php echo ($portfolio -> description)?></p>
												</div>
												<div class="component component--link-button">
													<a href="/blog-detail/{{$portfolio->id}}" class="component__link link-arrow"> Read their story &nbsp;<svg class="link-arrow-image" width="11px" height="8px" viewBox="0 0 11 8" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<g class="group-path" transform="translate(-138.000000, -586.000000)" fill="#0052CC">
																<path d="M145.2803,586.507862 L144.2193,587.568863 L145.9393,589.287862 L138.7503,589.287862 C138.3363,589.287862 138.0003,589.623862 138.0003,590.037862 C138.0003,590.451862 138.3363,590.787862 138.7503,590.787862 L145.9393,590.787862 L144.2193,592.507862 L145.2803,593.568863 L148.8103,590.037862 L145.2803,586.507862 Z" id="Fill-1"></path></g></g></svg> </a> </div>
															</div>
														</div>
													</div>
													<?php $count ++; 
												if($count > 6)
													break;
												?>
													@endforeach
												</div>
											</div>
										</div> -->
									
									<!-- Extra -->
									<!-- Services -->

<!-- 									<div class="services">
										<div class="container">
											<div class="row">
												<div class="col text-center">
													<div class="section_title_container">
														<div class="section_title"><h2>Our Services</h2></div>
														<div class="section_subtitle">This is kX Technologies</div>
													</div>
												</div>
											</div>
											<div class="row services_row">
												
												<?php $count=1; ?>
												@if(count($services))
												@foreach($services as $service)
												<div class="col-xl-4 col-md-6 service_col">
													<div class="service text-center my-style">
														<div class="service">
															<a href="/services-detail/{{$service->id}}">
																<div class="icon_container1 d-flex flex-column align-items-center justify-content-center ml-auto mr-auto">
																	<img style="max-width: 100%;" src="/uploads/{{$service->image}}">
																</div>
																<div style="text-transform: uppercase;" class="service_title">{{$service->name}}</div>
																<div class="service_text my-overflow">
																	<p><?php echo ($service->description)?></p>
																</div>
															</a>
														</div>
													</div>
												</div>
												<?php $count ++; 
												if($count > 6)
													break;
												?>
												@endforeach
												@else
												<h5>There are no posts in Services.</h5>
												@endif
											</div>
										</div>
									</div> -->
<!-- 									<div class="container">
										<div class="row reduced-row">
											<div class="col col-md-12 text-center">
												<div class="component-text"> 
													<h2 class="cta_title">Recommended reading</h2>
													<p class="cta_text">Learn more about industry best practices, keep up with recent product updates, and catch the latest news from Demo.</p> 
												</div>
											</div> 
											<div class="col col-md-4">
												<div class="component-textblock">
													<div class="component-right" style="padding-left:40px;">
														<h4 class="heading"><i class="fa fa-book" aria-hidden="true"></i>&nbsp;Amazing content</h4>
														<p><li><a href="/team-playbook">Demo Team Playbook</a></li></p>
														<p><li><a href="#">Agile development</a></li></p> 
														<p><li><a href="#">Git tutorials</a></li></p> 
														<p><li><a href="#">Service management</a></li></p> 
														<p><li><a href="#">Continuous delivery</a></li></p> 
													</div> 
												</div> 
											</div> 
											<div class="col col-md-4">
												<div class="component-textblock">
													<div class="component-right" style="padding-left:40px;">
														<h4 class="heading"><i class="fa fa-thumbs-up" aria-hidden="true"></i>&nbsp;Popular blogs</h4> 
														<?php $count=1; ?>
														@foreach($blogs as $blog)
														<p><li><a href="/blog-detail/{{$blog->id}}">{{$blog->title}}</a></li></p>
														<?php $count ++; 
														if($count > 6)
															break;
														?>
														@endforeach
													</div>
												</div>
											</div> 
											<div class="col col-md-4"> 
												<div class="component-textblock"> 
													<div class="component-right" style="padding-left:40px;"> <h4 class="heading"><i class="fa fa-bullhorn"></i>&nbsp;In the news</h4>
														<p><li><a href="#">Demo announces a new partnership with Slack</a></li></p>
														<p><li><a href="#">Demo packages tools to help enterprises accelerate devOps adoption</a></li></p> 
														<p><li><a href="#">Business Software Maker Demo Says It’s Going All-In With Amazon Cloud</a></li></p> 
													</div>
												</div> 
											</div>
										</div>
									</div> -->


									<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
									<script>
										baguetteBox.run('.tz-gallery');
									</script>
									<style>
										.pb-video-container {
											padding-top: 20px;
											background: #bdc3c7;
											font-family: Lato;
										}

										.pb-video {
											border: 1px solid #e6e6e6;
											padding: 5px;
										}

										.pb-video:hover {
											background: #0f0d35;
										}

										.pb-video-frame {
											transition: width 2s, height 2s;
										}


										.pb-row {
											margin-bottom: 10px;
										}
									</style>

									@endsection