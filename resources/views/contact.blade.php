@extends('layouts.main.main')

@section('content')

<!-- Home -->

	<div class="home home_blog_detail d-flex flex-column align-items-start justify-content-end">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="" data-speed="0.8"></div>
		<div class="home_overlay"><img src="images/home_overlay.png" alt=""></div>

	</div>

	<!-- Contact -->

	<div class="contact">
		<div class="container">
			<div class="row">

				<!-- Contact Form -->
				<div class="col-lg-6">
					<div class="contact_form_container">
						<div class="contact_form_title">Make an Appointment</div>
						@if (count($errors) > 0)
    <div class="alert alert-danger">
     <button type="button" class="close" data-dismiss="alert">×</button>
     <ul>
      @foreach ($errors->all() as $error)
       <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif
   @if ($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
						<form action="{{url('sendemail/send')}}" method="post" class="contact_form" id="contact_form">
							@csrf()
							<div class="d-flex flex-row align-items-start justify-content-between flex-wrap">
								<input type="text" name="name" class="contact_input" placeholder="Your Name" required="required">
								<input type="email" name="email" class="contact_input" placeholder="Your E-mail" required="required">
								<input type="tel" name="phone" class="contact_input" placeholder="Your Phone" required="required">
								<input type="tel" name="subject" class="contact_input" placeholder="Subject" required="required">
								<select name="service" class="contact_input" required>
									<option disabled="" selected="" value="">Select Service</option>
									<option>Jira Software</option>
									<option>Bitbucket</option>
									<option>Confluence</option>
									<option>Jira Align</option>
									<option>SourceTree</option>
									<option>Trello</option>
								</select>
						<textarea name="messages" id="messages" class="contact_input" required="" placeholder="Enter Yore Message here"></textarea>
							</div>
							<button type="submit" class="button button_1 contact_button trans_200">make an appointment</button>
						</form>
					</div>
				</div>

				<!-- Contact Content -->
				<div class="col-lg-5 offset-lg-1 contact_col">
					<div class="contact_content">
						<div class="contact_content_title">Get in touch with us</div>
						<div class="contact_content_text">
							<p>GOT SOMETHING FOR US? TELL US MORE.<br> Let's take a step ahead and say Hi to each other. We are always here for Your Message, Love, Complains and Feedbacks</p>
						</div>
						<div class="direct_line d-flex flex-row align-items-center justify-content-start">
							<div class="direct_line_title text-center">Direct Line</div>
							<div class="direct_line_num text-center">+123-1234567890</div>
						</div>
						<div class="contact_info">
							<ul>
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>Address</div>
									<div>Shop No. 201, 2nd Floor<br>Labim Mall, Lalitpur</div>
								</li>
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>Phone</div>
									<div><a href="tel:+1234567890">+123-1234567890</a> | <a href="tel:+1234567890">+123-1234567890</a></div>
								</li>
								<li class="d-flex flex-row align-items-start justify-content-start">
									<div>E-mail</div>
									<div><a href="mailto:info@kxtechnologies.com" class="__cf_email__" data-cfemail="255c4a505748444c49654248444c490b464a48">info@kxtechnologies.com</a></div>
								</li>
							</ul>
						</div>
						<div class="contact_social">
							<ul class="d-flex flex-row align-items-center justify-content-start">
								<li><a href="https://www.facebook.com/kxtechnologies/"><i class="fa fa-facebook" target="_blank" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="https://www.instagram.com/kxtechnologies/"><i class="fa fa-instagram" target="_blank" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
	</div>
	</div>

	<!-- Newsletter -->

	<div class="newsletter">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/worklife-image.png" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="newsletter_title">Subscribe to our newsletter</div>
				</div>
			</div>
			<div class="row newsletter_row">
				<div class="col-lg-8 offset-lg-2">
					<div class="newsletter_form_container">
						<form action="#" id="newsleter_form" class="newsletter_form">
							<input type="email" class="newsletter_input" placeholder="Your E-mail" required="required">
							<button class="newsletter_button">subscribe</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	@endsection