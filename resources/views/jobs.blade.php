@extends('layouts.main.main')
<link rel="stylesheet" type="text/css" href="{{asset('styles/services.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('styles/services_responsive.css')}}">
@section('content')

<!-- Home -->

	<div class="home home_blog_detail d-flex flex-column align-items-start justify-content-end">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/services.jpg" data-speed="0.8"></div>
		<div class="home_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div style="font-size: 59px;" class="home_title banner">All Jobs</div>
							<div class="home_text">Our aim Is to give you the Jobs that you desire.</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Services -->

	<div class="services">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						@if ($message = Session::get('success'))
						<div class="alert alert-success alert-block">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ $message }}</strong>
						</div>
						@endif
					<div class="section_title_container">
						<div class="section_title"><h2>Our Jobs</h2></div>
						<div class="section_subtitle">This is kX Technologies</div>
					</div>
				</div>
			</div>
			<div class="row services_row">
				
				<!-- Service -->
				@if(count($jobs))
				@foreach($jobs as $job)
				<div class="col-xl-4 col-md-6 service_col">
					<div class="service text-center my-style">
						<div class="service">
							<a href="/jobs-detail/{{$job->id}}">
							<div class="icon_container1 d-flex flex-column align-items-center justify-content-center ml-auto mr-auto">
								<img style="max-width: 100%;" src="/uploads/{{$job->image}}">
							</div>
							<div style="text-transform: uppercase;" class="service_title">{{$job->name}}</div>
							<div class="service_text my-overflow">
								<p><?php echo ($job->description)?></p>
							</div>
					</div>
					</a>
					</div>
				</div>
				@endforeach
				@else
            <h5>There are no Jobs Added.</h5>
            @endif
			</div>
			<div  class="row">
			<div class="apple"> 
{{ $jobs->links() }}
</div>
</div>
		</div>
	</div>
	<!--Call to action -->

		<div class="cta-1 my-border">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="cta_section">
							<div class="cta_content">
								<div class="cta_title ">Get started with our Jobs, Apply Now</div>
								<div class="cta_text ">Contact us to get a job quotation and appointment for the job .</div>
							<div class="home_buttons d-flex flex-row align-items-center justify-content-start">
					<div style="margin: 0 auto;" data-toggle="modal" data-target="#myModal1" class="button button_1 trans_200"><a href="#">Apply Jobs Now</a></div>
				</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>



	<!-- Newsletter -->

	<div class="newsletter">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/newsletter.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="newsletter_title">Subscribe to our newsletter</div>
				</div>
			</div>
			<div class="row newsletter_row">
				<div class="col-lg-8 offset-lg-2">
					<div class="newsletter_form_container">
						<form action="/subscribe/send" method="post" id="newsleter_form" class="newsletter_form">
						@csrf()
						<input type="email" name="email" class="newsletter_input" placeholder="Your E-mail" required="required">
						<button class="newsletter_button">subscribe</button>
						<br><br>
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						@if ($message = Session::get('success'))
						<div class="alert alert-success alert-block">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ $message }}</strong>
						</div>
						@endif
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<!-- The Modal -->
<div class="modal fade" id="myModal1">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content make-other">

			<!-- Modal Header -->
			<div class="modal-header my-modal">
				<div class="intro_form_title">Apply For Jobs</div>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="col-lg-12 intro_col hov_form">
				@if (count($errors) > 0)
    <div class="alert alert-danger">
     <button type="button" class="close" data-dismiss="alert">×</button>
     <ul>
      @foreach ($errors->all() as $error)
       <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif
   @if ($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
				<form action="{{url('sendemail/send')}}" method="post" class="contact_form" id="contact_form">
							@csrf()
							<div class="d-flex flex-row align-items-start justify-content-between flex-wrap">
								<input type="text" name="name" class="contact_input" placeholder="Your Name" required="required">
								<input type="email" name="email" class="contact_input" placeholder="Your E-mail" required="required">
								<input type="tel" name="phone" class="contact_input" placeholder="Your Phone" required="required">
								<input type="tel" name="subject" class="contact_input" placeholder="Subject" required="required">
								<select name="service" class="contact_input" required>
									<option disabled="" selected="" value="">Select Job</option>
									@foreach($alljobs as $job)
									<option>{{$job->name}}</option>
									@endforeach
								</select>
						<textarea name="messages" id="messages" class="contact_input" required="" placeholder="Enter Yore Message here"></textarea>
							</div>
							<button type="submit" class="button button_1 contact_button trans_200">Apply Now</button>
						</form>
			</div>
			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>


@endsection