@extends('layouts.main.main')
<link rel="stylesheet" type="text/css" href="{{asset('styles/services.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('styles/services_responsive.css')}}">
@section('content')


<!-- Home -->

	<div class="home home_blog_detail d-flex flex-column align-items-start justify-content-end">
		<div style="background: url('/uploads/{{$services->image}}'); background-attachment: fixed; background-repeat: no-repeat; background-size: cover;" class="parallax_background parallax-window" data-parallax="scroll" data-image-src="/uploads/{{$services->image}}" data-speed="0.8"></div>
	</div>

	<!-- Services -->

	<div class="services">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title_container">
						<div style="text-transform: capitalize;" class="section_title"><h2>{{$services->name}}</h2></div>
						<div class="section_subtitle">This is kX Technologies</div>
					</div>
				</div>
			</div>
			<div class="detail-image services_row">
				
				<!-- Service -->
				<div class="service_col">
					<div class="service text-center">
						<div class="service">
							<div>
								<img style="max-width: 50%;" src="/uploads/{{$services->image}}">
							</div>
							<div style="text-align: justify;" class="service_text">
								<p><?php echo ($services->description)?></p>
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>
	</div>
			<div class="cta">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="cta_section">
							<div class="cta_content">
								<div class="cta_title">Get started with our products, for Quotation</div>
								<div class="cta_text">Contact us to get a free quotation and a Demo Of our product.</div>
							<div class="home_buttons d-flex flex-row align-items-center justify-content-start">
					<div style="margin: 0 auto;" data-toggle="modal" data-target="#myModal" class="button button_1 trans_200"><a href="#">Make an Appointment</a></div>
				</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- Newsletter -->

	<div class="newsletter">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/newsletter.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="newsletter_title">Subscribe to our newsletter</div>
				</div>
			</div>
			<div class="row newsletter_row">
				<div class="col-lg-8 offset-lg-2">
					<div class="newsletter_form_container">
						<form action="#" id="newsleter_form" class="newsletter_form">
							<input type="email" class="newsletter_input" placeholder="Your E-mail" required="required">
							<button class="newsletter_button">subscribe</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>


@endsection