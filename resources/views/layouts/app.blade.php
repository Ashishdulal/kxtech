<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Dashboard-kX Technologies</title>

    <!-- Fontfaces CSS-->
    <link href="{{asset('css/font-face.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/font-awesome-5/css/fontawesome-all.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{asset('vendor/bootstrap-4.1/bootstrap.min.css')}}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{asset('vendor/animsition/animsition.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/wow/animate.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/css-hamburgers/hamburgers.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/slick/slick.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/vector-map/jqvmap.min.css')}}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{asset('css/theme.css')}}" rel="stylesheet" media="all">
    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar2 js-right-sidebar d-block ">
            <div class="logo">
                <a href="/">
                   <img style="max-width: 50%;" src="{{asset('images/kx-logo.png')}}" alt="logo">Technologies
                </a>
            </div>
            <div class="menu-sidebar2__content js-scrollbar1">
                <div class="account2">
                    <div class="image img-cir img-120">
                        <img src="{{asset('images/administrator.png')}}" alt="Administrator" />
                    </div>
                    <h4 class="name">Administrator</h4>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                                {{ __('Sign out') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                    </form>
                </div>
                <nav class="navbar-sidebar2">
                    <ul class="list-unstyled navbar__list">
                        <li>
                            <a href="/home">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            </li>
                            <li>
                            <a href="/home/our-focus">
                                <i class="fa fa-user-md"></i>Our Focus</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fa fa-cogs" aria-hidden="true"></i>Services
                                <span class="arrow">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="/home/services">
                                        <i class="fa fa-cog" aria-hidden="true"></i>Index</a>
                                </li>
                                <li>
                                    <a href="/home/services/create">
                                        <i class="fa fa-cog" aria-hidden="true"></i>Create</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-image"></i>Gallery
                                <span class="arrow">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="/home/gallery">
                                       <i class="fas fa-images"></i>Index</a>
                                </li>
                                <li>
                                    <a href="/home/gallery/create">
                                        <i class="fa fa-file-image-o" aria-hidden="true"></i>Create</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Portfolio
                                <span class="arrow">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="/home/portfolio-category">
                                       <i class="fas fa-plus-square"></i>Portfolio Category</a>
                                </li>
                                <li>
                                    <a href="/home/portfolio">
                                        <i class="fa fa-newspaper-o" aria-hidden="true"></i>Portfolio Posts</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Blogs
                                <span class="arrow">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="/home/blog-category">
                                       <i class="fas fa-plus-square"></i>Blog Category</a>
                                </li>
                                <li>
                                    <a href="/home/blogs">
                                        <i class="fa fa-newspaper-o" aria-hidden="true"></i>Blog Posts</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-image"></i>Slider
                                <span class="arrow">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="/home/sliders">
                                       <i class="fas fa-images"></i>Index</a>
                                </li>
                                <li>
                                    <a href="/home/slider/create">
                                        <i class="fa fa-file-image-o" aria-hidden="true"></i>Create</a>
                                </li>
                            </ul>
                        </li>

                        <!-- <li>
                            <a href="/home/team-members">
                                <i class="fa fa-user-md"></i>Team Members</a>
                        </li>
 -->                        <li>
                            <a href="/home/testimonials">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>Testimonial</a>
                        </li>
                        <li>
                            <a href="/home/jobs">
                                <i class="fa fa-tasks" aria-hidden="true"></i>Jobs</a>
                        </li>
                        <li><a class="js-arrow open" href="#"></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->
        <div class="page-container2">
    <!-- HEADER DESKTOP-->
    <header class="header-desktop2">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="header-wrap2 my-header-wrap">
                    <div class="logo d-block d-lg-none">
                        <a href="/">
                            <img src="{{asset('images/absolute-aesthetics.jpg')}}" alt="AbsoluteAsthetics" />
                        </a>
                    </div>
                    <div class="header-button-item  js-sidebar-btn responsive-menu-bar">
                                    <i class="zmdi zmdi-menu"></i>
                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </header>
                                <section class="au-breadcrumb m-t-75 my-breadcrum">
                                </section>
                                
        
@yield('content')


    </div>

    <!-- Jquery JS-->
    <script src="{{asset('vendor/jquery-3.2.1.min.js')}}"></script>
    <!-- Bootstrap JS-->
    <script src="{{asset('vendor/bootstrap-4.1/popper.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap-4.1/bootstrap.min.js')}}"></script>
    <!-- Vendor JS       -->
    <script src="{{asset('vendor/slick/slick.min.js')}}">
    </script>
    <script src="{{asset('vendor/wow/wow.min.js')}}"></script>
    <script src="{{asset('vendor/animsition/animsition.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}">
    </script>
    <script src="{{asset('vendor/counter-up/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('vendor/counter-up/jquery.counterup.min.js')}}">
    </script>
    <script src="{{asset('vendor/circle-progress/circle-progress.min.js')}}"></script>
    <script src="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('vendor/chartjs/Chart.bundle.min.js')}}"></script>
    <script src="{{asset('vendor/select2/select2.min.js')}}">
    </script>
    <script src="{{asset('vendor/vector-map/jquery.vmap.js')}}"></script>
    <script src="{{asset('vendor/vector-map/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('vendor/vector-map/jquery.vmap.sampledata.js')}}"></script>
    <script src="{{asset('vendor/vector-map/jquery.vmap.world.js')}}"></script>

    <!-- Main JS-->
    <script src="{{asset('js/main.js')}}"></script>


    
    <!-- <script src="{{asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script> -->
    <script>
        CKEDITOR.replace('description');
        // $('textarea').ckeditor();
        // $.noConflict();
        // jQuery( document ).ready(function( $ ) {
        //   $('#description').ckeditor(); // Code that uses jQuery's $ can follow here.
        // });
        // if class is prefered.
    </script>

</body>

</html>
<!-- end document-->