<!-- Header -->

<header class="header trans_400">
	<div class="header_content d-flex flex-row align-items-center jusity-content-start trans_400">
		<!-- Logo -->
		<div class="logo">
			<a href="/">
				<img src="{{asset('images/kx-logo.png')}}" alt="logo">
			</a>
		</div>

		<!-- Main Navigation -->
		<nav class="main_nav">
			<ul class="d-flex flex-row align-items-center justify-content-start">
				<li class="nav-item {{ Request::is('/') ? 'active' : '' }}"><a href="{{ url('/' )}}">Home</a></li>
				<!-- <li class="nav-item {{ Request::segment(1)=='expertises' ? 'active' : '' }}"><a href="/expertises">Expertise</a></li> -->
				<li class="nav-item {{ Request::segment(1)=='services' ? 'active' : '' }}"><a href="/services">Services</a></li>
				<!-- <li class="nav-item {{ Request::segment(1)=='show-gallery' ? 'active' : '' }}"><a href="/show-gallery">Gallery</a></li> -->
				<li class="nav-item {{ Request::segment(1)=='portfolio' ? 'active' : '' }}"><a href="/portfolio">Portfolio</a></li>
				<li class="nav-item {{ Request::segment(1)=='blog' ? 'active' : '' }}"><a href="/blog">Blog</a></li>
				<li class="nav-item {{ Request::segment(1)=='career' ? 'active' : '' }}"><a href="/career">Career</a></li>
				<li class="nav-item {{ Request::segment(1)=='contact' ? 'active' : '' }}"><a href="/contact">Contact</a></li>
				<!-- <li class="nav-item dropdown">
					<a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Services
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						@foreach($service_header as $service)
						<a class="dropdown-item" href="/services-detail/{{ $service->id }}">{{ $service->name }}</a>
						@endforeach
					</div>
				</li> -->

			</ul>
		</nav>
		<div class="header_extra d-flex flex-row align-items-center justify-content-end ml-auto">

			<!-- Header Social -->
<!-- 			<div class="social header_social">
				<ul class="d-flex flex-row align-items-center justify-content-start">
					<li><a href="tel:+977-9840000121"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
					<li><a target="_blank" href="https://www.instagram.com/absoluteaestheticsnepal/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					<li><a target="_blank" href="https://www.facebook.com/absoluteaestheticsnepal/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				</ul>
			</div> -->
			<div class="hamburger" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars" aria-hidden="true"></i></div>
		</div>

	</div>
	<!-- Hamburger -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light1 make-menu">

		<div class="collapse navbar-expand-lg" id="navbarNavAltMarkup">
			<div class="navbar-nav ">
				<a class="{{ Request::is('/') ? 'active' : '' }}" href="{{ url('/' )}}">Home</a>
				<!-- <a href="/expertises">Expertise</a> -->
				<a href="/services">Services</a>
				<!-- <a href="/show-gallery">Gallery</a> -->
				<a href="/portfolio">Portfolio</a>
				<a href="/blog">Blog</a>
				<a href="/career">Career</a>
				<a href="/contact">Contact</a>
			</div>
		</div>
	</nav>
</header>
<!-- The Modal -->
<div class="modal fade" id="myModal">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content make-other">

			<!-- Modal Header -->
			<div class="modal-header my-modal">
				<div class="intro_form_title">Make an Appointment</div>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="col-lg-12 intro_col hov_form">
				@if (count($errors) > 0)
    <div class="alert alert-danger">
     <button type="button" class="close" data-dismiss="alert">×</button>
     <ul>
      @foreach ($errors->all() as $error)
       <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif
   @if ($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
				<form action="{{url('sendemail/send')}}" method="post" class="contact_form" id="contact_form">
							@csrf()
							<div class="d-flex flex-row align-items-start justify-content-between flex-wrap">
								<input type="text" name="name" class="contact_input" placeholder="Your Name" required="required">
								<input type="email" name="email" class="contact_input" placeholder="Your E-mail" required="required">
								<input type="tel" name="phone" class="contact_input" placeholder="Your Phone" required="required">
								<input type="tel" name="subject" class="contact_input" placeholder="Subject" required="required">
								<select name="service" class="contact_input" required>
									<option disabled="" selected="" value="">Select Service</option>
									<option>Jira Software</option>
									<option>Bitbucket</option>
									<option>Confluence</option>
									<option>Jira Align</option>
									<option>SourceTree</option>
									<option>Trello</option>
								</select>
						<textarea name="messages" id="messages" class="contact_input" required="" placeholder="Enter Yore Message here"></textarea>
							</div>
							<button type="submit" class="button button_1 contact_button trans_200">make an appointment</button>
						</form>
			</div>
			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
