<!DOCTYPE html>
<html lang="en">
<head>
<title>kX Technologies</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="kX Technologies template project">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('plugins/OwlCarousel2-2.2.1/animate.css')}}">
<link href="{{asset('plugins/jquery-datepicker/jquery-ui.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{asset('styles/contact.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('styles/main_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('styles/responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('styles/about.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('styles/contact_responsive.css')}}">

<link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
<link rel="stylesheet" href="{{asset('css/gallery-grid.css')}}">

</head>
<body>



<div class="super_container">

<?php use App\Service;
use App\Blog;
use App\job;
use App\portfolio;

	$jobs = job::latest()->get();
	$service_header  = Service::latest()->get();
    $portfolios = portfolio::latest()->paginate();
	$blog_header = Blog::latest()->get();
?>


@include('layouts.main.header')

@yield('content')

@include('layouts.main.footer')


</div>
	<script>
		var outer = $('.sub_category');

		$('#right').click(function() {
		   outer.animate({scrollLeft: '+=300px'}, 1140);
		});

		$('#left').click(function() {
		   outer.animate({scrollLeft: '-=300px'}, 1140);
		});
	</script>
<script src="js/blog.js" type="5c455a00546d4b6deab5b493-text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script><script src="plugins/greensock/TweenMax.min.js" type="772b37959b40d44eaa8e7c82-text/javascript"></script>
<script src="{{asset('plugins/greensock/TimelineMax.min.js')}}" type="772b37959b40d44eaa8e7c82-text/javascript"></script>
<script src="{{asset('plugins/scrollmagic/ScrollMagic.min.js')}}" type="772b37959b40d44eaa8e7c82-text/javascript"></script>
<script src="{{asset('plugins/greensock/animation.gsap.min.js')}}" type="772b37959b40d44eaa8e7c82-text/javascript"></script>
<script src="{{asset('plugins/greensock/ScrollToPlugin.min.js')}}" type="772b37959b40d44eaa8e7c82-text/javascript"></script>
<script src="{{asset('plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}" type="772b37959b40d44eaa8e7c82-text/javascript"></script>
<script src="{{asset('plugins/easing/easing.js')}}" type="772b37959b40d44eaa8e7c82-text/javascript"></script>
<script src="{{asset('plugins/parallax-js-master/parallax.min.js')}}" type="772b37959b40d44eaa8e7c82-text/javascript"></script>
<script src="{{asset('plugins/jquery-datepicker/jquery-ui.js')}}" type="772b37959b40d44eaa8e7c82-text/javascript"></script>
<script src="{{asset('js/custom.js')}}" type="772b37959b40d44eaa8e7c82-text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA" type="c465a745e6492860a4416b13-text/javascript"></script>
<script src="{{asset('js/contact.js')}}" type="c465a745e6492860a4416b13-text/javascript"></script>
<script src="{{asset('js/services.js')}}" type="c465a745e6492860a4416b13-text/javascript"></script>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13" type="772b37959b40d44eaa8e7c82-text/javascript"></script>
<script type="772b37959b40d44eaa8e7c82-text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/a2bd7673/cloudflare-static/rocket-loader.min.js" data-cf-settings="772b37959b40d44eaa8e7c82-|49" defer=""></script>
</body>
</html>