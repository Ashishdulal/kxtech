	<!-- Footer -->

	<footer class="footer">
		<div class="footer_content">
			<div class="container">
				<div class="row">

					<!-- Footer About -->
					<div class="col-lg-3 footer_col">
						<div class="footer_about">
							<div class="footer_logo">
								<a href="/">
									<div><span>KX TECHNOLOGIES</span></div>
									<div><p class="footer_text_desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
									tempor incididunt ut labore et dolore magna aliqua.</p></div>
								</a>
							</div>
							<div class="footer_about_text">
								
							</div>
						</div>
					</div>

<div class="col-sm-9">
	<div class="row">
					<div class="col-lg-4 footer_col">
						<div class="footer_location">
							<div class="footer_title">SERVICES</div>
							<ul class="contact_list">
														<?php $count=1; ?>
								@foreach($service_header as $service)
								<li><a href="/services-detail/{{$service->id}}">{{$service->name}}</a></li>
								<?php $count ++; 
											           if($count > 5)
											              break;
											            ?>
								@endforeach
							</ul><br>
						</div>
					</div>

					<div class="col-lg-4 footer_col">
						<div class="footer_location">
							<div class="footer_title">PORTFOLIO</div>
							<ul class="contact_list">
								<?php $count=1; ?>
								@foreach($portfolios as $portfolio)
								<li><a href="/portfolio-detail/{{$portfolio->id}}">{{$portfolio->title}}</a></li>
								<?php $count ++; 
								if($count > 5)
								break;
								?>
								@endforeach
							</ul>
						</div>
					</div>
					<div class="col-lg-4 footer_col">
						<div class="footer_location">
							<div class="footer_title">CAREER</div>
							<ul class="contact_list">
								<?php $count=1; ?>
								@foreach($jobs as $job)
								<li><a href="/jobs-detail/{{$job->id}}">{{$job->name}}</a></li>
								<?php $count ++; 
								if($count > 5)
								break;
								?>
								@endforeach
							</ul>
						</div>
					</div>
					<!-- <div class="col-lg-3 footer_col">
						<div class="footer_location">
							<div class="footer_title">ABOUT kX Technologies</div>
							<ul class="contact_list">
								<li><a href="/expertises">Expertise</a></li>
								<li><a href="/career">Carrier</a></li>
								<li><a href="/blog">Blogs</a></li>
								<li><a href="#">Investor Relations</a></li>
								<li><a href="#">Trust & Security</a></li>
							</ul>
						</div>
					</div> -->
				</div>

</div>
</div>
			</div>
		</div>
		<div class="footer_bar">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="footer_bar_content  d-flex flex-md-row flex-column align-items-md-center justify-content-start">
							<div class="copyright"><!-- Link back to nepgeeks can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script type="772b37959b40d44eaa8e7c82-text/javascript">document.write(new Date().getFullYear());</script> All rights reserved
</div>
							<nav class="footer_nav ml-md-auto">
								<div class="social ">
					<ul class="d-flex flex-row align-items-center justify-content-start">
						<li><a target="_blank" href="https://www.facebook.com/kxtechnologies/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="https://www.twitter.com/kxtechnologies/"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a target="_blank" href="https://www.instagram.com/kxtechnologies/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						<li><a target="_blank" href="https://www.linkedin.com/kxtechnologies/"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
					</ul>
				</div>
							</nav>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</footer>
