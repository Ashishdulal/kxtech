@extends('layouts.main.main')
<link rel="stylesheet" type="text/css" href="{{asset('styles/blog.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('styles/blog_responsive.css')}}">

@section('content')

<!-- Home -->

<div class="home home_blog_detail d-flex flex-column align-items-start justify-content-end">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-speed="0.8"></div>
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title banner">Portfolio</div>
						<div class="home_text">Welcome to our company.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 			<div class="blog_home">
			<div class="site__desktop-navigation">
				<nav>
				<div class="blog-header-blog">
					<ul class="primary-blog">
						@foreach($portfolioCategories as $blogcategory)
						<li class="blog-item"><a href="/blog/blog-category-detail/{{$blogcategory->id}}">{{$blogcategory->title}}</a></li>
						@endforeach
						<li class="blog-item subscribe"><a href="">Subscribe</a></li>
						<a href=""><li class="blog-item fa fa-search" aria-hidden="true"></li></a>
					</ul>
				</div>
				</nav>
			</div>
		</div> -->

</div>

	<!-- category -->
<!-- 	<section class="category_color">	
		<div class="row">
						<div class="sub_category">
							<div class="inner">
								<ul class="primary-blog">
									@foreach($portfolioCategories as $portfolioCategory)
						<li class="blog-item"><a href="/portfolio/portfolio-category-detail/{{$portfolioCategory->id}}">{{$portfolioCategory->title}}</a></li>
						@endforeach
								</ul>
							</div>
						</div>
						<div class="cat_button" style="position: absolute;">
<button class="previous round" id="left">&#8249;</button>
<button class="next round" id="right">&#8250;</button>
</div>
					</div>

	</section> -->

<!-- Home -->
			<div style="background: #fff; margin-top: 0;padding: 50px 0 0; " class="blog_home">
			<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<span class="term-header">PORTFOLIO ABOUT</span>
					<h3 style="text-transform: capitalize;" class="term-header2">{{ $portfolioCatName }}</h3>
				</div>
				<div class="col-sm-8">
					<p class="term-description">
					Need more hours in a day? Work smarter, not harder with tips to help you check things off your list, handy hacks to prevent procrastination, and brain breaks to bust boredom.</p>
				</div>
			</div>
		</div>
		</div>


	<!-- Blog -->

	<div class="blog">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 sub_cat_menus">
              <div class="isotope-filters isotope-filters-horizontal">
                <button class="isotope-filters-toggle button button-sm button-primary" data-custom-toggle="#isotope-filters" data-custom-toggle-disable-on-blur="true">Filter<span class="caret"></span></button>
                <ul class="isotope-filters-list" id="isotope-filters">
                  <li><a class="" data-isotope-filter="*" data-isotope-group="gallery" href="/portfolio">All</a></li>
                  @foreach($portfolioCategories as $gallerycat)
                  <li><a data-isotope-filter="{{$gallerycat->id}}" data-isotope-group="gallery" href="/portfolio/portfolio-category-detail/{{$gallerycat->id}}">{{$gallerycat->title}} </a></li>
                  @endforeach
                </ul>
              </div>
            </div>
					
					@foreach($portfolios as $posts)
					@if($posts->cat_id == $id)
				<div class="col-sm-4 homepage_blog">
						<div class="homepage_border">
							<!-- Blog Post -->
							<div class="portfolio_image"><img style="width:100%;height:230px;" src="/uploads/{{$posts-> f_image}}" alt=""></div>
							<div class="blog_post_homepage">
								<div class="portfolio_title"><h4>{{$posts->title}}</h4></div>
								<div class="blog_post_info">
									<ul class="d-flex flex-row">
										<li>In : <a >
											{{ $portfolioCatName }}
										</a></li>
									</ul>
								</div>
								<div class="blog_post_text">
									<p><?php echo ($posts -> description)?></p>
								</div>
								<div class="component component--link-button">
									<a href="/portfolio-detail/{{$posts->id}}" class="component__link link-arrow"> Read their story &nbsp;<svg class="link-arrow-image" width="11px" height="8px" viewBox="0 0 11 8" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<g class="group-path" transform="translate(-138.000000, -586.000000)" fill="#0052CC">
												<path d="M145.2803,586.507862 L144.2193,587.568863 L145.9393,589.287862 L138.7503,589.287862 C138.3363,589.287862 138.0003,589.623862 138.0003,590.037862 C138.0003,590.451862 138.3363,590.787862 138.7503,590.787862 L145.9393,590.787862 L144.2193,592.507862 L145.2803,593.568863 L148.8103,590.037862 L145.2803,586.507862 Z" id="Fill-1"></path></g></g></svg> </a> </div>
											</div>
										</div>
									</div>
				@endif
				@endforeach
					
			</div>
			<div class="row page_nav_row">
				<div class="col">
					<div class="page_nav">
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Newsletter -->

	<div class="newsletter">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/makeup1.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="newsletter_title">Subscribe to our newsletter</div>
				</div>
			</div>
			<div class="row newsletter_row">
				<div class="col-lg-8 offset-lg-2">
					<div class="newsletter_form_container">
						<form action="#" id="newsleter_form" class="newsletter_form">
							<input type="email" class="newsletter_input" placeholder="Your E-mail" required="required">
							<button class="newsletter_button">subscribe</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>



@endsection