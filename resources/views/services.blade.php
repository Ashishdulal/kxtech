@extends('layouts.main.main')
<link rel="stylesheet" type="text/css" href="{{asset('styles/services.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('styles/services_responsive.css')}}">
@section('content')

<!-- Home -->

	<div class="home home_blog_detail d-flex flex-column align-items-start justify-content-end">
		<div class="parallax_background parallax-window"></div>
		<div class="home_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="home_content">
							<div class="home_title banner">Services</div>
							<div class="home_text">Provide services with technical innovation.</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Services -->

	<div class="services">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title_container">
						<div class="section_title"><h2>Our Services</h2></div>
						<div class="section_subtitle">This is kX Technologies</div>
					</div>
				</div>
			</div>
			<div class="row services_row">
				
				<!-- Service -->
				@if(count($services))
				@foreach($services as $service)
				<div class="col-xl-4 col-md-6 service_col">
					<div class="service text-center my-style">
						<div class="service">
							<a href="/services-detail/{{$service->id}}">
							<div class="icon_container1 d-flex flex-column align-items-center justify-content-center ml-auto mr-auto">
								<img style="max-width: 100%;" src="/uploads/{{$service->image}}">
							</div>
							<div style="text-transform: uppercase;" class="service_title">{{$service->name}}</div>
							<div class="service_text my-overflow">
								<p><?php echo ($service->description)?></p>
							</div>
					</a>
						</div>
					</div>
				</div>
				@endforeach
				@else
				<h5>There are no posts in Services.</h5>
				@endif
			</div>
			<div  class="row">
			<div class="apple"> 
{{ $services->links() }}
</div>
</div>
		</div>
	</div>
	<!--Call to action -->

		<div class="cta-1">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="cta_section">
							<div class="cta_content">
								<div class="cta_title">Get started with our products, for Quotation</div>
								<div class="cta_text">Contact us to get a free quotation and a Demo Of our product.</div>
							<div class="home_buttons d-flex flex-row align-items-center justify-content-start">
					<div style="margin: 0 auto;" data-toggle="modal" data-target="#myModal" class="button button_1 trans_200"><a href="#">Make an Appointment</a></div>
				</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- Before and After -->
<!-- 
	<div class="before_and_after">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<div class="section_subtitle">This is kX Technologies</div>
						<div class="section_title"><h2>Our Gallery</h2></div>
					</div>
				</div>
			</div>
			<div class="row before_and_after_row">
				<div class="col">
					<div class="ba_container">
						<figure class="cd-image-container is-visible">
							<div class="row">
								<div class="col-sm-6">
								<img style="max-width: 100%;" src="{{asset('/images/about-type.png')}}" alt="Modified Image">
						</div>
						<div class="col-sm-6">
							<img style="max-width: 100%;" src="{{asset('/images/park.jpg')}}" alt="Original Image">
							</div>
						</div>
						</figure>
					</div>
					<div class="ba_text text-center"><p>Here we help you fulfill your desired IT solution with the help of trained and experienced developers and designers.</p></div>
					<div class="ba_button_container text-center">
						<div class="button button_1 ba_button"><a href="/show-gallery">see gallery</a></div>
					</div>
				</div>
			</div>
		</div>
	</div> -->

	<!-- Prices -->

	<!-- <div class="prices">
		<div class="container">
			<div class="row">
				
				
				<div class="col-lg-6 price_col">
					<div class="price">
						<div class="price_title">Breast Augmentation</div>
						<div class="price_text">
							<p>Integer aliquet congue libero, eu gravida odio ultrces ut. Etiam ac erat ut enim maximus accumsan vel ac nisl.</p>
						</div>
						<div class="price_panel"><a href="/contact">Get Inquiry</a></div>
					</div>
				</div>

			
				<div class="col-lg-6 price_col">
					<div class="price">
						<div class="price_title">Otoplasty</div>
						<div class="price_text">
							<p>Integer aliquet congue libero, eu gravida odio ultrces ut. Etiam ac erat ut enim maximus accumsan vel ac nisl.</p>
						</div>
						<div class="price_panel"><a href="/contact">Get Inquiry</a></div>
					</div>
				</div>

				<div class="col-lg-6 price_col">
					<div class="price">
						<div class="price_title">Eyelid Surgery</div>
						<div class="price_text">
							<p>Integer aliquet congue libero, eu gravida odio ultrces ut. Etiam ac erat ut enim maximus accumsan vel ac nisl.</p>
						</div>
						<div class="price_panel"><a href="/contact">Get Inquiry</a></div>
					</div>
				</div>

				
				<div class="col-lg-6 price_col">
					<div class="price">
						<div class="price_title">Botox</div>
						<div class="price_text">
							<p>Integer aliquet congue libero, eu gravida odio ultrces ut. Etiam ac erat ut enim maximus accumsan vel ac nisl.</p>
						</div>
						<div class="price_panel"><a href="/contact">Get Inquiry</a></div>
					</div>
				</div>

			
				<div class="col-lg-6 price_col">
					<div class="price">
						<div class="price_title">Liposuction</div>
						<div class="price_text">
							<p>Integer aliquet congue libero, eu gravida odio ultrces ut. Etiam ac erat ut enim maximus accumsan vel ac nisl.</p>
						</div>
						<div class="price_panel"><a href="/contact">Get Inquiry</a></div>
					</div>
				</div>

				<div class="col-lg-6 price_col">
					<div class="price">
						<div class="price_title">Hyaluronic Acid</div>
						<div class="price_text">
							<p>Integer aliquet congue libero, eu gravida odio ultrces ut. Etiam ac erat ut enim maximus accumsan vel ac nisl.</p>
						</div>
						<div class="price_panel"><a href="/contact">Get Inquiry</a></div>
					</div>
				</div>

			</div>
		</div>
	</div> -->

	<!-- Newsletter -->

	<div class="newsletter">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/newsletter.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="newsletter_title">Subscribe to our newsletter</div>
				</div>
			</div>
			<div class="row newsletter_row">
				<div class="col-lg-8 offset-lg-2">
					<div class="newsletter_form_container">
						<form action="/subscribe/send" method="post" id="newsleter_form" class="newsletter_form">
						@csrf()
						<input type="email" name="email" class="newsletter_input" placeholder="Your E-mail" required="required">
						<button class="newsletter_button">subscribe</button>
						<br><br>
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						@if ($message = Session::get('success'))
						<div class="alert alert-success alert-block">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ $message }}</strong>
						</div>
						@endif
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>


@endsection