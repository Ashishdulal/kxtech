@extends('layouts.main.main')
<link rel="stylesheet" type="text/css" href="{{asset('styles/blog.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('styles/blog_responsive.css')}}">S

@section('content')
<!-- Home -->

<div class="home home_blog_detail d-flex flex-column align-items-start justify-content-end">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-speed="0.8"></div>
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<!-- <div class="home_content">
						<div class="home_title">Blog Detail</div>
						<div class="home_text">Welcome to our company.</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>

</div>

<!-- category -->
<!-- <section class="category_color">	
	<div class="row">
		<div class="sub_category">
			<div class="inner">
				<ul class="primary-blog">
					@foreach($portfolioCategories as $portfolioCategory)
					<li class="blog-item"><a href="/portfolio/portfolio-category-detail/{{$portfolioCategory->id}}">{{$portfolioCategory->title}}</a></li>
					@endforeach
				</ul>
			</div>
		</div>
		<div class="cat_button" style="position: absolute;">
			<button class="previous round" id="left">&#8249;</button>
			<button class="next round" id="right">&#8250;</button>
		</div>
	</div>

</section> -->
<!-- Blog -->
<div style="background: #fff; margin-top: 0;padding: 50px 0 0; " class="blog_home">
			<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<span class="term-header">PORTFOLIO Title:</span>
					<a href="/portfolio">
					<h3 style="text-transform: capitalize;" class="term-header2">{{$portfolios->title}}</h3>
					</a>
				</div>
				<div class="col-sm-8">
					<p class="term-description">
					Need more hours in a day? Work smarter, not harder with tips to help you check things off your list, handy hacks to prevent procrastination, and brain breaks to bust boredom.</p>
				</div>
			</div>
		</div>
		</div>
<div class="blog">
	<div class="container">
		<div class="row">					
			<div class="col">

				<!-- Blog Post -->
				<div class="blog_post">
					<div class="row">
						<div class="col-sm-6 blog_post_image-my"><img src="/uploads/{{$portfolios-> f_image}}" alt="{{$portfolios->name}}"></div>
						<br>
						<div class="col-sm-6 blog_post_image-my"><img src="/uploads/{{$portfolios-> i_image}}" alt="{{$portfolios->name}}"></div>
					</div>
				</div>
				<div class="blog_post_info">
					<ul class="d-flex flex-row align-items-center justify-content-center">
						<li>By : <a >Admin</a></li>
						<li>In : <a >
							@foreach($portfolioCategories as $portfolioCategory)
							@if($portfolios->cat_id === $portfolioCategory->id)
							{{$portfolioCategory->title}}
							@endif
							@endforeach
						</a></li>
					</ul>
				</div>
				<div class="blog_post_title"><a style="color: #000000;" >{{$portfolios->title}}</a></div>	
				<div class=" text-center-des">
					<p><?php echo ($portfolios->description)?></p>
				</div>
																<div class="home_buttons d-flex flex-row align-items-center justify-content-start">
					<div style="margin: 0 auto;" data-toggle="modal" data-target="#myModalinq" class="button button_1 trans_200"><a href="#">Get the demo</a></div>
				</div>
		<div class="cta-1">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="cta_section">
							<div class="cta_content">
								<div class="cta_title">Get started with our products, for Inquiry</div>
								<div class="cta_text">Contact us to get a free quotation and a Demo Of our product.</div>
							<div class="home_buttons d-flex flex-row align-items-center justify-content-start">
					<div style="margin: 0 auto;" data-toggle="modal" data-target="#myModalinq" class="button button_1 trans_200"><a href="#">Make an Inquiry</a></div>
				</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- The Modal -->
<div class="modal fade" id="myModalinq">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content make-other">

			<!-- Modal Header -->
			<div class="modal-header my-modal">
				<div class="intro_form_title">Make an Inquiry</div>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="col-lg-12 intro_col hov_form">
				@if (count($errors) > 0)
    <div class="alert alert-danger">
     <button type="button" class="close" data-dismiss="alert">×</button>
     <ul>
      @foreach ($errors->all() as $error)
       <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif
   @if ($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
				<form action="{{url('sendemail/send')}}" method="post" class="contact_form" id="contact_form">
							@csrf()
							<div class="d-flex flex-row align-items-start justify-content-between flex-wrap">
								<input type="text" name="name" class="contact_input" placeholder="Your Name" required="required">
								<input type="email" name="email" class="contact_input" placeholder="Your E-mail" required="required">
								<input type="tel" name="phone" class="contact_input" placeholder="Your Phone" required="required">
								<input type="tel" name="subject" class="contact_input" placeholder="Subject" required="required">
								<input style="width: 100%;" type="text" name="service" class="contact_input" placeholder="Subject" required="required">
						<textarea name="messages" id="messages" class="contact_input" required="" placeholder="Enter Yore Message here"></textarea>
							</div>
							<button type="submit" class="button button_1 contact_button trans_200">make an Inquiry</button>
						</form>
			</div>
			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>
			</div>

		</div>
	</div>
</div>

</div>


@endsection