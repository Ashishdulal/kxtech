@extends('layouts.main.main')
<link rel="stylesheet" type="text/css" href="{{asset('styles/blog.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('styles/blog_responsive.css')}}">

@section('content')

<!-- Home -->

<div class="home home_blog_detail d-flex flex-column align-items-start justify-content-end">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-speed="0.8"></div>
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title banner">Blogs</div>
						<div class="home_text">Welcome to our company.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 			<div class="blog_home">
			<div class="site__desktop-navigation">
				<nav>
				<div class="blog-header-blog">
					<ul class="primary-blog">
						@foreach($blogcategories as $blogcategory)
						<li class="blog-item"><a href="/blog/blog-category-detail/{{$blogcategory->id}}">{{$blogcategory->title}}</a></li>
						@endforeach
						<li class="blog-item subscribe"><a href="">Subscribe</a></li>
						<a href=""><li class="blog-item fa fa-search" aria-hidden="true"></li></a>
					</ul>
				</div>
				</nav>
			</div>
		</div> -->

</div>
	<!-- category -->
<!-- 	<section class="category_color">	
		<div class="row">
						<div class="sub_category">
							<div class="inner">
								<ul class="primary-blog">
									@foreach($blogcategories as $blogcategory)
									<li class="blog-item"><a href="/blog/blog-category-detail/{{$blogcategory->id}}">{{$blogcategory->title}}</a></li>
									@endforeach
								</ul>
							</div>
						</div>
						<div class="cat_button" style="position: absolute;">
<button class="previous round" id="left">&#8249;</button>
<button class="next round" id="right">&#8250;</button>
</div>

					</div>

	</section> -->

<!-- Home -->
			<div style="background: #fff; margin-top: 0;padding: 50px 0 0; " class="blog_home">
			<div class="container">
			<div class="row sub-cat-head">
				<div class="col-sm-4">
					<span class="term-header">ARTICLES ABOUT</span>
					<h3 style="text-transform: capitalize;" class="term-header2">{{ $blogCatName }}</h3>
				</div>
				<div class="col-sm-8">
					<p class="term-description">
					Need more hours in a day? Work smarter, not harder with tips to help you check things off your list, handy hacks to prevent procrastination, and brain breaks to bust boredom.</p>
				</div>
			</div>
		</div>
		</div>


	<!-- Blog -->

	<div class="blog">
		<div class="container">
			<div class="row">
					
				<div class="col-sm-7">
					@foreach($blogs as $blog)
					@if($blog->cat_id == $id)
					
					<!-- Blog Post -->
					<div class="blog_post">
						<div class="blog_post_image"><a href="/blog-detail/{{$blog->id}}"><img src="/uploads/{{$blog-> f_image}}" alt=""></a></div>
						<div class="blog_post_date d-flex flex-column align-items-center justify-content-center">
							<div class="date_day">{{ $blog->created_at->format('d') }}</div>
							<div class="date_month">{{ $blog->created_at->format('M') }}</div>
							<div class="date_year">{{ $blog->created_at->format('Y') }}</div>
						</div>
						<div class="blog_post_title"><a href="/blog-detail/{{$blog->id}}">{{$blog->title}}</a></div>
						<div class="blog_post_info">
							<ul class="d-flex flex-row align-items-center justify-content-center">
								<li>By : <a >Admin</a></li>
								<li>In : <a >
								@foreach($blogcategories as $blogcategory)
								@if($blog->cat_id === $blogcategory->id)
								{{$blogcategory->title}}
								@endif
							@endforeach
							</a></li>
							</ul>
						</div>
						<div class="blog_post_text text-center">
							<p><?php echo ($blog -> description)?></p>
						</div>
						<div class="blog_post_button text-center"><div class="button button_1 ml-auto mr-auto"><a href="/blog-detail/{{$blog->id}}">read more</a></div></div>
					</div>

				@endif
				@endforeach
				</div>
				<div class="col-sm-1"></div>
				<div class="col-sm-4">
						<div class="sub_category_list">
							<h4>Categories</h4>
							<div class="cat_inner">
								<ul class="primary-blog-cat">
									@foreach($blogcategories as $blogcategory)
									<li class="blog-item-cat"><span class="fa fa-chevron-right"></span><a class="blog-category-text" href="/blog/blog-category-detail/{{$blogcategory->id}}">{{$blogcategory->title}}</a></li>
									@endforeach
								</ul>
							</div>
						</div>
						<div class="sub_category_list">
							<h4>Recent Blog Posts</h4>
							<div class="cat_inner">
									<?php $count=1; ?>
                  @if(count($blogs))
                  @foreach($blogs as $blog)
                  <div class="blog-aside-post">
                    <h5><a href="/blog-detail/{{$blog->id}}"> {{$blog->title}}</a></h5>
                    <p>{{ $blog->created_at->format('D M , Y | H:i') }}</p>
                  </div>
                  <?php $count ++; ?>
                  <?php if($count > 3)
                  break
                  ?>
                  @endforeach
                  @else
                  <h5>There are no posts in Blog.</h5>
                  @endif
							</div>
						</div>
			</div>
			</div>
			<div class="row page_nav_row">
				<div class="col">
					<div class="page_nav">
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Newsletter -->

	<div class="newsletter">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/makeup1.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="newsletter_title">Subscribe to our newsletter</div>
				</div>
			</div>
			<div class="row newsletter_row">
				<div class="col-lg-8 offset-lg-2">
					<div class="newsletter_form_container">
						<form action="#" id="newsleter_form" class="newsletter_form">
							<input type="email" class="newsletter_input" placeholder="Your E-mail" required="required">
							<button class="newsletter_button">subscribe</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>



@endsection