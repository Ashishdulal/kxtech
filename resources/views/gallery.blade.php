@extends('layouts.main.main')

@section('content')

<div class="container gallery-container">

    <h1>Gallery</h1>

    <p class="page-description text-center">All of our works</p>
    
    <div class="tz-gallery">

        <div class="row">
            @if(count($gallery))
            @foreach ($gallery as $galle)
            <div class="col-sm-6 col-md-3">
                <?php 
                    $string = $galle->image;
                    $string = substr(strrchr($string, '.'), 1);
                ?>
                @if($string == "jpg" || $string == "png" || $string == "jpeg")
                <a class="lightbox" href="../uploads/gallery/{{$galle->image}}">
                    <img src="../uploads/gallery/{{$galle->image}}" alt="{{$galle->name}}">
                </a>
                @else
                <video class="pb-video-frame my-lightbox" width="100%" height="230" src="../uploads/gallery/{{$galle->image}}" frameborder="0" allowfullscreen controls></video>
                @endif
            </div>
            @endforeach
            @else
            <h5>There are no Gallery items Added.</h5>
            @endif
     </div>
 </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>
<style>
    .pb-video-container {
        padding-top: 20px;
        background: #bdc3c7;
        font-family: Lato;
    }

    .pb-video {
        border: 1px solid #e6e6e6;
        padding: 5px;
    }

        .pb-video:hover {
            background: #0f0d35;
        }

    .pb-video-frame {
        transition: width 2s, height 2s;
    }


    .pb-row {
        margin-bottom: 10px;
    }
</style>
@endsection