@extends('layouts.main.main')

@section('content')

<!-- Home -->

<div class="home d-flex flex-column align-items-start justify-content-end">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-speed="0.8"></div>
	<div class="home_container">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="home_content">
						<div class="home_title banner">Expertise</div>
						<div class="home_text">Welcome to our company.</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Intro -->

<div class="intro border_bottom">
	<div class="container">
		<div class="row">

			<!-- Intro Content -->
			<div class="col-lg-8">
				<div class="intro_content">
					<div class="section_title_container">
						<div class="section_title"><h2>Welcome to our Company</h2></div>
						<div class="section_subtitle">This is kX Technologies</div>
					</div>
					<div class="intro_text">
						<p>Kx Technology is an IT company dealing with both national and International customers. Full of passionate and well trained group of professionals. Our group offers services in the field of IT, Sales & Marketing, Logistical Support, Graphic Design, Digital Marketing and many other services. Here we help you fulfill your desired IT solution with the help of trained and experienced developers and designers.</p>
					</div>

					<!-- Milestones -->
					<div class="milestones">
						<div class="row milestones_row">
							
							<!-- Milestone -->
							<div class="col-md-3 milestone_col">
								<div class="milestone">
									<div class="milestone_counter" data-end-value="5000" data-sign-before="+">0</div>
									<div class="milestone_text">Websites</div>
								</div>
							</div>

							<!-- Milestone -->
							<div class="col-md-3 milestone_col">
								<div class="milestone">
									<div class="milestone_counter" data-end-value="352">0</div>
									<div class="milestone_text">Software Designs</div>
								</div>
							</div>

							<!-- Milestone -->
							<div class="col-md-3 milestone_col">
								<div class="milestone">
									<div class="milestone_counter" data-end-value="718">0</div>
									<div class="milestone_text">Trusted Clients</div>
								</div>
							</div>

							<!-- Milestone -->
							<div class="col-md-3 milestone_col">
								<div class="milestone">
									<div class="milestone_counter" data-end-value="50">0</div>
									<div class="milestone_text">Developers</div>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>

			<!-- Intro Image -->
			<div class="col-lg-3 offset-lg-1">
				<div class="intro_image"><img src="{{asset('/images/about-type.png')}}" alt=""></div>
			</div>
		</div>
	</div>
	<h5></h5>
</div>

<!-- enterprise level-->

<div class="cta-1 border_bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 place_bottom">
						<div class="cta-image ml-lg-auto make-center bottom_image">
							<img src="{{asset('/images/img_sq_infographics.png')}}">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="service bottom_text">
							<div style="text-transform: uppercase; font-size: 30px; line-height: 1.33333333; margin-bottom: 16px;" class="service_title">enterprise level application</div>
							<div style="font-size: 0.9rem; line-height: 30px; margin-bottom: 20px;text-align: justify;" class="cta_text">
							Our business application that acts as a software solution that provides business logic and tools for organizations to model whole business processes to improve productivity and efficiency. A business application that can be used to run multiple departments and business processes throughout an organization. Enterprise applications help to deliver good customer relationships, enhanced organizational efficiency and data-driven insights. It is a large platform of software systems designed to operate in a business environment. The software improves the functionality of support at the business level. Our enterprise application services assist in optimizing data and workflow, managing risk and compliance, increasing operational efficiency and productivity, and achieving peak performance through a holistic business and technology approach. </div>
										</div>
									</div>
								</div>
							</div>
							<h5></h5>
						</div>

<!-- api integration-->

<div class="cta-1 border_bottom">
			<div class="container">
				<div class="row">
										<div class="col-sm-6">
						<div class="service">
							<div style="text-transform: uppercase; font-size: 30px; line-height: 1.33333333; margin-bottom: 16px;" class="service_title">API integration</div>
							<div style="font-size: 0.9rem; line-height: 30px; margin-bottom: 20px; text-align: justify;" class="cta_text">
							Innovative applications and devices apply in different ways. We at kxtechs provide software application building tools and protocols. An online programming interface that enables back-end communication. It builds your online presence on the channel to endorse your products and services. We simplify design, then manage data and communicate between devices and programs. This is our effective solution for application and automation that enables you to update workflows to be faster and more productive. Integrating channels of communication to allow faster business processes. </div>
										</div>
									</div>
					<div class="col-sm-6">
						<div class="cta-image ml-lg-auto make-center">
							<img src="{{asset('/images/api-integration.png')}}">
						</div>
					</div>
								</div>
							</div>
							<h5></h5>
						</div>

<!-- user experience-->

<div class="cta-1 border_bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 place_bottom">
						<div class="cta-image ml-lg-auto make-center bottom_image1">
							<img src="{{asset('/images/User-experience.png')}}">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="service bottom_text">
							<div style="text-transform: uppercase; font-size: 30px; line-height: 1.33333333; margin-bottom: 16px;" class="service_title">User experience</div>
							<div style="font-size: 0.9rem; line-height: 30px; margin-bottom: 20px; text-align: justify;" class="cta_text">
							Our solution identifies the interaction of the individual with your specific product, service or system. We study the attitude of your user and translate it into a business outlook. It includes designing the entire product acquisition and integration process including branding, design, usability and functional aspects. We enhance the quality of user perceptions of your products and services that align with the goals of the company. We offer user experience at kxtech that focuses on keeping user needs and wants in mind. </div>
										</div>
									</div>
								</div>
							</div>
							<h5></h5>
						</div>

<!-- 020-revolution-->

<div class="cta-1">
			<div class="container">
				<div class="row">
										<div class="col-sm-6">
						<div class="service">
							<div style="text-transform: uppercase; font-size: 30px; line-height: 1.33333333; margin-bottom: 16px;" class="service_title">020 revolution</div>
							<div style="font-size: 0.9rem; line-height: 30px; margin-bottom: 20px; text-align: justify;" class="cta_text">
							Offline to online revolution is our way of improving online store sales and business. Our method is to use internet advertising to create business opportunities – to gain brand presence in the acquiring industry. We become experts in what we do, especially in online community development. For online research and better shopping experience, we create seamless digital experience. Making us represent the marketing revolution's online and offline channels. We create opportunities to increase sales and improve your online presence in digital channels for your physical store locations.</div>
										</div>
									</div>
					<div class="col-sm-6">
						<div class="cta-image ml-lg-auto make-center">
							<img src="{{asset('/images/020-revolution.jpg')}}">
						</div>
					</div>
								</div>
							</div>
						</div>

<!-- Call to action -->

<div class="cta-1 appointment_color">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="cta_container d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
					<div class="cta_content">
						<div class="cta_title white_color">Make your appointment today!</div>
						<div class="cta_text white_color">Why wait? Let’s Connect and Start a Project Today</div>
					</div>
					<div class="cta_phone ml-lg-auto"><a style="color: #fff;" href="tel:+977-1234567890" >+123-1234567890</a></div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Team -->

<!-- <div class="team">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="section_title_container text-center">
					<div class="section_subtitle">This is kX Technologies</div>
					<div class="section_title"><h2>Meet Our Members</h2></div>
				</div>
			</div>
		</div>
		<div class="row team_row">

			@foreach($doctor as $doctor)

			<a class="team_row" data-toggle="modal" data-target="#team-membermodal{{$doctor->id}}" href="#">
				<div class="col-lg-4 team_col">
					<div class="team_item text-center d-flex flex-column aling-items-center justify-content-end">
						<div class="team_image"><img src="uploads/{{$doctor->image}}" alt="{{$doctor->name}}"></div>
						<div class="team_content text-center">
							<div class="team_name"><a data-toggle="modal" data-target="#team-membermodal{{$doctor->id}}" href="#">{{$doctor->name}}</a></div>
							<div class="team_title">{{$doctor->designation}}</div>
							<div class="team_text my-overflow">
								<p><?php echo ($doctor->description)?></p>
							</div>
						</div>
					</div>
				</div>
			</a>
			<div class="modal fade" id="team-membermodal{{$doctor->id}}" tabindex="-1" role="dialog" aria-labelledby="team-membermodal5Title" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLongTitle">kX Technologies</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="portfolio-single">
								<div class="row">
									<div class="col-sm-6">
										<div class="portfolio-head">
											<img src="uploads/{{$doctor->image}}" alt="{{$doctor->name}}">
										</div>
										<ul class="portfolio-social">
											<li><a href="{{$doctor->facebook}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
											<li><a href="{{$doctor->twitter}}" target="_blank"><i class="fa fa-twitter"></i></a></li>
											<li><a href="{{$doctor->instagram}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
											<li><a href="mailto:{{$doctor->mail}}" target="_blank"><i class="fa fa-envelope"></i></a></li>
										</ul>
									</div>
									<div class="col-sm-6">
										<div class="text">
											<h4>{{$doctor->name}}</h4>
											<h2>{{$doctor->designation}}</h2>
											<p align="justify"><?php echo ($doctor->description)?></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach

		</div>
	</div>
</div> -->

<!-- Testimonials -->

<div class="testimonials">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="section_title_container text-center">
					<div class="section_title"><h2>Clients Testimonials</h2></div>
					<div class="section_subtitle">This is kX Technologies</div>
				</div>
			</div>
		</div>
<div id="myCarousel" class="carousel slide" data-ride="carousel">

	<!-- Wrapper for carousel items -->
	<div class="carousel-inner">
		@foreach($testimonials as $testimonial)
		<div class="item carousel-item">
			<div class="img-box"><img src="/uploads/{{$testimonial->image}}" alt="hello"></div>
			<p class="testimonial"><?php echo ($testimonial->description ) ?></p>
			<p class="overview"><b>{{$testimonial->name}}</b>Patient</p>
		</div>
		@endforeach
<span id="contact"></span>
		
		<div class="item carousel-item active">
			<div class="img-box"><img src="{{asset('/images/administrator.png')}}" alt="hye"></div>
			<p class="testimonial">kindly requesting you to write your opinion about our center and services : @ info@kxTechnologies.com.</p>
			<p class="overview"><b>kX Technologies</b>Admin</p>
		</div>	
	</div>
	<!-- Carousel controls -->
	<a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev">
		<i class="fa fa-angle-left"></i>
	</a>
	<a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
		<i class="fa fa-angle-right"></i>
	</a>
</div>
	</div>
</div>

<!-- Newsletter -->

<div class="newsletter">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-speed="0.8"></div>
	<div class="container">
		<div class="row">
			<div class="col text-center">
				<div class="newsletter_title">Subscribe to our newsletter</div>
			</div>
		</div>
		<div class="row newsletter_row">
			<div class="col-lg-8 offset-lg-2">
				<div class="newsletter_form_container">
					<form action="/subscribe/send" method="post" id="newsleter_form" class="newsletter_form">
						@csrf()
						<input type="email" name="email" class="newsletter_input" placeholder="Your E-mail" required="required">
						<button class="newsletter_button">subscribe</button>
						<br><br>
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						@if ($message = Session::get('success'))
						<div class="alert alert-success alert-block">
							<button type="button" class="close" data-dismiss="alert">×</button>
							<strong>{{ $message }}</strong>
						</div>
						@endif
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection