@extends('layouts.main.main')

@section('content')
	<div class="home">

		<!-- Home Slider -->
		<div class="home_slider_container">
			<div class="owl-carousel owl-theme home_slider">
				
				<!-- Slide -->
				@foreach($sliders as $slider)
				<div class="owl-item">
					<div class="background_image" style="background-image:url(uploads/{{{$slider->image}}})"></div>
					<div class="home_container">
						<div class="container">
							<div class="row">
								<div class="col">
									<div class="home_content">
										<div class="home_subtitle">{{$slider->title}}</div>
										<div class="home_title">{{$slider->title_1}}!</div>
										<div class="home_text">
											<p><?php echo ($slider->description)?></p>
										</div>
										<div class="home_buttons d-flex flex-row align-items-center justify-content-start">
											<div class="button button_1 trans_200"><a href="/about">Read More</a></div>
											<div data-toggle="modal" data-target="#myModal" class="button button_2 trans_200"><a href="#">Make an Appointment</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach

			</div>

			<!-- Home Slider Dots -->

			<div class="home_slider_dots">
				<ul id="home_slider_custom_dots" class="home_slider_custom_dots d-flex flex-row align-items-center justify-content-start">
									@foreach($sliders as $slider)
					<li class="home_slider_custom_dot trans_200 active"></li>
					@endforeach
				</ul>
			</div>

		</div>
	</div>

	<!-- Intro -->

	<div class="intro">
		<div class="container">
			<div class="row">

				<!-- Intro Content -->
				<div class="col-lg-6 intro_col">
					<div class="intro_content">
						<div class="section_title_container">
							<div class="section_subtitle">This is Absolute Asthetics</div>
							<div class="section_title"><h2>Welcome to our Clinic</h2></div>
						</div>
						<div class="intro_text">
							<p>Absolute Aesthetics is a specialized center geared towards achieving excellence regarding skin, hair and dental aesthetics.Here we help you get your desired look with the help of trained and experienced dermatologists, dentists and plastic surgeons.   This Is a center providing skin related procedures to suit your needs according to your skin.  Our aim Is to give you the flawless skin that you desire. Along with skin procedure we also provide services regarding dental aesthetics. Flawless young and radiant skin along with a bright shining  smile Is a winning combination.</p>
						</div>
						<div class="milestones">
							<div class="row milestones_row">
							
								<!-- Milestone -->
								<div class="col-md-4 milestone_col">
									<div class="milestone">
										<div class="milestone_counter" data-end-value="500" data-sign-before="+">0</div>
										<div class="milestone_text">Satisfied Patients</div>
									</div>
								</div>

								<!-- Milestone -->
								<div class="col-md-4 milestone_col">
									<div class="milestone">
										<div class="milestone_counter" data-end-value="352">0</div>
										<div class="milestone_text">Face Liftings</div>
									</div>
								</div>

								<!-- Milestone -->
								<div class="col-md-4 milestone_col">
									<div class="milestone">
										<div class="milestone_counter" data-end-value="718">0</div>
										<div class="milestone_text">Injectibles</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>

				<!-- Intro Form -->
				<div class="col-lg-6 intro_col">
					<div class="intro_form_container">
						<div class="intro_form_title">Make an Appointment</div>
						@if (count($errors) > 0)
    <div class="alert alert-danger">
     <button type="button" class="close" data-dismiss="alert">×</button>
     <ul>
      @foreach ($errors->all() as $error)
       <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif
   @if ($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
						<form action="{{url('sendemail/send')}}" method="post" class="contact_form" id="contact_form">
							@csrf()
							<div class="d-flex flex-row align-items-start justify-content-between flex-wrap">
								<input type="text" name="name" class="intro_select intro_input" placeholder="Your Name" required="required">
								<input type="email" name="email" class="intro_select intro_input" placeholder="Your E-mail" required="required">
								<input type="tel" name="phone" class="intro_select intro_input" placeholder="Your Phone" required="required">
								<select name="service" class="intro_select intro_input" required>
									<option disabled="" selected="" value="">Select Service</option>
									<option>Dental</option>
									<option>Dormatology | Aesthetics</option>
								</select>
								<select name="doctor" class="intro_select intro_input" required="required">
							<option disabled="" selected="" value="">Select Doctor</option>
							<option>DR. LOKENDRA LIMBU</option>
							<option>DR. KHAGENDRA CHHETRI</option>
							<option>DR. SUNESHA PRADHAN</option>
							<option>DR. RAJEEV SINGH</option>
							<option>DR. RATNA BAHADUR GHARTI</option>
						</select>
						<input type="text" name="date" id="datepicker" class="intro_input datepicker hasDatepicker" placeholder="Date" required="required">
							</div>
							<button type="submit" class="button button_1 contact_button trans_200">make an appointment</button>
						</form>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Why Choose Us -->

	<div class="why">
		<div class="container">
			<div class="row row-eq-height">

				<!-- Why Choose Us Image -->
				<div class="col-lg-6 order-lg-1 order-2">
					<div class="why_image_container">
						<div class="why_image"><img src="images/machine-image.jpg" alt="machine"></div>
					</div>
				</div>

				<!-- Why Choose Us Content -->
				<div class="col-lg-6 order-lg-2 order-1">
					<div class="why_content">
						<div class="section_title_container">
							<div class="section_subtitle">This is Absolute Asthetics</div>
							<div class="section_title"><h2>Why choose us?</h2></div>
						</div>
						<div class="why_text">
							<p>We help you get your desired look with the help of trained and experienced dermatologists, dentists and plastic surgeons.   This Is a center providing skin related procedures to suit your needs according to your skin.  Our aim Is to give you the flawless skin that you desire. Along with skin procedure we also provide services regarding dental aesthetics. Flawless young and radiant skin along with a bright shining  smile Is a winning combination.</p>
						</div>
						<div class="why_list">
							<ul>

								<!-- Why List Item -->
								<li class="d-flex flex-row align-items-center justify-content-start">
									<div class="icon_container d-flex flex-column align-items-center justify-content-center">
										<i class="fa fa-check" aria-hidden="true"></i>

									</div>
									<div class="why_list_content">
										<div class="why_list_title">Only Top Products</div>
									</div>
								</li>

								<!-- Why List Item -->
								<li class="d-flex flex-row align-items-center justify-content-start">
									<div class="icon_container d-flex flex-column align-items-center justify-content-center">
										<i class="fa fa-ravelry" aria-hidden="true"></i>
									</div>
									<div class="why_list_content">
										<div class="why_list_title">The best Doctors</div>
									</div>
								</li>

								<!-- Why List Item -->
								<li class="d-flex flex-row align-items-center justify-content-start">
									<div class="icon_container d-flex flex-column align-items-center justify-content-center">
										<i class="fa fa-grav" aria-hidden="true"></i>
									</div>
									<div class="why_list_content">
										<div class="why_list_title">Great Feedback</div>
									</div>
								</li>

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Call to action -->

	<div class="cta">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="cta_container d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
						<div class="cta_content">
							<div class="cta_title">Make your appointment today!</div>
							<div class="cta_text">Our aim Is to give you the flawless skin that you desire</div>
						</div>
						<div class="cta_phone ml-lg-auto">+977-9840000121</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Services -->

	<div class="services">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title_container">
						<div class="section_subtitle">This is Absolute Asthetics</div>
						<div class="section_title"><h2>Our Services</h2></div>
					</div>
				</div>
			</div>
			<div class="row services_row">
				
				<?php $counter=0; ?>
				<!-- Service -->
				@foreach($services as $service)
				@if($counter <= 5)
				<div class="col-xl-4 col-md-6 service_col">
					<div class="service text-center my-style">
						<div class="service">
							<a href="/services-detail/{{$service->id}}">
							<div class="icon_container1 d-flex flex-column align-items-center justify-content-center ml-auto mr-auto">
								<img style="max-width: 100%;" src="/uploads/{{$service->image}}">
							</div>
							<div style="text-transform: uppercase;" class="service_title">{{$service->name}}</div>
							<div class="service_text my-overflow">
								<p><?php echo ($service->description)?></p>
							</div>
						</a>
						</div>
					</div>
				</div>
				@endif
				<?php $counter++;?>
				@endforeach
		<div class="button button_a ml-auto mr-auto"><a href="/services">View All Services</a></div>
			</div>
		</div>
	</div>

	<div class="tz-gallery gallery-container">
		<div class="col text-center">
					<div class="section_title_container">
						<div class="section_subtitle">This is Absolute Asthetics</div>
						<div class="section_title"><h2>Our Gallery</h2></div>
					</div>
				</div>

        <div class="row services_row">
				<?php $counter=0; ?>

        	@foreach($gallery as $galle)
        	@if($counter <= 7)
            <div class="col-sm-6 col-md-3">
                <?php 
                    $string = $galle->image;
                    $string = substr(strrchr($string, '.'), 1);
                ?>
                @if($string == "jpg" || $string == "png" || $string == "jpeg")
                <a class="lightbox" href="../uploads/gallery/{{$galle->image}}">
                    <img src="../uploads/gallery/{{$galle->image}}" alt="{{$galle->name}}">
                </a>
                @else
                <video class="pb-video-frame my-lightbox" width="100%" height="230" src="../uploads/gallery/{{$galle->image}}" frameborder="0" allowfullscreen controls></video>
                @endif
            </div>
            @endif
            <?php $counter++;?>
			@endforeach
           
           <div class="button button_a ml-auto mr-auto"><a href="/show-gallery">View All Gallery</a></div>
        </div>

    </div>

	<!-- Extra -->

	<div class="extra">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/extra.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="extra_container d-flex flex-row align-items-start justify-content-end">
						<div class="extra_content">
							<div class="extra_disc d-flex flex-row align-items-end justify-content-start">
								<div>30<span>%</span></div>
								<div>Discount</div>
							</div>
							<div class="extra_title">Only in May</div>
							<div class="extra_text">
								<p>Flawless young and radiant skin along with a bright shining  smile Is a winning combination.</p>
							</div>
							<div class="button button_1 extra_link trans_200"><a href="/blog">read more</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Newsletter -->

	<div class="newsletter">
		<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="images/newsletter.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="newsletter_title">Subscribe to our newsletter</div>
				</div>
			</div>
			<div class="row newsletter_row">
				<div class="col-lg-8 offset-lg-2">
					<div class="newsletter_form_container">
						<form action="#" id="newsleter_form" class="newsletter_form">
							<input type="email" class="newsletter_input" placeholder="Your E-mail" required="required">
							<button class="newsletter_button">subscribe</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>
<style>
    .pb-video-container {
        padding-top: 20px;
        background: #bdc3c7;
        font-family: Lato;
    }

    .pb-video {
        border: 1px solid #e6e6e6;
        padding: 5px;
    }

        .pb-video:hover {
            background: #0f0d35;
        }

    .pb-video-frame {
        transition: width 2s, height 2s;
    }


    .pb-row {
        margin-bottom: 10px;
    }
</style>

	@endsection