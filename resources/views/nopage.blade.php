@extends('layouts.main-app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="my-text">
        <h2>Not Allowed.<br>Please Go Back <br>!!!<br>(<b>Admin</b>) </h2>
        <div>
            <a href="/"><button class="btn btn-primary btn-arrow-left">Go Back</button></a>
        </div>
    </div>
    </div>
</div>
@endsection
