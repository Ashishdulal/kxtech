-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 04, 2019 at 04:12 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nepgeeks_kxtechnology`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogcategories`
--

CREATE TABLE `blogcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogcategories`
--

INSERT INTO `blogcategories` (`id`, `title`, `created_at`, `updated_at`) VALUES
(4, 'technology', '2019-04-25 05:27:17', '2019-04-25 05:27:17'),
(5, 'health', '2019-04-25 05:28:39', '2019-04-25 05:28:39'),
(6, 'festivals', '2019-04-25 05:29:13', '2019-04-25 05:29:13'),
(7, 'food', '2019-04-25 05:29:26', '2019-04-25 05:29:26'),
(8, 'Ticker', '2019-05-02 23:54:35', '2019-05-09 04:41:13'),
(9, 'Application', '2019-06-05 18:39:01', '2019-06-05 18:39:01'),
(10, 'aaa', '2019-06-05 18:39:11', '2019-06-05 18:39:11');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `i_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `cat_id`, `title`, `description`, `f_image`, `i_image`, `created_at`, `updated_at`) VALUES
(3, 4, 'Samsung Galaxy Fold', '<p>The Galaxy Fold contains two displays; its front cover contains a small, 4.6-inch display in the center designed for one-handed use, and the device can fold open to expose a 7.3-inch display. Samsung rated the fold mechanism as supporting up to 200,000 uses. The tablet screen contains a large notch in the top-right corner, and is coated with a custom multi-layer laminate in lieu of glass.[6] Its power button contains a fingerprint reader. Samsung did not state which system-on-chip it uses in the Galaxy Fold, beyond that it is a &quot;state-of-the-art&quot; CPU with a 7 nanometer production process, contains 12 GB of RAM, and has 512 GB of non-expandable storage. Teardowns later revealed that it was the Qualcomm Snapdragon 855, which is used in all regions (unlike other Samsung phones that have been split between Snapdragon and Samsung&#39;s in-house Exynos chips depending on the market).[7][8] The Galaxy Fold will be sold with a 5G variant.[9][10][11] The device contains two batteries split between the two halves, totaling a 4380 mAh capacity. The Galaxy Fold contains 6 cameras, using the same sensors equipped on the Galaxy S10+,[6] including three rear-facing camera lenses (12-megapixel, 12-megapixel telephoto, and 16-megapixel ultra wide-angle), as well as a 10-megapixel front-facing camera on the cover, and a second 10-megapixel front-facing camera, accompanied by an RGB depth sensor, on the inside screen.[11][12][10] The Galaxy Fold ships with Android 9.0 &quot;Pie&quot; and Samsung&#39;s One UI software; by means of Multi Window mode, up to three supported apps can be placed on-screen at once. Apps open on the smaller screen can expand into their larger, tablet-oriented layouts when the user unfolds the device.</p>', 'blogs1556192721.jpg', 'blogs1556192734.jpg', '2019-04-25 05:57:28', '2019-05-26 03:37:14'),
(4, 5, 'Nisi corrupti commo', '<p>Cumque commodi placeLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodIpsam cum iusto doloLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla ariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<h1>Kx to power analytics for US real estate giant Zillow</h1>\r\n\r\n<p>4 Apr 2019 |&nbsp;<a href=\"https://kx.com/news-tags/real-estate/\">Real Estate</a>,&nbsp;<a href=\"https://kx.com/news-tags/real-time-analytics/\">Real-time Analytics</a>,&nbsp;<a href=\"https://kx.com/news-tags/zillow/\">Zillow</a></p>\r\n\r\n<p>Share on:</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p><strong>Kx enables seamless scaling of Zillow&rsquo;s analytics platform at lower cost</strong></p>\r\n\r\n<p>(4 April 2019) &ndash; Kx, a division of First Derivatives (FDP.L) and provider of the world&rsquo;s fastest time-series database kdb+, announces that&nbsp;<a href=\"https://www.zillow.com/\">Zillow</a>, the leading web-based real estate marketplace, will implement Kx technology to improve the performance of Zillow&rsquo;s platform through advanced real-time analytics, ultimately enhancing their digital customer experience.</p>\r\n\r\n<p>Implementing Kx technology will enable Zillow to collect, store, and analyze massive amounts of data in real time so they can improve the experience of their website visitors and customers. Zillow will use Kx across several areas, introducing new real-time analytics initiatives while enabling a reduction in Splunk costs.</p>\r\n\r\n<p>The Zillow website operations team oversees a system that processes over 20 billion events daily, more than 50% of which are streaming data points. To handle the exponentially growing data volumes, Kx will provide the intelligence required for monitoring, reporting, investigating and troubleshooting customer-reported incidents. Their centralized system is also used for optimizing customer experience by analyzing and tracking preferences and behaviors.</p>\r\n\r\n<p>The integration of Kx into the operations platform will enable Zillow to reduce the overall cost of site operations by lowering the platform&rsquo;s data processing costs, improving the efficiency of its analytics and providing seamless scaling of data volumes.</p>\r\n\r\n<p><strong>Jerome Ibanes, Monitoring &amp; Analytics at Zillow said:</strong>&nbsp;&ldquo;The data volumes our platform generates are growing exponentially and the various sources of this data have resulted in the need for a high-performance database to make sense of it. Kx has proven to be a great partner and has allowed us to harness the power of our data and obtain actionable intelligence.&rdquo;</p>\r\n\r\n<p><strong>Brian Conlon, Chief Executive Officer of Kx, said:</strong>&nbsp;&ldquo;The scale of streaming and historical data at Zillow is a perfect fit for Kx, which was designed from the start for today&rsquo;s volume of big data. The high-performance capabilities of Kx technology will help Zillow become even more analytics-driven and improve their customer experience while reducing TCO.&rdquo;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Kx</strong></p>\r\n\r\n<p>Kx is a division of First Derivatives (FDP.L), a global technology provider with 20 years of experience working with some of the world&rsquo;s largest finance, technology, retail, pharma, manufacturing and energy institutions. Kx technology, incorporating the kdb+ time-series database, is a leader in high-performance, in-memory computing, streaming analytics and operational intelligence. Kx delivers the best possible performance and flexibility for high-volume, data-intensive analytics and applications across multiple industries. The Group operates from 14 offices across Europe, North America and Asia Pacific, including its headquarters in Newry, and employs more than 2,400 people worldwide.</p>\r\n\r\n<p>Visit&nbsp;<a href=\"http://www.kx.com/\">www.kx.com</a>&nbsp;for more information.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Zillow</strong></p>\r\n\r\n<p>Zillow&reg; is the leading real estate and rental marketplace dedicated to empowering consumers with data, inspiration and knowledge around the place they call home, and connecting them with great real estate professionals. Zillow serves the full lifecycle of owning and living in a home: buying, selling, renting, financing, remodeling and more. Zillow Offers provides homeowners in some metropolitan areas with the opportunity to receive offers to purchase their home from Zillow. When Zillow buys a home, it will make necessary updates and list the home for resale on the open market.</p>\r\n\r\n<p>In addition to Zillow.com, Zillow operates the most popular suite of mobile real estate apps, with more than two dozen apps across all major platforms. Launched in 2006, Zillow is owned and operated by Zillow Group (NASDAQ:Z and ZG) and headquartered in Seattle.</p>\r\n\r\n<p>Zillow is a registered trademark of Zillow, Inc. Zillow Offers is a trademark of Zillow, Inc.</p>', 'blogs1558932649.jpg', 'blogs1557129227.jpg', '2019-04-25 06:01:07', '2019-05-29 02:45:12'),
(6, 7, 'A amet est at expl', '<p>Ipsam cum iusto doloLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo&nbsp;consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,&nbsp;quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse&nbsp;cillum dolore eu fugiat nulla ariatur. Excepteur sint occaecat cupidatat non&nbsp;proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<h1>Kx to power analytics for US real estate giant Zillow</h1>\r\n\r\n<p>4 Apr 2019 |&nbsp;<a href=\"https://kx.com/news-tags/real-estate/\">Real Estate</a>,&nbsp;<a href=\"https://kx.com/news-tags/real-time-analytics/\">Real-time Analytics</a>,&nbsp;<a href=\"https://kx.com/news-tags/zillow/\">Zillow</a></p>\r\n\r\n<p>Share on:</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p><strong>Kx enables seamless scaling of Zillow&rsquo;s analytics platform at lower cost</strong></p>\r\n\r\n<p>(4 April 2019) &ndash; Kx, a division of First Derivatives (FDP.L) and provider of the world&rsquo;s fastest time-series database kdb+, announces that&nbsp;<a href=\"https://www.zillow.com/\">Zillow</a>, the leading web-based real estate marketplace, will implement Kx technology to improve the performance of Zillow&rsquo;s platform through advanced real-time analytics, ultimately enhancing their digital customer experience.</p>\r\n\r\n<p>Implementing Kx technology will enable Zillow to collect, store, and analyze massive amounts of data in real time so they can improve the experience of their website visitors and customers. Zillow will use Kx across several areas, introducing new real-time analytics initiatives while enabling a reduction in Splunk costs.</p>\r\n\r\n<p>The Zillow website operations team oversees a system that processes over 20 billion events daily, more than 50% of which are streaming data points. To handle the exponentially growing data volumes, Kx will provide the intelligence required for monitoring, reporting, investigating and troubleshooting customer-reported incidents. Their centralized system is also used for optimizing customer experience by analyzing and tracking preferences and behaviors.</p>\r\n\r\n<p>The integration of Kx into the operations platform will enable Zillow to reduce the overall cost of site operations by lowering the platform&rsquo;s data processing costs, improving the efficiency of its analytics and providing seamless scaling of data volumes.</p>\r\n\r\n<p><strong>Jerome Ibanes, Monitoring &amp; Analytics at Zillow said:</strong>&nbsp;&ldquo;The data volumes our platform generates are growing exponentially and the various sources of this data have resulted in the need for a high-performance database to make sense of it. Kx has proven to be a great partner and has allowed us to harness the power of our data and obtain actionable intelligence.&rdquo;</p>\r\n\r\n<p><strong>Brian Conlon, Chief Executive Officer of Kx, said:</strong>&nbsp;&ldquo;The scale of streaming and historical data at Zillow is a perfect fit for Kx, which was designed from the start for today&rsquo;s volume of big data. The high-performance capabilities of Kx technology will help Zillow become even more analytics-driven and improve their customer experience while reducing TCO.&rdquo;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Kx</strong></p>\r\n\r\n<p>Kx is a division of First Derivatives (FDP.L), a global technology provider with 20 years of experience working with some of the world&rsquo;s largest finance, technology, retail, pharma, manufacturing and energy institutions. Kx technology, incorporating the kdb+ time-series database, is a leader in high-performance, in-memory computing, streaming analytics and operational intelligence. Kx delivers the best possible performance and flexibility for high-volume, data-intensive analytics and applications across multiple industries. The Group operates from 14 offices across Europe, North America and Asia Pacific, including its headquarters in Newry, and employs more than 2,400 people worldwide.</p>\r\n\r\n<p>Visit&nbsp;<a href=\"http://www.kx.com/\">www.kx.com</a>&nbsp;for more information.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Zillow</strong></p>\r\n\r\n<p>Zillow&reg; is the leading real estate and rental marketplace dedicated to empowering consumers with data, inspiration and knowledge around the place they call home, and connecting them with great real estate professionals. Zillow serves the full lifecycle of owning and living in a home: buying, selling, renting, financing, remodeling and more. Zillow Offers provides homeowners in some metropolitan areas with the opportunity to receive offers to purchase their home from Zillow. When Zillow buys a home, it will make necessary updates and list the home for resale on the open market.</p>\r\n\r\n<p>In addition to Zillow.com, Zillow operates the most popular suite of mobile real estate apps, with more than two dozen apps across all major platforms. Launched in 2006, Zillow is owned and operated by Zillow Group (NASDAQ:Z and ZG) and headquartered in Seattle.</p>\r\n\r\n<p>Zillow is a registered trademark of Zillow, Inc. Zillow Offers is a trademark of Zillow, Inc.</p>', 'blogs1556192798.jpg', 'blogs1556192812.jpg', '2019-04-25 06:01:38', '2019-05-29 02:45:54'),
(7, 6, 'Voluptate voluptates', '<p>Architecto aut suntLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod&nbsp;Ipsam cum iusto doloLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla ariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<h1>Kx to power analytics for US real estate giant Zillow</h1>\r\n\r\n<p>4 Apr 2019 |&nbsp;<a href=\"https://kx.com/news-tags/real-estate/\">Real Estate</a>,&nbsp;<a href=\"https://kx.com/news-tags/real-time-analytics/\">Real-time Analytics</a>,&nbsp;<a href=\"https://kx.com/news-tags/zillow/\">Zillow</a></p>\r\n\r\n<p>Share on:</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p><strong>Kx enables seamless scaling of Zillow&rsquo;s analytics platform at lower cost</strong></p>\r\n\r\n<p>(4 April 2019) &ndash; Kx, a division of First Derivatives (FDP.L) and provider of the world&rsquo;s fastest time-series database kdb+, announces that&nbsp;<a href=\"https://www.zillow.com/\">Zillow</a>, the leading web-based real estate marketplace, will implement Kx technology to improve the performance of Zillow&rsquo;s platform through advanced real-time analytics, ultimately enhancing their digital customer experience.</p>\r\n\r\n<p>Implementing Kx technology will enable Zillow to collect, store, and analyze massive amounts of data in real time so they can improve the experience of their website visitors and customers. Zillow will use Kx across several areas, introducing new real-time analytics initiatives while enabling a reduction in Splunk costs.</p>\r\n\r\n<p>The Zillow website operations team oversees a system that processes over 20 billion events daily, more than 50% of which are streaming data points. To handle the exponentially growing data volumes, Kx will provide the intelligence required for monitoring, reporting, investigating and troubleshooting customer-reported incidents. Their centralized system is also used for optimizing customer experience by analyzing and tracking preferences and behaviors.</p>\r\n\r\n<p>The integration of Kx into the operations platform will enable Zillow to reduce the overall cost of site operations by lowering the platform&rsquo;s data processing costs, improving the efficiency of its analytics and providing seamless scaling of data volumes.</p>\r\n\r\n<p><strong>Jerome Ibanes, Monitoring &amp; Analytics at Zillow said:</strong>&nbsp;&ldquo;The data volumes our platform generates are growing exponentially and the various sources of this data have resulted in the need for a high-performance database to make sense of it. Kx has proven to be a great partner and has allowed us to harness the power of our data and obtain actionable intelligence.&rdquo;</p>\r\n\r\n<p><strong>Brian Conlon, Chief Executive Officer of Kx, said:</strong>&nbsp;&ldquo;The scale of streaming and historical data at Zillow is a perfect fit for Kx, which was designed from the start for today&rsquo;s volume of big data. The high-performance capabilities of Kx technology will help Zillow become even more analytics-driven and improve their customer experience while reducing TCO.&rdquo;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Kx</strong></p>\r\n\r\n<p>Kx is a division of First Derivatives (FDP.L), a global technology provider with 20 years of experience working with some of the world&rsquo;s largest finance, technology, retail, pharma, manufacturing and energy institutions. Kx technology, incorporating the kdb+ time-series database, is a leader in high-performance, in-memory computing, streaming analytics and operational intelligence. Kx delivers the best possible performance and flexibility for high-volume, data-intensive analytics and applications across multiple industries. The Group operates from 14 offices across Europe, North America and Asia Pacific, including its headquarters in Newry, and employs more than 2,400 people worldwide.</p>\r\n\r\n<p>Visit&nbsp;<a href=\"http://www.kx.com/\">www.kx.com</a>&nbsp;for more information.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Zillow</strong></p>\r\n\r\n<p>Zillow&reg; is the leading real estate and rental marketplace dedicated to empowering consumers with data, inspiration and knowledge around the place they call home, and connecting them with great real estate professionals. Zillow serves the full lifecycle of owning and living in a home: buying, selling, renting, financing, remodeling and more. Zillow Offers provides homeowners in some metropolitan areas with the opportunity to receive offers to purchase their home from Zillow. When Zillow buys a home, it will make necessary updates and list the home for resale on the open market.</p>\r\n\r\n<p>In addition to Zillow.com, Zillow operates the most popular suite of mobile real estate apps, with more than two dozen apps across all major platforms. Launched in 2006, Zillow is owned and operated by Zillow Group (NASDAQ:Z and ZG) and headquartered in Seattle.</p>\r\n\r\n<p>Zillow is a registered trademark of Zillow, Inc. Zillow Offers is a trademark of Zillow, Inc.</p>', 'blogs1556263549.jpg', 'blogs1556263549.jpg', '2019-04-26 01:40:50', '2019-05-29 02:45:49'),
(8, 8, 'Cyclone Strikes India', '<p>Officials said the storm could be the most powerful to strike India since 1999, when a cyclone killed more than 10,000 people in the same region of eastern India. But the authorities in the region have significantly improved disaster preparation and response capabilities in the years since, and subsequent major storms have resulted in far fewer deaths. Cyclone Fani is forecast to drop as much as eight inches of rain on northern parts of the state of Andhra Pradesh and on the state of Odisha. The storm is expected to continue north, hitting the neighboring countries of Bangladesh and Bhutan, as well as parts of the Indian states of West Bengal, Arunachal Pradesh, Assam and Meghalaya. Airports in its path were closing and hundreds of trains have been canceled.</p>', 'blogs1556862603.jpg', 'blogs1556862583.jpg', '2019-05-03 00:04:43', '2019-05-26 04:08:19'),
(9, 8, 'How to remove dark spots', 'Dark spots on the skin do not require treatment, but some people may want to remove the spots for cosmetic reasons.\r\n\r\nA dermatologist can offer creams or procedures to lighten dark spots, or in some cases, remove them. Procedures are more expensive than creams and are more likely to cause side effects, though they tend to work faster.\r\n\r\nThe best treatment option may depend on the cause, the size of the dark spot, and the area of the body.\r\n\r\nA dermatologist may recommend one of the following treatments for dark spots on the skin:\r\n\r\nLaser treatment\r\nDifferent types of lasers are available. The most common laser to treat dark spots on the skin uses an intense pulse light laser. The light targets melanin and breaks up the dark spots.\r\n\r\nMicrodermabrasion\r\nDuring microdermabrasion, a dermatologist uses a special device that has an abrasive surface to remove the outer layer of the skin. This treatment promotes new collagen growth, which may help reduce spots.\r\n\r\nChemical peels\r\nA chemical peel involves applying a solution to the skin, which exfoliates the surface, leading to new skin growth. It may gradually fade dark spots on the skin.\r\n\r\nCryotherapy\r\nCryotherapy is a procedure that involves applying liquid nitrogen to the dark patches to freeze them, which injures the skin cells. The skin often heals lighter afterward.\r\n\r\nPrescription skin-lightening cream\r\nPrescription-lightening cream works by bleaching the skin. It typically works gradually and takes several months to decrease the appearance of dark spots.\r\n\r\nHydroquinone, which is the active ingredient in the creams, prevents the skin from producing melanin. Prescription products tend to have a strength of 3–4 percent.', 'blogs1556864087.jpg', 'blogs1556864087.jpg', '2019-05-03 00:29:47', '2019-05-03 00:29:47'),
(10, 4, 'At velit voluptates', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<h1>Kx to power analytics for US real estate giant Zillow</h1>\r\n\r\n<p>4 Apr 2019 |&nbsp;<a href=\"https://kx.com/news-tags/real-estate/\">Real Estate</a>,&nbsp;<a href=\"https://kx.com/news-tags/real-time-analytics/\">Real-time Analytics</a>,&nbsp;<a href=\"https://kx.com/news-tags/zillow/\">Zillow</a></p>\r\n\r\n<p>Share on:</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p><strong>Kx enables seamless scaling of Zillow&rsquo;s analytics platform at lower cost</strong></p>\r\n\r\n<p>(4 April 2019) &ndash; Kx, a division of First Derivatives (FDP.L) and provider of the world&rsquo;s fastest time-series database kdb+, announces that&nbsp;<a href=\"https://www.zillow.com/\">Zillow</a>, the leading web-based real estate marketplace, will implement Kx technology to improve the performance of Zillow&rsquo;s platform through advanced real-time analytics, ultimately enhancing their digital customer experience.</p>\r\n\r\n<p>Implementing Kx technology will enable Zillow to collect, store, and analyze massive amounts of data in real time so they can improve the experience of their website visitors and customers. Zillow will use Kx across several areas, introducing new real-time analytics initiatives while enabling a reduction in Splunk costs.</p>\r\n\r\n<p>The Zillow website operations team oversees a system that processes over 20 billion events daily, more than 50% of which are streaming data points. To handle the exponentially growing data volumes, Kx will provide the intelligence required for monitoring, reporting, investigating and troubleshooting customer-reported incidents. Their centralized system is also used for optimizing customer experience by analyzing and tracking preferences and behaviors.</p>\r\n\r\n<p>The integration of Kx into the operations platform will enable Zillow to reduce the overall cost of site operations by lowering the platform&rsquo;s data processing costs, improving the efficiency of its analytics and providing seamless scaling of data volumes.</p>\r\n\r\n<p><strong>Jerome Ibanes, Monitoring &amp; Analytics at Zillow said:</strong>&nbsp;&ldquo;The data volumes our platform generates are growing exponentially and the various sources of this data have resulted in the need for a high-performance database to make sense of it. Kx has proven to be a great partner and has allowed us to harness the power of our data and obtain actionable intelligence.&rdquo;</p>\r\n\r\n<p><strong>Brian Conlon, Chief Executive Officer of Kx, said:</strong>&nbsp;&ldquo;The scale of streaming and historical data at Zillow is a perfect fit for Kx, which was designed from the start for today&rsquo;s volume of big data. The high-performance capabilities of Kx technology will help Zillow become even more analytics-driven and improve their customer experience while reducing TCO.&rdquo;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Kx</strong></p>\r\n\r\n<p>Kx is a division of First Derivatives (FDP.L), a global technology provider with 20 years of experience working with some of the world&rsquo;s largest finance, technology, retail, pharma, manufacturing and energy institutions. Kx technology, incorporating the kdb+ time-series database, is a leader in high-performance, in-memory computing, streaming analytics and operational intelligence. Kx delivers the best possible performance and flexibility for high-volume, data-intensive analytics and applications across multiple industries. The Group operates from 14 offices across Europe, North America and Asia Pacific, including its headquarters in Newry, and employs more than 2,400 people worldwide.</p>\r\n\r\n<p>Visit&nbsp;<a href=\"http://www.kx.com/\">www.kx.com</a>&nbsp;for more information.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Zillow</strong></p>\r\n\r\n<p>Zillow&reg; is the leading real estate and rental marketplace dedicated to empowering consumers with data, inspiration and knowledge around the place they call home, and connecting them with great real estate professionals. Zillow serves the full lifecycle of owning and living in a home: buying, selling, renting, financing, remodeling and more. Zillow Offers provides homeowners in some metropolitan areas with the opportunity to receive offers to purchase their home from Zillow. When Zillow buys a home, it will make necessary updates and list the home for resale on the open market.</p>\r\n\r\n<p>In addition to Zillow.com, Zillow operates the most popular suite of mobile real estate apps, with more than two dozen apps across all major platforms. Launched in 2006, Zillow is owned and operated by Zillow Group (NASDAQ:Z and ZG) and headquartered in Seattle.</p>\r\n\r\n<p>Zillow is a registered trademark of Zillow, Inc. Zillow Offers is a trademark of Zillow, Inc.</p>', 'blogs1559018219.jpg', 'default-thumbnail.png', '2019-05-27 05:05:17', '2019-05-29 04:20:32'),
(11, 5, 'Officia voluptas vol', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<h1>Kx to power analytics for US real estate giant Zillow</h1>\r\n\r\n<p>4 Apr 2019 |&nbsp;<a href=\"https://kx.com/news-tags/real-estate/\">Real Estate</a>,&nbsp;<a href=\"https://kx.com/news-tags/real-time-analytics/\">Real-time Analytics</a>,&nbsp;<a href=\"https://kx.com/news-tags/zillow/\">Zillow</a></p>\r\n\r\n<p>Share on:</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p><strong>Kx enables seamless scaling of Zillow&rsquo;s analytics platform at lower cost</strong></p>\r\n\r\n<p>(4 April 2019) &ndash; Kx, a division of First Derivatives (FDP.L) and provider of the world&rsquo;s fastest time-series database kdb+, announces that&nbsp;<a href=\"https://www.zillow.com/\">Zillow</a>, the leading web-based real estate marketplace, will implement Kx technology to improve the performance of Zillow&rsquo;s platform through advanced real-time analytics, ultimately enhancing their digital customer experience.</p>\r\n\r\n<p>Implementing Kx technology will enable Zillow to collect, store, and analyze massive amounts of data in real time so they can improve the experience of their website visitors and customers. Zillow will use Kx across several areas, introducing new real-time analytics initiatives while enabling a reduction in Splunk costs.</p>\r\n\r\n<p>The Zillow website operations team oversees a system that processes over 20 billion events daily, more than 50% of which are streaming data points. To handle the exponentially growing data volumes, Kx will provide the intelligence required for monitoring, reporting, investigating and troubleshooting customer-reported incidents. Their centralized system is also used for optimizing customer experience by analyzing and tracking preferences and behaviors.</p>\r\n\r\n<p>The integration of Kx into the operations platform will enable Zillow to reduce the overall cost of site operations by lowering the platform&rsquo;s data processing costs, improving the efficiency of its analytics and providing seamless scaling of data volumes.</p>\r\n\r\n<p><strong>Jerome Ibanes, Monitoring &amp; Analytics at Zillow said:</strong>&nbsp;&ldquo;The data volumes our platform generates are growing exponentially and the various sources of this data have resulted in the need for a high-performance database to make sense of it. Kx has proven to be a great partner and has allowed us to harness the power of our data and obtain actionable intelligence.&rdquo;</p>\r\n\r\n<p><strong>Brian Conlon, Chief Executive Officer of Kx, said:</strong>&nbsp;&ldquo;The scale of streaming and historical data at Zillow is a perfect fit for Kx, which was designed from the start for today&rsquo;s volume of big data. The high-performance capabilities of Kx technology will help Zillow become even more analytics-driven and improve their customer experience while reducing TCO.&rdquo;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Kx</strong></p>\r\n\r\n<p>Kx is a division of First Derivatives (FDP.L), a global technology provider with 20 years of experience working with some of the world&rsquo;s largest finance, technology, retail, pharma, manufacturing and energy institutions. Kx technology, incorporating the kdb+ time-series database, is a leader in high-performance, in-memory computing, streaming analytics and operational intelligence. Kx delivers the best possible performance and flexibility for high-volume, data-intensive analytics and applications across multiple industries. The Group operates from 14 offices across Europe, North America and Asia Pacific, including its headquarters in Newry, and employs more than 2,400 people worldwide.</p>\r\n\r\n<p>Visit&nbsp;<a href=\"http://www.kx.com/\">www.kx.com</a>&nbsp;for more information.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Zillow</strong></p>\r\n\r\n<p>Zillow&reg; is the leading real estate and rental marketplace dedicated to empowering consumers with data, inspiration and knowledge around the place they call home, and connecting them with great real estate professionals. Zillow serves the full lifecycle of owning and living in a home: buying, selling, renting, financing, remodeling and more. Zillow Offers provides homeowners in some metropolitan areas with the opportunity to receive offers to purchase their home from Zillow. When Zillow buys a home, it will make necessary updates and list the home for resale on the open market.</p>\r\n\r\n<p>In addition to Zillow.com, Zillow operates the most popular suite of mobile real estate apps, with more than two dozen apps across all major platforms. Launched in 2006, Zillow is owned and operated by Zillow Group (NASDAQ:Z and ZG) and headquartered in Seattle.</p>\r\n\r\n<p>Zillow is a registered trademark of Zillow, Inc. Zillow Offers is a trademark of Zillow, Inc.</p>', 'blogs1558954402.jpg', 'blogs1558954362.jpg', '2019-05-27 05:07:42', '2019-05-29 02:45:00'),
(12, 5, 'health syllabus for classes', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<h1>Kx to power analytics for US real estate giant Zillow</h1>\r\n\r\n<p>4 Apr 2019 |&nbsp;<a href=\"https://kx.com/news-tags/real-estate/\">Real Estate</a>,&nbsp;<a href=\"https://kx.com/news-tags/real-time-analytics/\">Real-time Analytics</a>,&nbsp;<a href=\"https://kx.com/news-tags/zillow/\">Zillow</a></p>\r\n\r\n<p>Share on:</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p><strong>Kx enables seamless scaling of Zillow&rsquo;s analytics platform at lower cost</strong></p>\r\n\r\n<p>(4 April 2019) &ndash; Kx, a division of First Derivatives (FDP.L) and provider of the world&rsquo;s fastest time-series database kdb+, announces that&nbsp;<a href=\"https://www.zillow.com/\">Zillow</a>, the leading web-based real estate marketplace, will implement Kx technology to improve the performance of Zillow&rsquo;s platform through advanced real-time analytics, ultimately enhancing their digital customer experience.</p>\r\n\r\n<p>Implementing Kx technology will enable Zillow to collect, store, and analyze massive amounts of data in real time so they can improve the experience of their website visitors and customers. Zillow will use Kx across several areas, introducing new real-time analytics initiatives while enabling a reduction in Splunk costs.</p>\r\n\r\n<p>The Zillow website operations team oversees a system that processes over 20 billion events daily, more than 50% of which are streaming data points. To handle the exponentially growing data volumes, Kx will provide the intelligence required for monitoring, reporting, investigating and troubleshooting customer-reported incidents. Their centralized system is also used for optimizing customer experience by analyzing and tracking preferences and behaviors.</p>\r\n\r\n<p>The integration of Kx into the operations platform will enable Zillow to reduce the overall cost of site operations by lowering the platform&rsquo;s data processing costs, improving the efficiency of its analytics and providing seamless scaling of data volumes.</p>\r\n\r\n<p><strong>Jerome Ibanes, Monitoring &amp; Analytics at Zillow said:</strong>&nbsp;&ldquo;The data volumes our platform generates are growing exponentially and the various sources of this data have resulted in the need for a high-performance database to make sense of it. Kx has proven to be a great partner and has allowed us to harness the power of our data and obtain actionable intelligence.&rdquo;</p>\r\n\r\n<p><strong>Brian Conlon, Chief Executive Officer of Kx, said:</strong>&nbsp;&ldquo;The scale of streaming and historical data at Zillow is a perfect fit for Kx, which was designed from the start for today&rsquo;s volume of big data. The high-performance capabilities of Kx technology will help Zillow become even more analytics-driven and improve their customer experience while reducing TCO.&rdquo;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Kx</strong></p>\r\n\r\n<p>Kx is a division of First Derivatives (FDP.L), a global technology provider with 20 years of experience working with some of the world&rsquo;s largest finance, technology, retail, pharma, manufacturing and energy institutions. Kx technology, incorporating the kdb+ time-series database, is a leader in high-performance, in-memory computing, streaming analytics and operational intelligence. Kx delivers the best possible performance and flexibility for high-volume, data-intensive analytics and applications across multiple industries. The Group operates from 14 offices across Europe, North America and Asia Pacific, including its headquarters in Newry, and employs more than 2,400 people worldwide.</p>\r\n\r\n<p>Visit&nbsp;<a href=\"http://www.kx.com/\">www.kx.com</a>&nbsp;for more information.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Zillow</strong></p>\r\n\r\n<p>Zillow&reg; is the leading real estate and rental marketplace dedicated to empowering consumers with data, inspiration and knowledge around the place they call home, and connecting them with great real estate professionals. Zillow serves the full lifecycle of owning and living in a home: buying, selling, renting, financing, remodeling and more. Zillow Offers provides homeowners in some metropolitan areas with the opportunity to receive offers to purchase their home from Zillow. When Zillow buys a home, it will make necessary updates and list the home for resale on the open market.</p>\r\n\r\n<p>In addition to Zillow.com, Zillow operates the most popular suite of mobile real estate apps, with more than two dozen apps across all major platforms. Launched in 2006, Zillow is owned and operated by Zillow Group (NASDAQ:Z and ZG) and headquartered in Seattle.</p>\r\n\r\n<p>Zillow is a registered trademark of Zillow, Inc. Zillow Offers is a trademark of Zillow, Inc.</p>', 'blogs1561610685.jpg', 'blogs1561610685.png', '2019-05-27 05:12:37', '2019-06-27 10:44:45'),
(13, 8, 'Labore necessitatibu', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<h1>Kx to power analytics for US real estate giant Zillow</h1>\r\n\r\n<p>4 Apr 2019 |&nbsp;<a href=\"https://kx.com/news-tags/real-estate/\">Real Estate</a>,&nbsp;<a href=\"https://kx.com/news-tags/real-time-analytics/\">Real-time Analytics</a>,&nbsp;<a href=\"https://kx.com/news-tags/zillow/\">Zillow</a></p>\r\n\r\n<p>Share on:</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p><strong>Kx enables seamless scaling of Zillow&rsquo;s analytics platform at lower cost</strong></p>\r\n\r\n<p>(4 April 2019) &ndash; Kx, a division of First Derivatives (FDP.L) and provider of the world&rsquo;s fastest time-series database kdb+, announces that&nbsp;<a href=\"https://www.zillow.com/\">Zillow</a>, the leading web-based real estate marketplace, will implement Kx technology to improve the performance of Zillow&rsquo;s platform through advanced real-time analytics, ultimately enhancing their digital customer experience.</p>\r\n\r\n<p>Implementing Kx technology will enable Zillow to collect, store, and analyze massive amounts of data in real time so they can improve the experience of their website visitors and customers. Zillow will use Kx across several areas, introducing new real-time analytics initiatives while enabling a reduction in Splunk costs.</p>\r\n\r\n<p>The Zillow website operations team oversees a system that processes over 20 billion events daily, more than 50% of which are streaming data points. To handle the exponentially growing data volumes, Kx will provide the intelligence required for monitoring, reporting, investigating and troubleshooting customer-reported incidents. Their centralized system is also used for optimizing customer experience by analyzing and tracking preferences and behaviors.</p>\r\n\r\n<p>The integration of Kx into the operations platform will enable Zillow to reduce the overall cost of site operations by lowering the platform&rsquo;s data processing costs, improving the efficiency of its analytics and providing seamless scaling of data volumes.</p>\r\n\r\n<p><strong>Jerome Ibanes, Monitoring &amp; Analytics at Zillow said:</strong>&nbsp;&ldquo;The data volumes our platform generates are growing exponentially and the various sources of this data have resulted in the need for a high-performance database to make sense of it. Kx has proven to be a great partner and has allowed us to harness the power of our data and obtain actionable intelligence.&rdquo;</p>\r\n\r\n<p><strong>Brian Conlon, Chief Executive Officer of Kx, said:</strong>&nbsp;&ldquo;The scale of streaming and historical data at Zillow is a perfect fit for Kx, which was designed from the start for today&rsquo;s volume of big data. The high-performance capabilities of Kx technology will help Zillow become even more analytics-driven and improve their customer experience while reducing TCO.&rdquo;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Kx</strong></p>\r\n\r\n<p>Kx is a division of First Derivatives (FDP.L), a global technology provider with 20 years of experience working with some of the world&rsquo;s largest finance, technology, retail, pharma, manufacturing and energy institutions. Kx technology, incorporating the kdb+ time-series database, is a leader in high-performance, in-memory computing, streaming analytics and operational intelligence. Kx delivers the best possible performance and flexibility for high-volume, data-intensive analytics and applications across multiple industries. The Group operates from 14 offices across Europe, North America and Asia Pacific, including its headquarters in Newry, and employs more than 2,400 people worldwide.</p>\r\n\r\n<p>Visit&nbsp;<a href=\"http://www.kx.com/\">www.kx.com</a>&nbsp;for more information.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Zillow</strong></p>\r\n\r\n<p>Zillow&reg; is the leading real estate and rental marketplace dedicated to empowering consumers with data, inspiration and knowledge around the place they call home, and connecting them with great real estate professionals. Zillow serves the full lifecycle of owning and living in a home: buying, selling, renting, financing, remodeling and more. Zillow Offers provides homeowners in some metropolitan areas with the opportunity to receive offers to purchase their home from Zillow. When Zillow buys a home, it will make necessary updates and list the home for resale on the open market.</p>\r\n\r\n<p>In addition to Zillow.com, Zillow operates the most popular suite of mobile real estate apps, with more than two dozen apps across all major platforms. Launched in 2006, Zillow is owned and operated by Zillow Group (NASDAQ:Z and ZG) and headquartered in Seattle.</p>\r\n\r\n<p>Zillow is a registered trademark of Zillow, Inc. Zillow Offers is a trademark of Zillow, Inc.</p>', 'blogs1559027237.jpg', 'default-thumbnail.png', '2019-05-28 01:22:17', '2019-05-29 02:46:16'),
(14, 5, 'Qui dolores error ei', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<h1>Kx to power analytics for US real estate giant Zillow</h1>\r\n\r\n<p>4 Apr 2019 |&nbsp;<a href=\"https://kx.com/news-tags/real-estate/\">Real Estate</a>,&nbsp;<a href=\"https://kx.com/news-tags/real-time-analytics/\">Real-time Analytics</a>,&nbsp;<a href=\"https://kx.com/news-tags/zillow/\">Zillow</a></p>\r\n\r\n<p>Share on:</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p><strong>Kx enables seamless scaling of Zillow&rsquo;s analytics platform at lower cost</strong></p>\r\n\r\n<p>(4 April 2019) &ndash; Kx, a division of First Derivatives (FDP.L) and provider of the world&rsquo;s fastest time-series database kdb+, announces that&nbsp;<a href=\"https://www.zillow.com/\">Zillow</a>, the leading web-based real estate marketplace, will implement Kx technology to improve the performance of Zillow&rsquo;s platform through advanced real-time analytics, ultimately enhancing their digital customer experience.</p>\r\n\r\n<p>Implementing Kx technology will enable Zillow to collect, store, and analyze massive amounts of data in real time so they can improve the experience of their website visitors and customers. Zillow will use Kx across several areas, introducing new real-time analytics initiatives while enabling a reduction in Splunk costs.</p>\r\n\r\n<p>The Zillow website operations team oversees a system that processes over 20 billion events daily, more than 50% of which are streaming data points. To handle the exponentially growing data volumes, Kx will provide the intelligence required for monitoring, reporting, investigating and troubleshooting customer-reported incidents. Their centralized system is also used for optimizing customer experience by analyzing and tracking preferences and behaviors.</p>\r\n\r\n<p>The integration of Kx into the operations platform will enable Zillow to reduce the overall cost of site operations by lowering the platform&rsquo;s data processing costs, improving the efficiency of its analytics and providing seamless scaling of data volumes.</p>\r\n\r\n<p><strong>Jerome Ibanes, Monitoring &amp; Analytics at Zillow said:</strong>&nbsp;&ldquo;The data volumes our platform generates are growing exponentially and the various sources of this data have resulted in the need for a high-performance database to make sense of it. Kx has proven to be a great partner and has allowed us to harness the power of our data and obtain actionable intelligence.&rdquo;</p>\r\n\r\n<p><strong>Brian Conlon, Chief Executive Officer of Kx, said:</strong>&nbsp;&ldquo;The scale of streaming and historical data at Zillow is a perfect fit for Kx, which was designed from the start for today&rsquo;s volume of big data. The high-performance capabilities of Kx technology will help Zillow become even more analytics-driven and improve their customer experience while reducing TCO.&rdquo;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Kx</strong></p>\r\n\r\n<p>Kx is a division of First Derivatives (FDP.L), a global technology provider with 20 years of experience working with some of the world&rsquo;s largest finance, technology, retail, pharma, manufacturing and energy institutions. Kx technology, incorporating the kdb+ time-series database, is a leader in high-performance, in-memory computing, streaming analytics and operational intelligence. Kx delivers the best possible performance and flexibility for high-volume, data-intensive analytics and applications across multiple industries. The Group operates from 14 offices across Europe, North America and Asia Pacific, including its headquarters in Newry, and employs more than 2,400 people worldwide.</p>\r\n\r\n<p>Visit&nbsp;<a href=\"http://www.kx.com/\">www.kx.com</a>&nbsp;for more information.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>About Zillow</strong></p>\r\n\r\n<p>Zillow&reg; is the leading real estate and rental marketplace dedicated to empowering consumers with data, inspiration and knowledge around the place they call home, and connecting them with great real estate professionals. Zillow serves the full lifecycle of owning and living in a home: buying, selling, renting, financing, remodeling and more. Zillow Offers provides homeowners in some metropolitan areas with the opportunity to receive offers to purchase their home from Zillow. When Zillow buys a home, it will make necessary updates and list the home for resale on the open market.</p>\r\n\r\n<p>In addition to Zillow.com, Zillow operates the most popular suite of mobile real estate apps, with more than two dozen apps across all major platforms. Launched in 2006, Zillow is owned and operated by Zillow Group (NASDAQ:Z and ZG) and headquartered in Seattle.</p>\r\n\r\n<p>Zillow is a registered trademark of Zillow, Inc. Zillow Offers is a trademark of Zillow, Inc.</p>', 'default-thumbnail.png', 'default-thumbnail.png', '2019-05-28 01:22:44', '2019-05-29 02:45:32');

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram_link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `name`, `designation`, `image`, `description`, `facebook_link`, `twitter_link`, `instagram_link`, `mail`, `created_at`, `updated_at`) VALUES
(2, 'Lokendra Limbu', 'Venereologist & Cosmetologist & Laser Surgeon', 'doctor1557040533.jpg', '<p>Dr. Limbu He has completed his MD in dermatology from the National Academy of Medical Sciences, Bir Hospital. Available</p>\r\n\r\n<p><strong>Timing :</strong></p>\r\n\r\n<p>-As per consultation</p>\r\n\r\n<p><strong>SPECIALITY : </strong></p>\r\n\r\n<p>-<strong>&nbsp;</strong>Venereologist, -Cosmetologist,</p>\r\n\r\n<p>-Laser Surgeon</p>\r\n\r\n<p><strong>EDUCATION :</strong></p>\r\n\r\n<p>-Dermatologist</p>\r\n\r\n<p><strong>EXPERIENCE : </strong>-5+ Years</p>', 'https://www.facebook.com/username', 'https://www.twitter.com/username', 'https://www.facebook.com/username', 'lrajl@hotmail.com', '2019-05-05 01:30:33', '2019-05-26 23:21:59'),
(3, 'KHAGENDRA CHHETRI', 'Oral and Maxillofacial surgeons', 'doctor1557039855.jpg', '<p>Dr. Chhetri has completed his residency in Oral and Maxillofacial Surgery in 2013. After passing out he has worked in the field of Maxillofacial Surgery in reputed hospitals like Norvic International Hospital, Thapathali and National Institute of Neurological and Allied Sciences, Bansbari. Available</p>\r\n\r\n<p><strong>Timing :</strong> -As per consultation</p>\r\n\r\n<p><strong>SPECIALITY :</strong></p>\r\n\r\n<p>-Oral and Maxillofacial Surgery,</p>\r\n\r\n<p>-Facial Cosmetic and Reconstructive Surgery</p>\r\n\r\n<p><strong>EDUCATION :</strong></p>\r\n\r\n<p>-DMD, MDS(OMFS), FIAOMS</p>\r\n\r\n<p><strong>EXPERIENCE :</strong> -5+ Years</p>', '#', '#', '#', 'chhetriomfs@gmail.com', '2019-05-05 00:33:40', '2019-05-26 23:48:32'),
(6, 'Sunesha Pradhan', 'General and Cosmetic Dentistry', 'doctor1557040400.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>', 'https://www.facebook.com/username', 'https://www.twitter.com/username', 'https://www.facebook.com/username', 'user@gmail.com', '2019-05-05 01:28:20', '2019-05-26 23:48:43'),
(8, 'Rajeev Singh', 'General Dentistry and Orthodontics', 'doctor1557040876.jpg', '<p>Dr. Singh has completed his BDS in dentistry from RV DENTAL COLLEGE BANGALORE . He has completed Fellowship in ICCDE Singapore, he has experience in all general dentistry including RCT, orthodontic, implant,crown and bridges.</p>\r\n\r\n<p><strong>Available Timing :</strong></p>\r\n\r\n<p>-As per cunsultion</p>\r\n\r\n<p><strong>EDUCATION :</strong></p>\r\n\r\n<p><strong>-</strong>BDS (Bangalore) , ICCDE Singapore (pos) ,</p>\r\n\r\n<p><strong>EXPERIENCE :</strong></p>\r\n\r\n<p><strong>-</strong>7 yrs</p>', 'https://www.facebook.com/username', 'https://www.twitter.com/username', 'https://www.facebook.com/username', 'beetle_rulz@hotmail.com', '2019-05-05 01:35:49', '2019-05-26 23:50:35'),
(9, 'Ratna Bahadur Gharti', 'dermatologist', 'doctor1557130433.jpg', '<p>Dr. Ratna has over 5 years of experience in dermatology. He worked as an assistant professor in a medical college in Nepal and as a consultant dermatologist in Maldives. He is also trained in aesthetic and cosmetic procedures and&nbsp;specializes in laser treatment, botox and fillers.</p>\r\n\r\n<p><strong>Timing :</strong>&nbsp;-As per consultation</p>\r\n\r\n<p><strong>SPECIALITY :</strong></p>\r\n\r\n<p>-aesthetic and cosmetic procedure,</p>\r\n\r\n<p>-laser treatment, botox and fillers.</p>\r\n\r\n<p><strong>EDUCATION :</strong></p>\r\n\r\n<p>-NMC 7064,</p>\r\n\r\n<p>-MBBS, MD (Dermatology)</p>\r\n\r\n<p><strong>EXPERIENCE :</strong>&nbsp;</p>\r\n\r\n<p>-5&nbsp;Years</p>', 'https://www.facebook.com/username', 'https://www.twitter.com/username', 'https://www.facebook.com/username', 'ratna_ghartee@hotmail.com', '2019-05-06 02:28:53', '2019-05-26 23:50:47');

-- --------------------------------------------------------

--
-- Table structure for table `foci`
--

CREATE TABLE `foci` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `foci`
--

INSERT INTO `foci` (`id`, `name`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'madtech', 'focus1561091760.jpg', '<p>The newest and fastest-growing evolution that uses technology for marketing and advertising areas. It offers companies the ability to improve and turns data into actionable insights using AI, machine learning and statistical analysis. Optimizing your marketing tactics and streamline all your marketing strategies. Madtech creates powerful impact to your integrated platform and help you achieved your digital advertising goals.</p>', '2019-06-21 10:36:00', '2019-06-21 10:36:00'),
(2, 'Agritech', 'focus1561091829.jpg', '<p>The practice of advanced technology in agriculture which improve products and services resulting from farming. It utilizes technological advancements and expand your food production that is essential in transforming your agricultural techniques and convert this into your commercial success. Revolutionizes your traditional agriculture and emerge with new technologies that gives you agritech solutions.</p>', '2019-06-21 10:37:09', '2019-06-21 10:37:09'),
(3, 'Fintech', 'focus1561091963.jpg', '<p>The modern use of software and technologies to progress in the financial operations. The advanced financial scheme that delivers efficient products and services where operational cost is reduced and large datasets is utilized. Transforming your financial services in the areas of payment systems, invoicing and collections.</p>', '2019-06-21 10:39:23', '2019-06-21 10:39:23'),
(4, 'Foodtech', 'focus1561092007.jpg', '<p>The innovative shift from the food sector where new technology and modernization are merged to improve the entire food chain. This is the technological food progression in production, processing and distribution where all aspects in food science are involved. Whether it&rsquo;s the new systems and principles of food conservation ensuring the abundant and healthier food supply.</p>', '2019-06-21 10:40:07', '2019-06-21 10:40:07'),
(5, 'Healthtech', 'focus1561092045.jpg', '<p>Act as integral part in the health technologies. It reinvents healthcare services by developing new tools in improving health and well-being of individuals. It&rsquo;s the use of contemporary technology in delivering innovative solutions in the form of healthcare information management system, health care document, business intelligence software, electronic medical records, mobile health services and patient monitoring systems.</p>', '2019-06-21 10:40:45', '2019-06-21 10:40:45'),
(6, 'Design', 'focus1561092079.jpg', '<p>We transform your ideas and insights into unforgettable design using our developed strategies merged with our user interface (UI), user experience (UX) and graphic design portfolios. We provide solutions based on specific situation and create engaging contents to deliver exceptional user experience enabling technology adoption. We deliver leading edge digital platform that gratifies your expectations and advances your planned executions.</p>', '2019-06-21 10:41:19', '2019-06-21 10:41:19'),
(7, 'Technology', 'focus1561092187.jpg', '<p>We are IT driven company that provide business solutions based on our advanced technology and quality system platform. It is integrated with the artificial intelligence, operational Intelligence, programming, data management and in-house software into one ecosystem. That focuses on delivering high business performance including improvement in business mobility, employee productivity and automating your business operations.</p>', '2019-06-21 10:43:07', '2019-06-21 10:43:07');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(3, 'face painting', 'gallery1556185462.jpg', '2019-04-25 03:59:22', '2019-05-06 02:06:17'),
(5, 'skin care', 'gallery1556185479.jpg', '2019-04-25 03:59:39', '2019-05-06 02:06:26'),
(11, 'laughing', 'gallery1556188252.jpg', '2019-04-25 04:45:01', '2019-05-06 02:06:58'),
(15, 'xsxsxsxs', 'gallery1557213422.mp4', '2019-05-07 01:32:02', '2019-05-07 01:32:02'),
(17, 'cdscds', 'gallery1557222030.mp4', '2019-05-07 03:55:30', '2019-05-07 03:55:30'),
(20, 'ASDFGHJKL', 'gallery1559299420.mp4', '2019-05-31 16:43:40', '2019-05-31 16:43:40'),
(21, 'Higher Nature Zinc 90 Tablets', 'gallery1561878784.mp4', '2019-06-30 13:13:04', '2019-06-30 13:13:04');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Engineering', 'jobs1560070001.jpg', '<p>Engineering is the use of scientific principles to design and build machines, structures, and other things, including bridges, roads, vehicles, and buildings.</p>', '2019-06-09 14:46:41', '2019-06-09 14:46:41'),
(2, 'General & Admin', 'jobs1560070030.jpg', '<p>General and administrative expenses include all of the non-selling expenses. General and administrative expenses are costs that contribute to the overall .</p>', '2019-06-09 14:47:10', '2019-06-09 14:47:10'),
(3, 'Support', 'jobs1560070049.jpg', '<p>IT Support Officer Training courses for computer operators and technicians working in IT departments in an organization.</p>', '2019-06-09 14:47:29', '2019-06-09 14:48:14'),
(4, 'Intern', 'jobs1560070184.png', '<p>An internship is a period of work experience offered by an organization for a limited period of time. Once confined to medical graduates, the term is now used for a wide range of placements in businesses, non-profit organizations and government agencies.</p>', '2019-06-09 14:49:44', '2019-06-09 14:49:44'),
(5, 'Marketting &Sales', 'jobs1560070252.jpg', '<p>The digital age has introduced obstacles for Sales and Marketing, but it has also opened doors to countless ways to connect with customers.</p>', '2019-06-09 14:50:52', '2019-06-09 14:50:52'),
(6, 'Product Management', 'jobs1560072618.jpg', '<p>Product management is an organisational lifecycle function within a company dealing with the planning, forecasting, and production, or marketing of a product or products at all stages of the product lifecycle.</p>', '2019-06-09 15:29:49', '2019-06-09 15:30:18'),
(7, 'Senior Designer', 'jobs1560073015.jpg', '<p>Senior Designer responsibilities include: Overseeing all design projects, from conception to delivery. Designing original pieces, including illustrations and infographics. Reviewing junior designers&#39; work to ensure high quality.</p>', '2019-06-09 15:36:55', '2019-06-09 15:36:55');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_21_100535_create_services_table', 2),
(5, '2019_04_25_093755_create_galleries_table', 3),
(6, '2019_04_25_103355_create_blogs_table', 4),
(7, '2019_04_25_103905_create_blogcategories_table', 4),
(9, '2019_05_03_112154_create_sliders_table', 5),
(10, '2019_05_05_052429_create_doctors_table', 6),
(11, '2019_05_05_083506_create_testimonials_table', 7),
(12, '2019_05_10_075803_create_views_table', 8),
(13, '2019_05_10_115117_create_view_counts_table', 8),
(14, '2019_06_09_071956_create_jobs_table', 9),
(15, '2019_06_17_071303_create_foci_table', 10),
(16, '2019_06_17_085749_create_portfolio_categories_table', 10),
(17, '2019_06_17_101050_create_portfolios_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('admin@admin.com', '$2y$10$.RNYskhOmhyyB47kcyPL4.yiQYWyuDx9dU1KRHXWE6nqQ.Q6suteO', '2019-04-21 03:32:41'),
('azizdulal.ad@gmail.com', '$2y$10$Rt0CzTbkq6YH4LJnxdh0xeMeyHI1xhsO8VrNRdPU0xLMl4E0x4ure', '2019-05-08 23:57:42'),
('admin@admin.com', '$2y$10$.RNYskhOmhyyB47kcyPL4.yiQYWyuDx9dU1KRHXWE6nqQ.Q6suteO', '2019-04-21 03:32:41'),
('azizdulal.ad@gmail.com', '$2y$10$Rt0CzTbkq6YH4LJnxdh0xeMeyHI1xhsO8VrNRdPU0xLMl4E0x4ure', '2019-05-08 23:57:42'),
('admin@admin.com', '$2y$10$.RNYskhOmhyyB47kcyPL4.yiQYWyuDx9dU1KRHXWE6nqQ.Q6suteO', '2019-04-21 03:32:41'),
('azizdulal.ad@gmail.com', '$2y$10$Rt0CzTbkq6YH4LJnxdh0xeMeyHI1xhsO8VrNRdPU0xLMl4E0x4ure', '2019-05-08 23:57:42');

-- --------------------------------------------------------

--
-- Table structure for table `portfolios`
--

CREATE TABLE `portfolios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `i_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolios`
--

INSERT INTO `portfolios` (`id`, `cat_id`, `title`, `description`, `f_image`, `i_image`, `created_at`, `updated_at`) VALUES
(3, 9, 'ERP', '<p>Enterprise Resource Planning (ERP) is an integrated software that is used to manage the business and to automate its functions in the back office. Simplifying business processes and integrating all operational aspects. It tracks business resources and commitments status. It facilitates information flow and manages connections between all business functions. It allows greater visibility in critical business areas.</p>', 'portfolios1561439797.png', 'portfolios1561439759.png', '2019-06-25 11:15:59', '2019-06-25 11:16:37'),
(4, 9, 'CRM', '<p>Our strategy is to manage a company&#39;s relationships and customer interactions. The CRM system helps businesses to stay customer-connected, streamline processes and improve profitability. It is a tool that helps you focus on your company&#39;s relationship with customers throughout your life cycle. A powerful CRM solution that is a multi-faceted platform where everything is important.</p>', 'portfolios1561439898.jpg', 'portfolios1561439898.png', '2019-06-25 11:18:18', '2019-06-25 11:18:18'),
(5, 9, 'HRM', '<p>Our strategic approach to effective people management within a company to maximize performance productivity by optimizing employee effectiveness. Our structure is to acquire, develop and maintain talent. This involves developing and administering programs designed to increase business success. It is about cultivating the relationship between employer and employee to be able to achieve success from human talent and skills.</p>', 'portfolios1561439991.jpg', 'portfolios1561439991.png', '2019-06-25 11:19:51', '2019-06-25 11:19:51'),
(6, 9, 'POS', '<p>Our POS is a complete management system for retailers, stores and other general merchandise or businesses that want to manage their stock and increase their profits. The POS solutions we have on the marketplace have accounting features and make business decisions about data. Extending your online presence to the physical world. We manage all aspects of your business easily and save hours of manual work. Deliver the best possible experience for businesses and to ensure return on investment.</p>\r\n\r\n<p>Using the latest and leading-edge technology, we offer powerful management systems including POS hardware and POS software and inventory solutions based on our customer needs. Our POS system is user friendly, reliable, sturdy and easy to use. Get a few clicks to access and control any store. Help manage multiple warehouses without worrying about selling products out of stock. Our website synchronizes all data. POS provides informative product control reports, customer insights, sales performance, and increased tracking.</p>', 'portfolios1561440088.jpg', 'portfolios1561440113.jpg', '2019-06-25 11:21:28', '2019-06-25 11:21:53'),
(7, 10, 'UMS', '<p>Your business can improve communications, work remotely and provide responsive customer service through our unified messaging system. We&#39;re helping you get messages across just one platform. To help your organization communicate better and become more productive, UMS has so much to offer for your business. With increasing business weight to boost and improve productivity while reducing communication issues. Reliable, highly secure, accessible and fully featured solution to this problem are our unified messaging platform. It allows you to access email, voice and fax messages, video messaging from a single inbox.</p>\r\n\r\n<p>Providing the best solution for your business needs. We can help you streamline your communications by deploying unified messaging solutions and capitalize on potential cost savings. This provides an efficient communication channel for your business anywhere and anytime. We have already proved ourselves that many companies are reaching out for our help with track of successes. Give you the flexible storage to improve security for your organizations.</p>', 'portfolios1561440782.png', 'portfolios1561440782.png', '2019-06-25 11:33:02', '2019-06-25 11:33:02'),
(8, 10, 'Live Chat', '<p>Our live chat creates a personal relationship with your customers in search of support. Without interrupting their experience, the fastest and most effective way to offer help. It&#39;s clean, free of clutter, and easy to use. Over the past few years of experience, we have provided customer support with over thousands of businesses using our software to increase their profits. Whether you are a small or large company with thousands of employees, we offer support that matches your business size. Our live chat can turn your website visitors into customers, boost sales and earn more.</p>\r\n\r\n<p>The live chat software supports MAC OS, Windows, Linux and iOS. Tailor the live chat window to suit the design of your website. Use greeting message and operator photo to add friendly touches. We manage the behavior of offline chat to ensure that leads are not missed. Provide a fully managed 24/7 service to ensure online accessibility and never miss a chance to engage visitors to your website. Our software has been developed to offer a flexible range of features and our experienced operators are trained expertly to ensure that we maximize every opportunity for you to capture new leads.&nbsp;&nbsp;&nbsp;&nbsp;</p>', 'portfolios1561440888.png', 'portfolios1561440888.jpg', '2019-06-25 11:34:48', '2019-06-25 11:34:48'),
(9, 10, 'Chatbot', '<p>We provide business services with high-quality chatbot development. The bots we offer are user-friendly software that enables real-time automatic interactions between people. Facilitate a direct and personal interaction between your brand and your customer while simultaneously providing real-time interactions 24/7 regardless of your customers &#39; location and time zone.</p>\r\n\r\n<p>It eliminates complexity and helps your customers build the best messaging experience. Provide robust administrative features and connect with our best-powered chatbot to interact with users, making the smartest bot available using powerful machine learning and artificial intelligence.</p>\r\n\r\n<p>We can easily publish your chatbot to mobile devices, web applications, chat services and other popular channels. The chatbots we are building are programmed automatically to communicate with the messages received. To receive and respond to messages, the chatbots leverage chat media such as SMS text, website chat windows and social messaging services across various platforms.</p>', 'portfolios1561441040.jpg', 'portfolios1561441005.jpg', '2019-06-25 11:36:45', '2019-06-25 11:37:20'),
(10, 11, 'Multi-Vendor', '<p>Our multi-vendor is specifically designed for online stores. The multi-vendor e-commerce solution that can be easily installed to help you handle multiple suppliers on your website. We design multi-vendor e-commerce solution to help online merchants easily open an online store where multiple vendors sell their products, services and electronic goods across a single storefront, whether it&#39;s a small online shopping mall or a huge marketplace. With our fully customizable and rich feature multi-vendor platform, you are assured that the number of conversions from target audience to loyal customer will increase rapidly. Based on your audience&#39;s changing trends, we help you grow and expand your marketplace. Our multi-vendor marketplace design to provide you with comprehensive shopping cart features. Offer you the best online marketplace multi-vendor e-commerce platform solutions. Whether building an e-commerce marketplace for digital goods, online / offline services, a versatile catalog of products or any type of product / service, we are excellent in creating a fully functional online marketplace that sells.</p>', 'portfolios1561441189.jpg', 'portfolios1561441189.png', '2019-06-25 11:39:49', '2019-06-25 11:39:49'),
(11, 11, 'Payment Solutions', '<p>We provide superior payment solutions to the world&#39;s leading companies. Scalable solutions that simplify complex transacted payments that are strategically positioned as a true payment partner with proven transactions. Operating as a payment partner with thousands of customers including banks and various businesses outside the financial services industry. Our payment solutions are willing to support strategic plans to upgrade your existing payment channels and enter new and emerging channels with confidence. Supporting virtually all payment types and innovative technologies, including electronic payments, credit / debit card payments, mobile payments and more. It is your universal payment company that empowers payments that are easy, secure, fast and simple. With the help of our experienced team, that turn your payment solutions process conveniently and cost-effectively. Having the ability to connect to a wide range of online payment gateways helps to make it easier for you to transact.</p>', 'portfolios1561441282.png', 'portfolios1561441282.jpg', '2019-06-25 11:41:23', '2019-06-25 11:41:23'),
(12, 11, 'Shipping', '<p>We are a leading logistics company with reliable selection of services like freight and warehousing. The shipping carrier that best matches your e-commerce distribution needs. With the unparalleled shipping experience is our offer with every order you send. All the tools that you need to exceed the expectations of your customers are here. Providing with innovative logistics solution that will help your companies grow. Our platform is designed for any size of business. Manage every shipping aspect.&nbsp; We move goods and deliver them to every corner of the globe irrespective of your industry or market we have solutions for small and large enterprises. Track and manage shipments using digital technologies and connect to their final destinations in a controllable, fast transit and seamless way. We are proven to deliver flexible shipping across the globe with countless experience.</p>', 'portfolios1561441645.png', 'portfolios1561441645.png', '2019-06-25 11:47:25', '2019-06-25 11:47:25');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_categories`
--

CREATE TABLE `portfolio_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolio_categories`
--

INSERT INTO `portfolio_categories` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'health', '2019-06-18 17:20:12', '2019-06-18 17:20:12'),
(2, 'technology', '2019-06-18 17:20:23', '2019-06-18 17:20:23'),
(3, 'medical', '2019-06-18 17:20:31', '2019-06-18 17:20:31'),
(4, 'agriculture', '2019-06-18 17:20:43', '2019-06-18 17:20:43'),
(5, 'sports', '2019-06-18 17:20:50', '2019-06-18 17:20:50'),
(6, 'transportation', '2019-06-18 17:21:01', '2019-06-18 17:21:01'),
(7, 'school', '2019-06-18 17:21:18', '2019-06-18 17:21:18'),
(8, 'accounting', '2019-06-18 17:21:28', '2019-06-18 17:21:28'),
(9, 'Enterprise', '2019-06-25 11:08:48', '2019-06-25 11:08:48'),
(10, 'Communication', '2019-06-25 11:08:59', '2019-06-25 11:08:59'),
(11, 'E-commerce', '2019-06-25 11:09:11', '2019-06-25 11:09:11');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `image`, `description`, `created_at`, `updated_at`) VALUES
(16, 'Web Design & Development', 'services1561092493.jpg', '<p>To help your business attract more visitors and keep them on your site, we offer professional, affordable web design services. We create websites with a comprehensive strategy designed to make customers of your website visitors. Our expert team has built millions of websites and helped thousands of organizations, businesses and individuals build their ideal websites. The website we are building is responsive and mobile-friendly, with millions of devices being browsed. We design your website to compete with your business needs globally. The website we are making also uses to track data that can be used to improve your business, in addition to sharing content information. It is custom mobile and SEO friendly and offers website copywriting, landing page design and optimization.&nbsp; We are a full service and development company that offers a variety of services for web design.</p>', '2019-06-21 10:48:13', '2019-06-25 10:57:24'),
(17, 'Regulation and Compliance', 'services1561092524.jpg', '<p>We offer assistance in avoiding potential penalties and suspension by completing accurate, affordable and timely government filing for you. We remove from your list the problem of regulatory compliance so you can concentrate on running your business. Providing the most reliable and cost-effective reporting services for regulatory compliance and support for any kind of industry. Our goal is to complete compliance with your company&#39;s laws, policies and regulations. Adopting the use of compliance control consolidated and harmonized sets. This approach is intended to ensure that all necessary requirements for governance are met without duplication. Your true regulatory and compliance partner to avoid in-house compliance team unnecessary costs. In all of your operations, we maintain seamless compliance, focusing on your core competencies.</p>', '2019-06-21 10:48:44', '2019-06-25 11:00:15'),
(18, 'Digital Marketing', 'services1561092563.jpg', '<p>A full-service internet marketing company that offers innovative web marketing solutions across the globe to small, medium to large companies. Helping businesses increase sales, build leads, and expand market share. The goal is to provide our customers with a unique and highly effective online presence. With more than a million leads for customers, we become a leading force in redesigning the web.</p>\r\n\r\n<p>We use social media marketing, search marketing and email marketing to promote products and services by leveraging online marketing tactics. Our digital marketing is more cost-effective, provides conversion, generates better revenue, facilitates interaction, cater mobile consumer, and much more. Through digital marketing, we position our clients to become influencers. Our skilled team of social marketers create, manage and deliver top performing social media campaigns for your business.</p>', '2019-06-21 10:49:23', '2019-06-25 10:58:24'),
(19, 'Domain & Hosting', 'services1561092592.jpg', '<p>Our web hosting works on any web server. A service that make your website available on the internet.&nbsp; Deliver the best tools and server space to get your site hosted, live and equipped for visitors. Crafted with our speed, unparalleled security and reliable web hosting service. Consider as one of the finest domain hosting services that DNS hosting, email hosting and web hosting are offered. Our approach is to provide the latest web hosting trends as a hosting provider, including cloud hosting, new authentication tools, automated backups, advance website builders, new hosting hardware, and eco-friendly hosting for your website.</p>', '2019-06-21 10:49:52', '2019-06-25 10:57:56'),
(20, 'Research & Development', 'services1561092619.jpg', '<p>From the beginning, innovations have been at the center of our company. Our R&amp;D team is developing trusted products, systems and services that contribute to improving the quality of people&#39;s lives. Our offering covers the full spectrum of R&amp;D including research, technology, system and processes. By engaging in innovative activities, we are not only providing systems, but also creating solutions with customers.</p>\r\n\r\n<p>To delight the most discerning customers, we combine creativity and technology to create transformative results. The company promotes a strategy for growth and R&amp;D activities that generate new values for customers. We integrate all development phases from basic research to forward-looking development of technology and product to ensure that the company develops cutting-edge and high-quality components quickly and continuously.</p>', '2019-06-21 10:50:19', '2019-06-25 11:00:00'),
(21, 'Graphic Design', 'services1561092658.jpg', '<p>From logo designs, re-branding strategies and marketing materials, we offer a wide range of branding and graphic design service. Making sure our customers build brand identity that will direct them to the top. Our skilled professional team will work closely with our client to understand their business &#39; key message. Specializing in creating eye catching and professional designs to foster your brand and connect to your target audience. We offer a comprehensive graphic design service that covers all areas of art. The graphic designs that we made are known and praised by many around the world. It is user friendly and provides user interface design, publishing design, movement design, and much more. The graphic we structure uses aesthetics and technical requirements when designing a format for your brand. To meet your design requirements, we offer custom designs and complete solutions.</p>', '2019-06-21 10:50:58', '2019-06-25 10:57:38'),
(22, 'Market Analysis', 'services1561438877.jpg', '<p>We develop a business plan that presents market information that you operate in and formulates your business management strategy. It outlines your company&#39;s potential market direction and acts as a key component. Evaluate the suitability of a particular market for your industry and study the current market while searching for new potential markets. Whether you&#39;re a startup looking to expand or re-evaluate your current status. Our market analysis helps you identify the attractiveness of a particular market while examining and analyzing your business &#39; market size, target market, buying patterns for customers and competition.</p>', '2019-06-25 11:01:17', '2019-06-25 11:01:17'),
(23, 'Workshops and Training', 'services1561438950.jpg', '<p>We offer extensive training courses and educational workshops from a wide range of fields to familiarize our participants with a specific field of skills, techniques and ideas. Our programs have been tailored to meet your specific needs. A range of courses that you and your business will find perfect. We aim to deliver the highest quality programs to our customers in a highly competitive environment. Unlock their full potential and ensure that their learning and development needs are met by our training solutions while maximizing business success. As we have grown over the past few years and enabled us to become a leading provider of training. Each year, we use our experience to provide individuals and organizations with effective and relevant training. Our company has the most comprehensive range of training portfolios. We encompass collaborative online learning, online mentorship programs and social learning experiences.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', '2019-06-25 11:02:30', '2019-06-25 11:02:30'),
(24, 'Advisory', 'services1561439010.png', '<p>Our consulting service focuses on helping customers improve their performance and effectively manage their risk. We are working to address specific circumstances and make strategy for them. These circumstances present a vast array of challenges and provide an opportunity to have a serious impact on the businesses of our customers and their own careers. Thus, we offer solution whether its finance, supply chain and customer management.</p>\r\n\r\n<p>We aim to provide integrated approach across a wide range of industries on all types of customers. Using innovative methods to meet the ever-changing needs of businesses. Our services are designed to reflect the priorities of our customers. Aim to help transform their businesses by working with them whenever transactions are planned or underway. Our consultancy is the fastest growing service line of professionals serving around the world. Our success is based on a mix of our dedicated professionals, their expertise and the collective commitment of the company.</p>', '2019-06-25 11:03:30', '2019-06-25 11:03:30'),
(25, 'Development Resources', 'services1561439116.png', '<p>We are one of the growing businesses with dedicated resources for development. This is our fundamental approach to pre-and post-market value creation. We design successful operations and identify our customers &#39; revenue resources, customer base, products and services. To help create value that distills the potential of an organization to its core and essence. The company provides each organization with resources for business development, management consulting and coaching to help their businesses grow while providing the framework for success in overcoming their challenges. We also provide our customers with technical and software support</p>', '2019-06-25 11:05:16', '2019-06-25 11:05:16'),
(26, 'Sales & Marketing', 'services1561439166.jpg', '<p>We are a multi-functional sales and marketing partner to grow your business. Our service includes improving your sales and marketing strategies, ensuring that your website works effectively for you. Providing in-depth sales and marketing services in SEO, email and marketing automation, website redesigns, planning for sales and marketing. Also, help you with various sales and marketing aspects, including lead generation, advertising and sales demonstrations. We have built a long-term partnership with our customers. Helping businesses optimize their sales and marketing strategy to drive profitable growth.</p>', '2019-06-25 11:06:06', '2019-06-25 11:06:06'),
(27, 'Logistical Support', 'services1561439205.jpg', '<p>We are a global logistics support market leader. With years of proven experience in establishing and maintaining logistical procedures. With centralized management approach and integrated business systems are our strategies to deliver exceptional value to our customers to effectively acquire, organize, store, inventory, replace and transport equipment, tools, parts, supplies to every location worldwide.</p>\r\n\r\n<p>Our company is focused on optimizing business processes and competitive solutions for its customers. Striving to improve their operational activities and expand their logistical accomplishments. Therefore, we deliver excellence in handling materials while simplifying the overall process and delivering the desired performance targets for logistics. Offering a wide range of innovative systems integrated solutions.</p>', '2019-06-25 11:06:45', '2019-06-25 11:06:45');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `name`, `image`, `title`, `title_1`, `description`, `created_at`, `updated_at`) VALUES
(6, 'home slider', 'Slider1558599745.png', 'WE HELP YOU GET  MORE TRAFFIC,  LEADS & SALES', '--------------------------------------------', '<h3><samp>Toronto&rsquo;s Search Engine Experts&nbsp;</samp><samp>In Malaysia.</samp></h3>', '2019-05-23 02:34:14', '2019-06-30 14:46:12'),
(7, 'ASDFGHJKL', 'Slider1559470942.jpg', 'Ship better software, faster & Reliable', '--------------------------------------------', '<h3><samp>Everything your team needs to build a great software.</samp></h3>', '2019-06-02 04:27:38', '2019-06-20 11:03:26');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `image`, `description`, `created_at`, `updated_at`) VALUES
(3, 'Ashish Dulal', 'testimonials1557122133.jpg', '<p><em>Hello every one .This company has give the fruitful apps and software that helped in my business growth. thank you very much for the support&nbsp;you guys gave it to me.</em></p>', '2019-05-05 03:41:50', '2019-06-05 11:16:07'),
(6, 'Anuska Shrestha', 'testimonials1557124689.jpg', '<p><em>thanks for the fruitful result tht has made with the use of the software.</em></p>', '2019-05-05 05:48:10', '2019-06-25 10:14:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', NULL, '$2y$10$9OIy6IUc6daTolbBw6NcV.7KZ1WyYYsC/8q2J98py/pOePe1/A2oK', 'sZ8A3G6sbNfBJHA5tFBFHwWrOeGHS0jnEobEsuZ5T5ylXJJgoMs3WJRbqcNu', '2019-04-21 00:52:27', '2019-04-21 00:52:27'),
(2, 'user', 'user@user.com', NULL, '$2y$10$Ytdlyml.r0DrDiSRiG9mFeD8nNfMq2irqhlWOSC8kY0.B1uBRWQQS', NULL, '2019-04-21 03:38:01', '2019-04-21 03:38:01');

-- --------------------------------------------------------

--
-- Table structure for table `views`
--

CREATE TABLE `views` (
  `id` int(10) UNSIGNED NOT NULL,
  `viewable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `viewable_id` bigint(20) UNSIGNED NOT NULL,
  `visitor` text COLLATE utf8mb4_unicode_ci,
  `collection` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viewed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `view_counts`
--

CREATE TABLE `view_counts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view_count` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `view_counts`
--

INSERT INTO `view_counts` (`id`, `ip`, `session_id`, `view_count`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'MuvDKCjLvEe6ID0Qgu8qFPJzFUPgbNTBs0GISBzO', 5, '2019-05-12 05:44:44', '2019-05-12 05:45:09'),
(2, '127.0.0.1', 'I2sj3eCicNcmMZgJKsEYJrL0mbDethT3VLJ4o7d9', 2, '2019-05-12 05:45:21', '2019-05-12 05:45:21'),
(3, '127.0.0.1', 'HuQjK91SveGZTxj4ShwT7Xd35gODZDGnNsQrOXtf', 2, '2019-05-18 23:10:34', '2019-05-18 23:10:34'),
(4, '127.0.0.1', 'diXb3Ldc2jUlM4dj0XgjgDRIDpyWMsgr49uKaPcY', 10, '2019-05-23 02:17:47', '2019-05-23 02:21:28'),
(5, '127.0.0.1', 'SSXdzPAm4xlJWkhpZUVFAGCIQHZUBYDJhfgrcVON', 33, '2019-05-23 02:30:43', '2019-05-23 04:19:35'),
(6, '127.0.0.1', 'CiKZMoZPpxPCimJMy2MGM8Jvfi6zgow6bnDYy1Ow', 107, '2019-05-23 04:50:21', '2019-05-23 06:04:29'),
(7, '127.0.0.1', '1WVFCKy95NzIeXuFjG3bFfXeoEpwNxOroljc1PL0', 95, '2019-05-23 06:07:30', '2019-05-23 06:36:47'),
(8, '127.0.0.1', 'IekK2afvWGpJyW7MB9M6OTkDydLlntR4UTJY4VBD', 5, '2019-05-26 00:15:39', '2019-05-26 00:17:05'),
(9, '127.0.0.1', 'QVr9O0qAC5IJHU3yVwewIwWBdrG9T558i7iRyOrs', 664, '2019-05-26 00:19:21', '2019-05-26 06:05:04'),
(10, '127.0.0.1', 'KszXzSQ3txP6eZsfAp6QjsKM1cNUNJ0y3yujo4Y4', 3, '2019-05-26 05:05:22', '2019-05-26 05:05:23'),
(11, '127.0.0.1', 'o6Nhr1PxiX9s3cjT8FlPy06xzZQIQRsgcBtUmDQf', 5, '2019-05-26 23:04:21', '2019-05-26 23:04:43'),
(12, '127.0.0.1', 'HNm1EM91kV0Ha3TgtvBjkQAzesa4dvfBamYOMMwa', 58, '2019-05-26 23:05:51', '2019-05-27 04:19:16'),
(13, '127.0.0.1', '77cSYeGiTGe12IptZkTGOBhzT6HYUDi6kjoPEcZ0', 13, '2019-05-27 22:42:49', '2019-05-27 22:44:20'),
(14, '127.0.0.1', 'wvM6IWAX5Bfal3JxmGw8C8piMcw6JKBALBIl745w', 3, '2019-05-27 22:46:37', '2019-05-27 22:46:38'),
(15, '127.0.0.1', 'BpFZxkjAav5WWrLajl6oYtRmgi6XOd2s48xLCXqr', 27, '2019-05-28 00:58:54', '2019-05-28 06:00:07'),
(16, '127.0.0.1', 'zPLF1tSttA1b4jHWzE0v6NyQCwbEK8ZyEknvpiwV', 5, '2019-05-28 22:53:27', '2019-05-28 22:53:32'),
(17, '127.0.0.1', 'yx6BLQZeYIZiJxGN3ImpYCWRyrbiZanjKlp7dXl0', 47, '2019-05-28 23:52:22', '2019-05-29 05:11:01'),
(18, '1.186.48.58', 'KHqSjtXsQPw0macZ4PNUS7hJDrNYk5G4mApIv96n', 3, '2019-05-30 17:47:55', '2019-05-30 17:47:57'),
(19, '43.231.210.221', 's7t3h9Kb7FA9mKz4Esd75n4WmZKTmr5vRYyMQy9P', 5, '2019-05-30 17:50:46', '2019-05-30 17:51:39'),
(20, '52.114.77.26', 'P05XB8cr0QHjAyY9Vj0EHOhyzYqz6cKQOHya54jy', 2, '2019-05-30 17:52:15', '2019-05-30 17:52:15'),
(21, '64.233.173.158', 'MjChl3EPkw10z6TiSEy4A2KzGy9Q6jzFrsTcvCGn', 9, '2019-05-30 18:00:29', '2019-05-30 18:02:15'),
(22, '64.233.173.157', 'MjChl3EPkw10z6TiSEy4A2KzGy9Q6jzFrsTcvCGn', 1, '2019-05-30 18:00:33', '2019-05-30 18:00:33'),
(23, '64.233.173.158', 'TQcNv6JC32QnRzPQP6JHINNwyLKfAIpAV72UmOYS', 4, '2019-05-31 07:38:36', '2019-05-31 07:48:59'),
(24, '64.233.173.156', 'TQcNv6JC32QnRzPQP6JHINNwyLKfAIpAV72UmOYS', 1, '2019-05-31 07:38:37', '2019-05-31 07:38:37'),
(25, '27.34.105.230', 'mDKyDbzDYezEYNuxuGJdY4VfgsrEG9UdCGhGWcxW', 13, '2019-05-31 08:20:02', '2019-05-31 08:35:06'),
(26, '27.34.26.223', 'JSD8vUHjzBxFvMWm52Mnt5zJ3TStQz7daUwdcRRv', 9, '2019-05-31 10:43:23', '2019-05-31 11:26:03'),
(27, '27.34.26.223', 'tALBVppHbIXgB7XxDxnYQBuRQ3uOJnI0Nj8Bv3On', 2, '2019-05-31 10:46:01', '2019-05-31 10:46:01'),
(28, '27.34.26.223', 'vgDnsjdxu2TP5HnhA16hJWD5EA3TUctXYzr86wKH', 5, '2019-05-31 10:46:01', '2019-05-31 10:46:16'),
(29, '27.34.26.223', '8GRqy4YGtVMmjtuqn6jEXqKWkxZNauGLcrYyJIzq', 2, '2019-05-31 11:23:03', '2019-05-31 11:23:03'),
(30, '103.9.188.129', 'b8cDWY0jL4J96Lz33W134VsA1pbk6QykkMtg80ry', 27, '2019-05-31 11:31:19', '2019-05-31 12:28:30'),
(31, '27.34.27.119', 'ksOe6hHXW8H6LYAU59Yarvn6i4QEIG1k9sTnTG8H', 21, '2019-05-31 11:56:04', '2019-05-31 16:34:27'),
(32, '64.233.173.156', 'vWHdhmc7ZqVlX6rHacp42UvSz5E2i9cEWB4V4O33', 3, '2019-05-31 16:54:47', '2019-05-31 16:54:51'),
(33, '64.233.173.158', 'WN90uACAN0MmycaiXcqbbq65bEdecOMtFc6OVW7L', 2, '2019-06-02 09:06:19', '2019-06-02 09:06:19'),
(34, '27.34.26.91', 'uqtWcM6odeSs3kVkdixwa2dJwn1juLCEq4n467ge', 2, '2019-06-02 10:12:29', '2019-06-02 10:12:29'),
(35, '27.34.26.91', 'mpqlmRgQKKIXP51gxJRSYFfO6RtVcjPWMC25YdxT', 8, '2019-06-02 10:12:29', '2019-06-02 11:17:01'),
(36, '27.34.26.91', 'aIDHdbGtSqiz3pXWPlWCx0KlPeXKsiWEW0tCJr0P', 10, '2019-06-02 11:18:07', '2019-06-02 14:22:23'),
(37, '64.233.173.157', 'e4xr9VoEv7SV59sXPYj6e0FcHXIVP48vlRLlVbGU', 2, '2019-06-02 11:33:40', '2019-06-02 11:33:40'),
(38, '103.9.188.129', 'XdRmNKsqGhjWbxWtvgLnwSAyTjbvbsifWZbkUWK5', 20, '2019-06-02 11:49:14', '2019-06-02 14:18:47'),
(39, '27.34.26.91', 'JgjO9xhhR3JtN1cIpCgExsYRjFlA1KeYHWOHr8Xq', 17, '2019-06-02 17:00:57', '2019-06-02 17:37:29'),
(40, '27.34.26.107', 'Mxy7YYtu34FSHS8CWuyYeByUQx5xRBHmDsM1228u', 2, '2019-06-03 10:31:03', '2019-06-03 10:31:03'),
(41, '27.34.26.127', 'ztptWWPmrumpODRXTc0z9JbC3iqqrOzCax9ytxMs', 9, '2019-06-03 16:21:36', '2019-06-03 16:58:12'),
(42, '45.64.160.132', '6o9vREr5QkoK0JCLohanDLr2dKze44vPMvjUQfiD', 2, '2019-06-04 13:03:44', '2019-06-04 13:03:44'),
(43, '45.64.160.132', 'kdSgJhftayPkZKmUyUzaXYfCL7e2v9owfyjRit56', 13, '2019-06-04 13:03:45', '2019-06-04 17:52:14'),
(44, '27.34.27.25', 'kdSgJhftayPkZKmUyUzaXYfCL7e2v9owfyjRit56', 1, '2019-06-04 15:04:28', '2019-06-04 15:04:28'),
(45, '27.34.27.25', 'j0Gw0XrctYI7XzwmQUHCYEaSgm8thgNZ50SEg6ZZ', 3, '2019-06-04 15:14:40', '2019-06-04 15:19:25'),
(46, '49.230.58.117', 'WBZgNqGBdIzpn5m5Mjwa87GoFl098slb8nkIpYs2', 15, '2019-06-04 16:52:49', '2019-06-04 17:27:04'),
(47, '27.34.27.25', 'MFgzdEI0aciTyvJkcybFIlDqBxIW4sp49hAGeqfa', 5, '2019-06-04 17:34:44', '2019-06-04 17:37:25'),
(48, '27.34.26.255', 'mKOGugYK2FGqvVVdNNMt8bKEV6wguWXsZkHKg4bd', 6, '2019-06-05 10:50:59', '2019-06-05 11:02:49'),
(49, '103.9.188.129', 'IlRGCHXAS8vvWrTpVbMD7T3HsYs4svfvw6i8DZDD', 4, '2019-06-05 10:53:21', '2019-06-05 10:53:54'),
(50, '27.34.26.255', 'RzKBdLJttAJDCHzH35JSyEZ57zQ2SvuDeWCMWBQt', 19, '2019-06-05 11:32:44', '2019-06-05 18:39:38'),
(51, '27.34.26.130', 'RzKBdLJttAJDCHzH35JSyEZ57zQ2SvuDeWCMWBQt', 1, '2019-06-05 14:28:18', '2019-06-05 14:28:18'),
(52, '103.9.188.129', 'Zwu9VtlF0vwn5Yj5L0Uarf3hdCaULnz3WulPP91K', 26, '2019-06-05 16:12:00', '2019-06-05 17:04:04'),
(53, '27.34.26.130', 'YHX01Q6QgYY17klJLoM8fY57GZ2IKK0rZ137xNiv', 2, '2019-06-05 17:03:52', '2019-06-05 17:03:52'),
(54, '103.9.188.129', 'TpI1hnpTgag5CZELNarnHaKaA6XsIE5Pl51PaVEE', 5, '2019-06-06 10:59:04', '2019-06-06 11:30:21'),
(55, '27.34.24.245', 'tXRZ3qHpbN85Vl6pHltIUqxsqKEEep45m8HPjc7k', 2, '2019-06-06 11:42:04', '2019-06-06 11:42:04'),
(56, '103.9.188.129', 'nQelOFWJyvZgBNPAVLc7qEoByaU3nFBiDJDDyPDb', 7, '2019-06-06 13:39:09', '2019-06-06 13:47:00'),
(57, '103.9.188.129', 'EeEbqujyuDuI4tAfL1h8RGItQyTtfVItD9ZRJyeT', 2, '2019-06-06 16:03:55', '2019-06-06 16:03:55'),
(58, '103.11.217.58', 'i2nImTpZgr9I80BDRbGLCJizjK3BvAgs0vc7A87j', 2, '2019-06-07 00:03:07', '2019-06-07 00:03:07'),
(59, '103.11.217.58', 'H0vear6FOfZ2TgTXf7cVlQlLBE6xDGsd4ldqfryr', 2, '2019-06-07 00:24:51', '2019-06-07 00:24:51'),
(60, '103.9.188.129', 'CdauwiuQB4YdIT2uFqVgSQJwgPaadcbleQGJhOYf', 2, '2019-06-07 12:38:52', '2019-06-07 12:38:52'),
(61, '27.34.26.28', 'imkesIRrWNzXyjF4Xhiwwdz1xZpyujew5U6M9jVs', 2, '2019-06-07 13:00:33', '2019-06-07 13:00:33'),
(62, '27.34.24.45', 'wv1Zekvn1hW8zg4SSypjTAzAMRHTIIMpJ3Ao3QSd', 12, '2019-06-09 11:42:58', '2019-06-09 14:45:40'),
(63, '103.9.188.129', 'fsGDDmHoidPchYsbtKs81c8toc8jpBwNI0XuziIV', 7, '2019-06-09 12:12:56', '2019-06-09 12:54:54'),
(64, '27.34.24.45', 'FUzDd1QEVTttD0UTcXj6WxmvN9jzYDJJpOaj8jxP', 11, '2019-06-09 15:38:39', '2019-06-09 16:41:35'),
(65, '103.9.188.129', 'kE32WSFBq8FGLnas223CxsKdFMYWNHrGCOfRM3Xp', 27, '2019-06-09 15:40:20', '2019-06-09 16:40:50'),
(66, '66.102.6.125', 'IwsqiphnTITCchck471BjLequAjJMScymQdQMyOU', 2, '2019-06-09 16:22:07', '2019-06-09 16:22:07'),
(67, '103.9.188.129', 'pl7LMjwu3Dqflbpdw6Q3wE1MxJnHRHhGwOA7CbHZ', 5, '2019-06-09 16:34:35', '2019-06-09 16:44:50'),
(68, '66.102.6.123', 'tqGEf9NKe6g1NCquuDVX0WZnr0I40eeWt0SbOPji', 2, '2019-06-10 19:32:28', '2019-06-10 19:32:28'),
(69, '66.102.6.125', '0z7xe6cZ4hoPuWT3cPdOa2D01SPQoJa8LXyXSof2', 2, '2019-06-11 08:14:55', '2019-06-11 08:14:55'),
(70, '27.34.24.245', 'Tf2SqYmFRfUO4RirjaUbzM2SeSNuDMbC7WI6NqJO', 3, '2019-06-11 11:28:45', '2019-06-11 11:55:50'),
(71, '103.9.188.129', 'ZOiP6pETKdk25TZtlvLpeC5KK2FlzSpoNTVgrsJC', 9, '2019-06-11 11:37:58', '2019-06-11 11:58:16'),
(72, '27.34.24.245', 'EIi8enfTiZl372JtLxnnkoBGHRPOK206NxAo7oGp', 9, '2019-06-11 12:49:40', '2019-06-11 13:07:28'),
(73, '27.34.25.223', 'ozkY3trTKdhuFvcRBslTYiXQq2bw2GtKSt5pel8Z', 9, '2019-06-11 17:24:54', '2019-06-11 17:32:59'),
(74, '66.102.6.123', '4CTMDPI5l4i534aXKm9tBKMROeL6WfI5wW6MBVkM', 2, '2019-06-11 23:26:37', '2019-06-11 23:26:37'),
(75, '66.102.6.123', 'vCeGTrjHS7npIxROVCagjUaS5WBVjIcvy7Ct0AaU', 2, '2019-06-12 11:38:06', '2019-06-12 11:38:06'),
(76, '27.34.25.25', '3xX2DgJ0K1aCE30A3NucTnQKwmFXTCWgDoDWcg3V', 6, '2019-06-12 15:21:26', '2019-06-12 17:20:25'),
(77, '103.9.188.129', 'jFUPBbl0Wjw311xOYsB9wqzNHcMRjgpdBHd1zkga', 4, '2019-06-12 15:44:53', '2019-06-12 17:22:56'),
(78, '49.230.56.190', 'hTSodyNdlog5CBnKhJe2tlLa1PVu57aigcynWI22', 2, '2019-06-12 21:28:08', '2019-06-12 21:28:08'),
(79, '66.102.6.121', 'cdaqzk9Ra5yW9xQHIZ0DVmvYac4FDlg4SLU2maXG', 2, '2019-06-13 11:22:02', '2019-06-13 11:22:02'),
(80, '66.102.6.123', 'wGwNJU3JukGVPxESJAba0PPDvNW9OXR7kmZuK5wO', 2, '2019-06-14 11:41:19', '2019-06-14 11:41:19'),
(81, '66.102.6.121', 'MgzOSOJhZFnOUGN7j5DGaL73dMH02Vm6OprBKoZ7', 2, '2019-06-16 08:20:07', '2019-06-16 08:20:07'),
(82, '27.34.24.248', 'bK6V45CB8NXuLujVuK18KNnEBx4h7vN9YZe7YsvH', 3, '2019-06-16 11:46:00', '2019-06-16 11:46:12'),
(83, '27.34.26.150', 'LOCYVtyQctaHbYZf9H5J2OSbSxow0IuohYwAjJgg', 2, '2019-06-16 13:45:53', '2019-06-16 13:45:53'),
(84, '182.93.95.99', 'h84rbp9KEcv5knoEBRX6rkgeIFlja9ld6mblqYCK', 2, '2019-06-16 18:27:51', '2019-06-16 18:27:51'),
(85, '182.93.95.99', 'Wyl24aKqDWcoKe2IM8HSROxhyQsL6MFZ3qS5ZzHG', 2, '2019-06-17 10:29:21', '2019-06-17 10:29:21'),
(86, '66.102.6.121', '4SbV5t61vnI2NVTpvZWwdiNVE7s8ijBRLDkYrOTB', 2, '2019-06-17 11:04:28', '2019-06-17 11:04:28'),
(87, '36.252.36.118', 'i2mg5LkDqO7x8cjLdKIP1duhv1lS6u7wRiAnTcZz', 2, '2019-06-17 12:01:22', '2019-06-17 12:01:22'),
(88, '27.34.24.237', 'pT5sy1tYi5jtNfGx53kKQ85ubT765ifBrJy9FX8W', 6, '2019-06-17 12:03:10', '2019-06-17 12:12:50'),
(89, '27.34.24.237', 'I7yKBr5o5OuOsjgLpd0cvi03PzpEddFlpZ5HPcmg', 7, '2019-06-17 12:14:18', '2019-06-17 14:26:42'),
(90, '27.34.24.157', 'I7yKBr5o5OuOsjgLpd0cvi03PzpEddFlpZ5HPcmg', 1, '2019-06-17 14:26:42', '2019-06-17 14:26:42'),
(91, '66.102.6.121', 'l9aG6u9mGBurB34yhaG4MbZRW0y0LBp8sXCr2Qhx', 2, '2019-06-18 11:49:31', '2019-06-18 11:49:31'),
(92, '27.34.26.99', 'iEC3WJSlE2niyqT9PnDXkuRwY1JdO5yl4zwAYbxY', 2, '2019-06-18 12:50:03', '2019-06-18 12:50:03'),
(93, '27.34.26.99', '5WqP9grgrwyIKz0wGAXzqeUAFaXhLIbDfgImZVk4', 3, '2019-06-18 12:50:04', '2019-06-18 14:22:12'),
(94, '27.34.26.99', 'NTK9UxcZ2a8t5NHf4ecuwRm2mDKLErYx4FAycQzk', 5, '2019-06-18 17:12:32', '2019-06-18 17:19:48'),
(95, '27.34.111.120', 'HngNstWYlniY5dh0AJA7zsUk7v0zUyz11h0O2Acc', 3, '2019-06-18 18:24:33', '2019-06-18 18:25:13'),
(96, '66.102.6.125', 'hqXoqNndjGj2vIwqIRo17QCTmThTCRiPBIiS6AQP', 2, '2019-06-19 15:07:51', '2019-06-19 15:07:51'),
(97, '27.34.24.16', '3kbHY3ynuuPFD6KSFRqMkZQ0DeaW6YrsE9NTu7Sv', 2, '2019-06-19 15:12:34', '2019-06-19 15:12:34'),
(98, '27.34.24.16', 'JBVOOLmevYvD8baxNO5hAGIJ9AJ4MnfyF1JsyFOX', 2, '2019-06-19 16:41:09', '2019-06-19 16:41:09'),
(99, '66.102.6.125', 'RkdZ2H7jEx3PaTynhA0ZuiYcIATTtCUBuJtm5UBh', 2, '2019-06-20 08:14:56', '2019-06-20 08:14:56'),
(100, '27.34.24.16', 'wgk0rGDgb5PBcjzylLKS7QIq9E9k947I5xhTek5O', 2, '2019-06-20 10:26:31', '2019-06-20 10:26:31'),
(101, '64.233.173.158', 'OjLZIfkPQPJN9eMOqhWokKfxskjW2pBSXw1Fh5yN', 12, '2019-06-20 10:30:08', '2019-06-20 11:02:14'),
(102, '64.233.173.156', 'OjLZIfkPQPJN9eMOqhWokKfxskjW2pBSXw1Fh5yN', 1, '2019-06-20 10:30:17', '2019-06-20 10:30:17'),
(103, '27.34.24.16', 'eknKvy4iblmbo4fw7mzuncX9pSa2hFN2zTMeV2N5', 3, '2019-06-20 10:31:14', '2019-06-20 10:31:18'),
(104, '64.233.173.157', 'OjLZIfkPQPJN9eMOqhWokKfxskjW2pBSXw1Fh5yN', 1, '2019-06-20 10:49:17', '2019-06-20 10:49:17'),
(105, '27.34.24.16', '5KaaSYoV2LMkfZ9PfSK829Az6UQFzQMJxwFwL3k5', 5, '2019-06-20 11:02:21', '2019-06-20 11:11:06'),
(106, '182.93.95.99', 'fXYX8BlK9B0qFiTJqsm4ibeTwLmTZPdRYHB2sz5z', 4, '2019-06-20 12:22:02', '2019-06-20 12:26:11'),
(107, '202.79.34.189', 'LlaxlL7Eptbg1XFNCTZ5jyi0yhd6tXqEUXZss8Sg', 10, '2019-06-20 17:05:00', '2019-06-20 18:51:28'),
(108, '49.126.211.20', 'sRz0QlE2MZHtMX1EiebcgA42sUN9j2At3NcZBb6D', 3, '2019-06-20 23:53:21', '2019-06-20 23:54:02'),
(109, '66.102.6.125', '2MGGCgWLhVYL8PpcdKiTmm8UshmJtazzkOrqQeAf', 2, '2019-06-21 07:38:47', '2019-06-21 07:38:47'),
(110, '202.79.34.189', 'RHt2upEnkAVTh3RRaepq7XEwayKu0yqdfbdia0or', 2, '2019-06-21 09:50:24', '2019-06-21 09:50:24'),
(111, '27.34.26.149', 'IG2e6PjskMyqrwPHihvEUZpnjvNoH9BhSsHQUY6i', 3, '2019-06-21 10:33:32', '2019-06-21 10:33:37'),
(112, '27.34.26.149', 'EG15XijnynyhsRIn7R0JQRzCK8SEVYHLE26oBbMu', 10, '2019-06-21 10:36:04', '2019-06-21 11:10:42'),
(113, '27.34.24.34', 'EG15XijnynyhsRIn7R0JQRzCK8SEVYHLE26oBbMu', 1, '2019-06-21 11:03:18', '2019-06-21 11:03:18'),
(114, '27.34.24.34', 'n61wBWaX9AXtHUOsu363nDoh3kqxP1GVxfl3steu', 2, '2019-06-21 11:10:49', '2019-06-21 11:10:49'),
(115, '202.79.34.189', 'ioJq5OpsXwfsgZBNIIMxP3s8oerzW76ceHbfjV31', 5, '2019-06-21 17:56:10', '2019-06-21 18:08:49'),
(116, '66.102.6.93', '0No8w0qc1ips4R9ST4Iu4BAwQ7RX2gbWwixh2V8e', 2, '2019-06-23 08:36:59', '2019-06-23 08:36:59'),
(117, '202.79.34.189', 'JCd8LOsGvHHSA67nOpoQRE4I8j1vyFNM8GiInsEL', 6, '2019-06-23 09:59:55', '2019-06-23 11:16:01'),
(118, '66.102.6.92', 'YKMkVnafCjeWgADy9oIWXh0bUMzwPpYmvSPJg1yj', 2, '2019-06-24 09:06:16', '2019-06-24 09:06:16'),
(119, '202.79.34.189', 'n857rAe9QxSfFEX58gpV4wyI72kkh45G6dKuYo4d', 4, '2019-06-24 10:48:43', '2019-06-24 12:42:12'),
(120, '27.34.24.13', 'VdNA3aZzEyzWWGDKY4DG84FoZOdaHaJ4aH44IYrf', 2, '2019-06-24 11:21:19', '2019-06-24 11:21:19'),
(121, '36.252.69.164', '3ey2dDXB374l9gsVv38B92ZwgxoVGCaZThV3sJDa', 2, '2019-06-24 11:22:55', '2019-06-24 11:22:55'),
(122, '27.34.24.13', '3gg08U4w1dGAQt3NAflPWGGcCb41ovSf2eJ8GRSz', 18, '2019-06-24 11:23:28', '2019-06-24 11:54:27'),
(123, '27.34.24.13', '3LXKottcoaQIrUWm4tPflwjKeVcAVt2vLpDKph57', 6, '2019-06-24 11:56:51', '2019-06-24 13:41:09'),
(124, '27.34.24.13', 'xVsXJdk7r0vqpcqvB9LbIqmYj8LoDhuzjjQNjfKU', 4, '2019-06-24 11:58:22', '2019-06-24 12:17:42'),
(125, '27.34.26.248', '3LXKottcoaQIrUWm4tPflwjKeVcAVt2vLpDKph57', 1, '2019-06-24 13:09:19', '2019-06-24 13:09:19'),
(126, '27.34.26.248', 'lbruuYbwcTtSqGgEJGI3lPbB6A9TqWXAZh0hGD0H', 2, '2019-06-24 13:10:00', '2019-06-24 13:10:00'),
(127, '27.34.26.248', 'YoiGT6EesrJKUI9lWnb3BUIK5ZXb2xd08HW9WiKp', 2, '2019-06-24 13:42:31', '2019-06-24 13:42:31'),
(128, '27.34.26.248', 'CplrLcY5Bx0DWpfaUkmRcaFoazuF3ytA4fnyepqo', 2, '2019-06-24 17:39:15', '2019-06-24 17:39:15'),
(129, '66.102.6.125', '05Hvu3jrStQZeuKOqyDkhqeWjnHGhVXyQbP0fuc9', 2, '2019-06-25 10:06:53', '2019-06-25 10:06:53'),
(130, '27.34.24.121', 'KxYWXGWoVoZiqkAFwjuzRUMO5I5ZLATEYLiqfYd6', 6, '2019-06-25 10:07:05', '2019-06-25 10:14:00'),
(131, '27.34.24.121', '1v7BafIdE849GP6ZzAtdlgENY6FBoJY5FANWFNyv', 7, '2019-06-25 10:14:52', '2019-06-25 12:34:44'),
(132, '202.79.34.189', '7nQPZH8lrTvCz9ZigK9ZTnAt45No8RZbpzdua5YN', 5, '2019-06-25 10:16:59', '2019-06-25 10:29:12'),
(133, '27.34.25.141', '2fp766hSSON0JZ8CxOROcZTXE3lyPjCTBLioQa1K', 9, '2019-06-25 12:07:30', '2019-06-25 12:25:27'),
(134, '27.34.25.141', '1v7BafIdE849GP6ZzAtdlgENY6FBoJY5FANWFNyv', 1, '2019-06-25 12:17:55', '2019-06-25 12:17:55'),
(135, '27.34.26.35', '1v7BafIdE849GP6ZzAtdlgENY6FBoJY5FANWFNyv', 1, '2019-06-25 12:34:44', '2019-06-25 12:34:44'),
(136, '202.79.34.189', 'D4xvxvxa6p69tGfyuXoTGbaLgAPCR2scWYidJosp', 2, '2019-06-25 13:25:52', '2019-06-25 13:25:52'),
(137, '182.93.95.99', 'd7K0FvhixYnuf4O1qYIaVNShV0cauvyp9hDAMoqu', 2, '2019-06-25 13:34:31', '2019-06-25 13:34:31'),
(138, '27.34.26.35', '3X6pK6c4Kp2OD0xGZBefy9RbqNGAbm1liCYZZUtR', 4, '2019-06-25 16:02:59', '2019-06-25 16:34:24'),
(139, '27.34.26.35', 'iZ8nlAbfeHxSW9Hu8iOvoWjQsMZXbEeWA0921dC3', 3, '2019-06-25 16:24:05', '2019-06-25 16:24:47'),
(140, '66.102.6.125', 'OgmjCUIXcP1W8iC5pMozQ9zqBzkbNXLrLnpKxAyr', 2, '2019-06-26 12:33:08', '2019-06-26 12:33:08'),
(141, '202.79.34.189', 'TXp1xhf9wH2e6mehDvnVIDOSX8nKEqkHBm8GbASV', 3, '2019-06-26 12:41:13', '2019-06-26 12:41:47'),
(142, '27.34.25.162', 'Uyb8PGNY1nr4fn41AgjCpiBQS14R0UQvKO7A8VtM', 2, '2019-06-26 12:46:14', '2019-06-26 12:46:14'),
(143, '27.34.25.162', 'iJNXg3w1BKIVKBYAU6yibSFg7YF2nMk0SqpkEn6j', 2, '2019-06-26 13:29:53', '2019-06-26 13:29:53'),
(144, '202.79.34.189', 'aEP4IWtLAmSRtONOqjJaiyPKO7fBi9JBi1idt12Q', 2, '2019-06-26 15:31:31', '2019-06-26 15:31:31'),
(145, '202.79.34.189', '0GuClVJIairDuM78aZabxO2nkYTBIWFRGd6DywMX', 2, '2019-06-26 15:31:31', '2019-06-26 15:31:31'),
(146, '27.34.27.130', '8K1auN4UrEnJnVzrwcPobyLm5dj9heE4VO2dUJ5P', 2, '2019-06-26 18:04:55', '2019-06-26 18:04:55'),
(147, '27.34.27.130', 'K0yZbBD9xuWvIk8jxhmMXMaRp6qnWPRUP8qbbbob', 2, '2019-06-26 18:04:55', '2019-06-26 18:04:55'),
(148, '202.79.34.189', '5GjbT65JR4k90OjIoTrmB7SoBY93z0LAWaqnIKxF', 2, '2019-06-26 18:40:39', '2019-06-26 18:40:39'),
(149, '27.34.26.74', 'YXRfEE6S7PmFtYfZyMysW3al4Ta1sGknF49xSVKS', 2, '2019-06-26 20:29:00', '2019-06-26 20:29:00'),
(150, '64.233.173.157', 'FDZvCMlDNVxLceHWbbE0pUbvVac9n4f8Urg0CmP6', 2, '2019-06-26 21:58:33', '2019-06-26 21:58:33'),
(151, '202.79.34.189', 'br5mcM7fY3dFzBJXSfXsYEMBjMvTVYjScbMDEY2G', 3, '2019-06-27 09:54:16', '2019-06-27 09:55:05'),
(152, '66.102.6.125', 'uYTtC3rVjUOEWuDslhwBWffdVhdgZY81Xj39lcbB', 2, '2019-06-27 10:42:51', '2019-06-27 10:42:51'),
(153, '27.34.27.19', 'VftaCtHTKSICmvTZqqRgrqdAUIAv3slYZwHNrg6c', 3, '2019-06-27 10:42:53', '2019-06-27 10:43:36'),
(154, '27.34.27.180', 'b6aF3s2WCrLP4v7zBfq2aLMMH2duwzGMRaNhS2n4', 2, '2019-06-27 12:22:07', '2019-06-27 12:22:07'),
(155, '27.34.27.180', 'eKKlufjhbrBUbJ2vdiFbwBJOIqoiZOMsbpEYOSAu', 3, '2019-06-27 14:01:05', '2019-06-27 15:18:25'),
(156, '202.79.34.189', 'lf4NT8AjDwV4BRHrGvQkr1BQM9pMsvLoemjO95i0', 6, '2019-06-27 16:44:13', '2019-06-27 16:52:50'),
(157, '202.79.34.189', 'W2AvFgRhYqn9ERmtE4vx8Rnbe8HMhb35Fk1PiFa9', 3, '2019-06-27 17:04:53', '2019-06-27 17:05:11'),
(158, '27.34.105.242', 'x63pqaqLcZU7b2bkI0xXsjka4qvic8URdAPBThPs', 2, '2019-06-27 22:35:09', '2019-06-27 22:35:09'),
(159, '66.102.6.125', '9WTmGNy0ERlA97B6eJE26WeCMu7xhOQH9fYgW4Ch', 2, '2019-06-27 22:36:53', '2019-06-27 22:36:53'),
(160, '27.34.25.254', '7XkVhu0mojBHpf7q54tTTuq2XuDHmyCWNc8TFTbn', 6, '2019-06-28 01:03:58', '2019-06-28 01:08:36'),
(161, '66.102.6.123', 'nxqSJJGZc3QfmHGlC9g0SiSx2n6JeEdX3EjCljEI', 2, '2019-06-28 10:51:48', '2019-06-28 10:51:48'),
(162, '27.34.27.155', 'B6CBNiOBi0qAHimQFeWn2ifuoqiOZZpkjSHHmbcu', 7, '2019-06-28 11:29:36', '2019-06-28 13:21:16'),
(163, '202.79.34.189', 'Sh6AIK4KRRkmtLz0m56Iq5nXjp8LI257ESLhWAYh', 2, '2019-06-28 11:31:29', '2019-06-28 11:31:29'),
(164, '64.233.173.158', 'X4H8QFN0KwQT1uDoDyIJWKSOhKSWB9xkxgU8JTsK', 3, '2019-06-28 13:26:55', '2019-06-28 14:18:25'),
(165, '64.233.173.157', 'X4H8QFN0KwQT1uDoDyIJWKSOhKSWB9xkxgU8JTsK', 1, '2019-06-28 14:18:25', '2019-06-28 14:18:25'),
(166, '27.34.25.99', 'IxYwR9EBWIzH5KafoCPKVEB7EXWDM3NcAufDcslh', 4, '2019-06-28 22:07:25', '2019-06-28 22:09:30'),
(167, '27.34.25.99', 'ogWvlVapzFG9P33RtOCfsU9lCDgAEVQVfihLa4DX', 5, '2019-06-28 22:07:55', '2019-06-28 22:15:38'),
(168, '27.34.21.45', '7OR2JOhFewrsKpmyfE1OzMrcxLL7h01kPDzgaRHy', 2, '2019-06-28 22:15:51', '2019-06-28 22:15:51'),
(169, '66.102.6.123', '7OPd8UfIaYQBckW56Yhvu8EocmC0frKoNIoqXRBJ', 2, '2019-06-29 09:18:25', '2019-06-29 09:18:25'),
(170, '66.102.6.123', 'Qz95muYbDkJpQPjqqKNnc1GokUdz9GlqpZ6pfEma', 2, '2019-06-30 11:39:33', '2019-06-30 11:39:33'),
(171, '27.34.24.13', '7DoTQZY0jw4xJfXBmpwSJOlUL2VOETGkMryv0u3V', 2, '2019-06-30 11:40:20', '2019-06-30 11:40:20'),
(172, '27.34.24.13', 'VJ7aTculaCmqrih1eC6Snx9xN1nb3SwqD1eXq1nK', 15, '2019-06-30 11:48:07', '2019-06-30 17:05:26'),
(173, '202.79.34.189', 'ewIzfUVxIg1FvMKUbuIqQ0tZBwyr3VWSkLCBndbd', 2, '2019-06-30 13:28:59', '2019-06-30 13:28:59'),
(174, '27.34.25.90', 'VJ7aTculaCmqrih1eC6Snx9xN1nb3SwqD1eXq1nK', 1, '2019-06-30 17:05:26', '2019-06-30 17:05:26'),
(175, '202.79.34.189', 'Di5AkTFkOI3gM3ogXquaWbNooAIAW56vDuVXTcmq', 2, '2019-07-01 10:01:04', '2019-07-01 10:01:04'),
(176, '27.34.27.204', 'Owxc8B1hannv16pqLSON5AMQ5fJyYeaEhzQd9xQK', 2, '2019-07-01 11:09:51', '2019-07-01 11:09:51'),
(177, '66.102.6.125', '80Eyyl5ywKZxELvlpy1u5OSg9G7ZP2TAwsdlVVxZ', 2, '2019-07-01 11:59:11', '2019-07-01 11:59:11'),
(178, '27.34.26.146', 'Ttqc4i6ivsMAg7IXA6vkntjLYbbMrlX3YWp8DoJq', 2, '2019-07-02 01:51:48', '2019-07-02 01:51:48'),
(179, '27.34.26.146', 'JPcTulqxugi7iRmhngWcRtMBZGATu9Yu1L82Qmsv', 2, '2019-07-02 01:51:50', '2019-07-02 01:51:50'),
(180, '27.34.26.146', 'HbpAp2c4q5F95yg6R1dNhkcYqUSP5ehkHPMBLOWt', 3, '2019-07-02 01:55:49', '2019-07-02 01:56:19'),
(181, '66.102.6.123', 'y0ektR4EzJU0N1quEu3AbvhSYsRXO91XAdYcwD1Q', 2, '2019-07-02 10:31:37', '2019-07-02 10:31:37'),
(182, '27.34.26.34', '2oBg0XLt4McSwA8If1u0KtgbwqMfHRVvkL1yaqH0', 12, '2019-07-02 11:11:15', '2019-07-02 13:53:46'),
(183, '49.126.223.149', 'Sir6BT9OMBX8qSaVwBECvCtHO5XRX1I3XIQBGJrs', 4, '2019-07-02 11:55:18', '2019-07-02 12:06:32'),
(184, '66.102.6.121', 'Ze7FVP5aCfe6wRDITS1x9IrsIwYb6UsqIqJvTeQW', 2, '2019-07-03 10:53:45', '2019-07-03 10:53:45'),
(185, '27.34.26.131', '7gFPgqlaBaU6q8BHvZmcCYDsXZUC7dL2MsCDXBue', 4, '2019-07-03 11:05:21', '2019-07-03 12:48:59'),
(186, '64.233.173.158', 'Bfbz9ZZls50cMfJMA76wrQsg0VLfp3MW4duOejzm', 2, '2019-07-03 11:09:56', '2019-07-03 11:09:56'),
(187, '64.233.173.157', 'ONTLKsZvQD65oZ6bdMQbUxwTyzVvgOWysuCN04ae', 2, '2019-07-03 11:13:25', '2019-07-03 11:13:25'),
(188, '27.34.26.101', 'jo1ZqgkWibPtRDzzA565cX6Lf5XB1oLRtIqX4ysH', 3, '2019-07-04 00:46:49', '2019-07-04 00:47:24'),
(189, '27.34.26.101', 'EQX1MbZtukk56GpvTIKFWKoPeMsZonKMKnxO8cId', 2, '2019-07-04 00:47:36', '2019-07-04 00:47:36'),
(190, '27.34.27.171', 'lgqCLm5bSaPSNBP9GxtbqPqLuJLuxbTej8cmroUA', 2, '2019-07-04 11:03:09', '2019-07-04 11:03:09'),
(191, '27.34.27.171', 'tmRdNzF1Pyyp6VDwlltJWs660QqqZYe9W3kbz437', 9, '2019-07-04 11:03:10', '2019-07-04 12:06:44'),
(192, '64.233.173.157', 'HZRJbKDOpMrmNJbcySAjsXUhm8ZnTLuaTRlCPpq1', 2, '2019-07-04 11:11:03', '2019-07-04 11:11:03'),
(193, '66.102.6.123', 'YaXerKceaTUNvOIQfIXawgtOwpq9iF7h0RD2iEYF', 2, '2019-07-04 11:22:13', '2019-07-04 11:22:13'),
(194, '27.34.27.171', '3zAce4SYbbzmV8SEsfWbKUie0Y7DaFKgnH9ss3rj', 11, '2019-07-04 11:40:07', '2019-07-04 16:09:05'),
(195, '27.34.27.171', 'JHgX5BElmWqcsON86riIjbFmmwzHCdF9q5ijwPp1', 2, '2019-07-04 12:06:25', '2019-07-04 12:06:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogcategories`
--
ALTER TABLE `blogcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foci`
--
ALTER TABLE `foci`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `portfolios`
--
ALTER TABLE `portfolios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_categories`
--
ALTER TABLE `portfolio_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `views`
--
ALTER TABLE `views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `views_viewable_type_viewable_id_index` (`viewable_type`,`viewable_id`);

--
-- Indexes for table `view_counts`
--
ALTER TABLE `view_counts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogcategories`
--
ALTER TABLE `blogcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `foci`
--
ALTER TABLE `foci`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `portfolios`
--
ALTER TABLE `portfolios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `portfolio_categories`
--
ALTER TABLE `portfolio_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `views`
--
ALTER TABLE `views`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `view_counts`
--
ALTER TABLE `view_counts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
