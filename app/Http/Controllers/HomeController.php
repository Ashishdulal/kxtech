<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\view_count;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $rows = view_count::get();
        $users = count($rows);
        $today_view = view_count::whereDate('created_at', DB::raw('CURDATE()'))->get();
        $total_count_today = 0;
        $total_count = 0;
        $total_week_count = 0;
        foreach($today_view as $view){
            $total_count_today += $view->view_count;
        }
        foreach($rows as $view){
            $total_count += $view->view_count;
        }
        $fromDate = Carbon::now()->subDay()->startOfWeek()->toDateString();

        $week_visit = view_count::where('created_at', '>=', $fromDate)->get();
        foreach($week_visit as $week){
            $total_week_count += $week->view_count;
        }
        
        return view('home', compact('users', 'total_count_today', 'total_week_count', 'total_count'));
    }
}
