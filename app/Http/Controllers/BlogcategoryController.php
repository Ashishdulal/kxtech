<?php

namespace App\Http\Controllers;

use App\Blogcategory;
use App\Blog;
use Illuminate\Http\Request;

class BlogcategoryController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogcategories = Blogcategory::all();
        return view ('dashboard.blog.blog-category.index', compact('blogcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.blog.blog-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title'=> ['required', 'min:3']
        ]);

        $blogcategories = new Blogcategory();

        $blogcategories->title = request('title');

        $blogcategories->save();

        return redirect('/home/blog-category');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blogcategory  $blogcategory
     * @return \Illuminate\Http\Response
     */
    public function show(Blogcategory $blogcategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blogcategory  $blogcategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Blogcategory $blogcategory,$id)
    {
        $blogcategories = Blogcategory::findOrfail($id);
        return view ('/dashboard/blog/blog-category/edit',compact('blogcategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blogcategory  $blogcategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blogcategory $blogcategory,$id)
    {
        $blogcategories = Blogcategory::findOrFail($id);

          request()->validate([
            'title'=> ['required', 'min:3']
        ]);

        $blogcategories->title = request('title');

        $blogcategories->save();

        return redirect('/home/blog-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blogcategory  $blogcategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogcategories = Blogcategory::findOrFail($id)->delete();
        return redirect()->back();
    }
}
