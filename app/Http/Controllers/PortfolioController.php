<?php

namespace App\Http\Controllers;

use App\portfolio;
use App\portfolioCategory;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = portfolio::all();
        $portfolioCategories = portfolioCategory::all();
        return view('dashboard.portfolio.posts.index', compact('portfolios','portfolioCategories')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $portfolioCategories = portfolioCategory::all();
        return view ('dashboard.portfolio.posts.create', compact('portfolioCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $portfolios = new portfolio();
        $request->validate([
            'title' => ['required', 'min:3'],
            'description' => ['required', 'min:10'],
            'f_image' => 'image|mimes:jpg,png,jpeg|',
            'i_image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $portfolios->title = request('title');
        $portfolios->cat_id = request('cat_id');
        $portfolios->description = request('description');
        if(file_exists($request->file('f_image'))){
            $f_image = "portfolios".time().'.'.$request->file('f_image')->getclientOriginalExtension()Namelocation = public_path('uploads');
            $request->file('f_image')->move($location, $f_image);
            $portfolios->f_image = $f_image;
        }
        else{
            $portfolios->f_image = 'default-thumbnail.png';
        }
        if(file_exists($request->file('i_image'))){
            $i_image = "portfolios".time().'.'.$request->file('i_image')->getclientOriginalExtension()Namelocation = public_path('uploads');
            $request->file('i_image')->move($location, $i_image);
            $portfolios->i_image = $i_image;
        }
        else{
            $portfolios->i_image = 'default-thumbnail.png';
        }
        $portfolios->save();
        return redirect('/home/portfolio');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function show(portfolio $portfolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function edit(portfolio $portfolio,$id)
    {
        $portfolios = portfolio::findOrFail($id);
        $portfolioCategories = portfolioCategory::all();
        return view('dashboard.portfolio.posts.edit', compact('portfolios','portfolioCategories')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, portfolio $portfolio,$id)
    {
        $portfolios = portfolio::findOrFail($id);
        $request->validate([
            'title' => ['required', 'min:3'],
            'description' => ['required', 'min:10'],
            'f_image' => 'image|mimes:jpg,png,jpeg|',
            'i_image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $portfolios->title = request('title');
        $portfolios->cat_id = request('cat_id');
        $portfolios->description = request('description');
        if(file_exists($request->file('f_image'))){
            $f_image = "portfolios".time().'.'.$request->file('f_image')->getclientOriginalName();
            $location = public_path('uploads');
            $request->file('f_image')->move($location, $f_image);
            $portfolios->f_image = $f_image;
        }
        else{
            $portfolios->f_image = $portfolios->f_image;
        }
        if(file_exists($request->file('i_image'))){
            $i_image = "portfolios".time().'.'.$request->file('i_image')->getclientOriginalName();
            $location = public_path('uploads');
            $request->file('i_image')->move($location, $i_image);
            $portfolios->i_image = $i_image;
        }
        else{
            $portfolios->i_image = $portfolios->i_image;
        }
        $portfolios->save();
        return redirect('/home/portfolio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolios = portfolio::findOrFail($id)->delete();
        return redirect()->back();
    }
}
