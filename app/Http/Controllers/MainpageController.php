<?php

namespace App\Http\Controllers;

use App\Testimonial;
use App\Doctors;
use App\job;
use App\Slider;
use Carbon\Carbon;
use App\Blog;
use App\Blogcategory;
use App\Gallery;
use App\Service;
use App\portfolio;
use App\portfolioCategory;
use App\Mainpage;
use App\Focus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\view_count; 
use Session;

class MainpageController extends Controller 
{

   public function index(){
    $viewCount = view_count::firstOrCreate([
      'ip' => $_SERVER['REMOTE_ADDR'],
      'session_id' => session()->getID()
    ]);

    $result = view_count::where('session_id', session()->getID())->first();

    if($result){

      $result->view_count += 1;
      $result->save();
    }

    $blogs = Blog::all();
    $blogcategories = Blogcategory::all();
    $portfolioCategories = portfolioCategory::latest()->get();
    $portfolios = portfolio::latest()->paginate();
   	$services = Service::latest()->get();
   	$gallery = Gallery::latest()->get();
    $sliders = Slider::latest()->get();
    $focus = Focus::latest()->get();
    $doctor = Doctors::all();
   	return view('mainIndex',compact('services', 'gallery','sliders','blogs','blogcategories','doctor','focus','portfolioCategories','portfolios'));
   }

   public function aboutPage(){
    $doctor = Doctors::all();
    $testimonials = Testimonial::latest()->get();;
   return view('about', compact('doctor','testimonials'));
  }

   public function galleryPage(){
   	$gallery = Gallery::latest()->get();
   return view('gallery', compact('gallery'));
  }
  public function carrierhomepage()
  {
    $jobs = job::latest()->paginate(6);
    $alljobs = job::all();
      return view('jobs',compact('jobs','alljobs'));
  }

	public function blogdetail($id)
    {
        $blogs = Blog::findOrFail($id);
        $allblogs = Blog::latest()->get();
        $blogcategories = Blogcategory::latest()->get();
        return view ('blog-detail',compact('blogs','blogcategories','allblogs'));
    }
    public function blogCategoryPage($id)
    {
      $blogCatName = Blogcategory::findOrFail($id)->title;
        $blogcategories = Blogcategory::latest()->get();
        $blogs = Blog::latest()->paginate();
        return view ('blog-category-detail',compact('blogs','blogcategories', 'id', 'blogCatName'));
    }
    // public function portfolioCategoryPage($id)
    // {
    //   $portfolioCatName = portfolioCategory::findOrFail($id)->title;
    //     $portfolioCategories = portfolioCategory::latest()->get();
    //     $portfolios = portfolio::latest()->paginate();
    //     return view ('portfolio-category-detail',compact('portfolios','portfolioCategories', 'id', 'portfolioCatName'));
    // }
    public function portfoliohomepage()
    {
        $portfolios = portfolio::latest()->get();
        $portfolioCategories = portfolioCategory::latest()->get();
        return view ('portfolio',compact('portfolios','portfolioCategories'));
    }
    public function portfoliodetailpage($id)
    {
        $portfolios = portfolio::findOrFail($id);
        $portfolioCategories = portfolioCategory::latest()->get();
        return view ('portfolio-detail',compact('portfolios','portfolioCategories'));
    }
    public function bloghomepage()
    {
        $blogs = Blog::latest()->paginate(4);
        $blogcategories = Blogcategory::latest()->get();
        return view ('blog',compact('blogs','blogcategories'));
    }
    public function servicepage()
    {
        $services = Service::latest()->paginate(6);
        return view('services', compact('services'));
    }
    public function jobsIndex()
    {
      $jobs = job::latest()->paginate(6);
      return view('jobs',compact('jobs'));
    }
    public function jobsDetail(job $job,$id)
    {
      $jobs = job::findOrFail($id);
      $alljobs = $job::latest()->get();
      return view ('jobs-detail',compact('jobs','alljobs'));
    }
    public function detailpage(Service $service,$id)
    {
        $services = Service::findOrFail($id);
        return view('service-detail', compact('services'));
    }
    public function FocusDetailPage(focus $Focus,$id)
    {
        $focus = Focus::findOrFail($id);
        return view('focus-detail', compact('focus'));
    }
}